package com.bbva.mzic.accounts.rm.enums;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "subTypeEnum", namespace = "urn:com:bbva:zic:accounts:facade:v0:enums")
@XmlType(name = "subTypeEnum", namespace = "urn:com:bbva:zic:accounts:facade:v0:enums")
@XmlAccessorType(XmlAccessType.FIELD)

public enum SubTypeEnum {
	NOW_ACCOUNT, // Negotiable account of withdrawal order that generates interest.
	INVESTMENT, // Investment account in dollars.
	INTEREST_FREE_CHECK, // Account without interest in dollars.
}
