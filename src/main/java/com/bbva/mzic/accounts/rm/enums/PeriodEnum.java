package com.bbva.mzic.accounts.rm.enums;

public enum PeriodEnum {
	DAILY, // This condition applies Daily.
	WEEKLY, // This condition applies Weekly.
	MONTHLY, // This condition applies Monthly.
	YEARLY // This condition applies Yearly.
}
