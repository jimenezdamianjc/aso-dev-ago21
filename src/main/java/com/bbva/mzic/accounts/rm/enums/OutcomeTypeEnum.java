package com.bbva.mzic.accounts.rm.enums;

public enum OutcomeTypeEnum {
	FEE_AMOUNT, // Fee amount.
	FEE_PERCENTAGE // Fee percentage.
}
