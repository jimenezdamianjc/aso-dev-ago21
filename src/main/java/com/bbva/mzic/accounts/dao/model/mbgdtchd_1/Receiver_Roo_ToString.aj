// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.mbgdtchd_1;

import java.lang.String;

privileged aspect Receiver_Roo_ToString {
    
    public String Receiver.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Holder: ").append(getHolder() == null ? "null" : getHolder().size()).append(", ");
        sb.append("Value: ").append(getValue());
        return sb.toString();
    }
    
}
