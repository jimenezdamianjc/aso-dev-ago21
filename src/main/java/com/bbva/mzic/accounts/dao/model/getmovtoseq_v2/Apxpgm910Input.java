
package com.bbva.mzic.accounts.dao.model.getmovtoseq_v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Clase Java para apxpgm910Input complex type.
 * 
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que
 * haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="apxpgm910Input">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WK_CVECTA" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WK_FECINI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WK_FECFIN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WK_MONTOIN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WK_MONTOFI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WK_ACCION" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WK_PAGINAANT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WK_TIPOOPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(	name = "apxpgm910Input",
			propOrder = { "wkcvecta", "wkfecini", "wkfecfin", "wkmontoin", "wkmontofi", "wkaccion", "wkpaginaant", "wktipoope" })
public class Apxpgm910Input {

	@XmlElement(name = "WK_CVECTA", required = true)
	protected String wkcvecta;
	@XmlElement(name = "WK_FECINI", required = true)
	protected String wkfecini;
	@XmlElement(name = "WK_FECFIN", required = true)
	protected String wkfecfin;
	@XmlElement(name = "WK_MONTOIN", required = true)
	protected String wkmontoin;
	@XmlElement(name = "WK_MONTOFI", required = true)
	protected String wkmontofi;
	@XmlElement(name = "WK_ACCION", required = true)
	protected String wkaccion;
	@XmlElement(name = "WK_PAGINAANT", required = true)
	protected String wkpaginaant;
	@XmlElement(name = "WK_TIPOOPE", required = true)
	protected String wktipoope;

	/**
	 * Obtiene el valor de la propiedad wkcvecta.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWKCVECTA() {
		return wkcvecta;
	}

	/**
	 * Define el valor de la propiedad wkcvecta.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWKCVECTA(String value) {
		this.wkcvecta = value;
	}

	/**
	 * Obtiene el valor de la propiedad wkfecini.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWKFECINI() {
		return wkfecini;
	}

	/**
	 * Define el valor de la propiedad wkfecini.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWKFECINI(String value) {
		this.wkfecini = value;
	}

	/**
	 * Obtiene el valor de la propiedad wkfecfin.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWKFECFIN() {
		return wkfecfin;
	}

	/**
	 * Define el valor de la propiedad wkfecfin.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWKFECFIN(String value) {
		this.wkfecfin = value;
	}

	/**
	 * Obtiene el valor de la propiedad wkmontoin.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWKMONTOIN() {
		return wkmontoin;
	}

	/**
	 * Define el valor de la propiedad wkmontoin.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWKMONTOIN(String value) {
		this.wkmontoin = value;
	}

	/**
	 * Obtiene el valor de la propiedad wkmontofi.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWKMONTOFI() {
		return wkmontofi;
	}

	/**
	 * Define el valor de la propiedad wkmontofi.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWKMONTOFI(String value) {
		this.wkmontofi = value;
	}

	/**
	 * Obtiene el valor de la propiedad wkaccion.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWKACCION() {
		return wkaccion;
	}

	/**
	 * Define el valor de la propiedad wkaccion.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWKACCION(String value) {
		this.wkaccion = value;
	}

	/**
	 * Obtiene el valor de la propiedad wkpaginaant.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWKPAGINAANT() {
		return wkpaginaant;
	}

	/**
	 * Define el valor de la propiedad wkpaginaant.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWKPAGINAANT(String value) {
		this.wkpaginaant = value;
	}

	/**
	 * Obtiene el valor de la propiedad wktipoope.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWKTIPOOPE() {
		return wktipoope;
	}

	/**
	 * Define el valor de la propiedad wktipoope.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWKTIPOOPE(String value) {
		this.wktipoope = value;
	}

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(150);
        builder.append("Apxpgm910Input [wkcvecta=").append(wkcvecta).append(", wkfecini=").append(wkfecini).append(", wkfecfin=")
                .append(wkfecfin).append(", wkmontoin=").append(wkmontoin).append(", wkmontofi=").append(wkmontofi).append(", wkaccion=")
                .append(wkaccion).append(", wkpaginaant=").append(wkpaginaant).append(", wktipoope=").append(wktipoope).append(']');
        return builder.toString();
    }
	
}
