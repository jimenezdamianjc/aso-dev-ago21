package com.bbva.mzic.accounts.dao.model.mtkdt053_1;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * <p>Bean fila para el campo tabular <code>businessAgent</code>, utilizado por la clase <code>RespuestaTransaccionMtkdt053_1</code></p>
 * 
 * @see RespuestaTransaccionMtkdt053_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Businessagent {
	
	/**
	 * <p>Campo <code>userId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "userId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 13, signo = true)
	private String userid;
	
	/**
	 * <p>Campo <code>userName</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "userName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 90, signo = true)
	private String username;
	
	/**
	 * <p>Campo <code>userType</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "userType", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true)
	private String usertype;
	
}
