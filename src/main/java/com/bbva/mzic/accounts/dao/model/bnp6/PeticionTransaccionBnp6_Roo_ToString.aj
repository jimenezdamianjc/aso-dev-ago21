package com.bbva.mzic.accounts.dao.model.bnp6;

import java.lang.String;

privileged aspect PeticionTransaccionBnp6_Roo_ToString {
    
    public String PeticionTransaccionBnp6.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Cuerpo: ").append(getCuerpo());
        return sb.toString();
    }
    
}
