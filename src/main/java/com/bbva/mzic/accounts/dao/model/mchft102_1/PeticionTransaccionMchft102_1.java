package com.bbva.mzic.accounts.dao.model.mchft102_1;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Transacci&oacute;n <code>MCHFT102</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMchft102_1</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionMchft102_1</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: MCHFT102-01-MX_15-02-2018.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;&lt;transaction xmlns=&quot;http://www.w3schools.com&quot; transactionName=&quot;MCHFT102&quot; application=&quot;MCHF&quot; version=&quot;01&quot; country=&quot;MX&quot; language=&quot;ES&quot;&gt;&lt;paramsIn&gt;&lt;parameter order=&quot;1&quot; name=&quot;idClient&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;2&quot; name=&quot;startPeriodDate&quot; type=&quot;String&quot; size=&quot;30&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;3&quot; name=&quot;endPeriodDate&quot; type=&quot;String&quot; size=&quot;30&quot; mandatory=&quot;0&quot;/&gt;&lt;/paramsIn&gt;&lt;paramsOut&gt;&lt;parameter order=&quot;1&quot; name=&quot;idClient&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;&lt;group name=&quot;month&quot; order=&quot;2&quot;&gt;&lt;parameter order=&quot;1&quot; name=&quot;monthName&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;2&quot; name=&quot;dateUpdateHealth&quot; type=&quot;String&quot; size=&quot;30&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;3&quot; name=&quot;attitudinalSegment&quot; type=&quot;String&quot; size=&quot;25&quot; mandatory=&quot;0&quot;/&gt;&lt;parameter order=&quot;4&quot; name=&quot;healthId&quot; type=&quot;Double&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;5&quot; name=&quot;healthDetail&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;1&quot;/&gt;&lt;group name=&quot;parameters&quot; order=&quot;6&quot;&gt;&lt;parameter order=&quot;1&quot; name=&quot;healthParameter&quot; type=&quot;Long&quot; size=&quot;10&quot; mandatory=&quot;0&quot;/&gt;&lt;parameter order=&quot;2&quot; name=&quot;healthValue&quot; type=&quot;Double&quot; size=&quot;30&quot; mandatory=&quot;0&quot;/&gt;&lt;/group&gt;&lt;group name=&quot;whyHealth&quot; order=&quot;7&quot;&gt;&lt;parameter order=&quot;1&quot; name=&quot;idWhyHealth&quot; type=&quot;Long&quot; size=&quot;10&quot; mandatory=&quot;0&quot;/&gt;&lt;parameter order=&quot;2&quot; name=&quot;whyHealthDetail&quot; type=&quot;String&quot; size=&quot;125&quot; mandatory=&quot;0&quot;/&gt;&lt;/group&gt;&lt;group name=&quot;business&quot; order=&quot;8&quot;&gt;&lt;parameter order=&quot;1&quot; name=&quot;businessId&quot; type=&quot;Long&quot; size=&quot;10&quot; mandatory=&quot;0&quot;/&gt;&lt;parameter order=&quot;2&quot; name=&quot;businessDetail&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;0&quot;/&gt;&lt;parameter order=&quot;3&quot; name=&quot;infoTipId&quot; type=&quot;Long&quot; size=&quot;10&quot; mandatory=&quot;0&quot;/&gt;&lt;parameter order=&quot;4&quot; name=&quot;infoTipDetail&quot; type=&quot;String&quot; size=&quot;140&quot; mandatory=&quot;0&quot;/&gt;&lt;parameter order=&quot;5&quot; name=&quot;actionTipId&quot; type=&quot;Long&quot; size=&quot;10&quot; mandatory=&quot;0&quot;/&gt;&lt;parameter order=&quot;6&quot; name=&quot;actionDetail&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;0&quot;/&gt;&lt;parameter order=&quot;7&quot; name=&quot;actionAmount&quot; type=&quot;Double&quot; size=&quot;17&quot; mandatory=&quot;0&quot;/&gt;&lt;parameter order=&quot;8&quot; name=&quot;ActionAmountPercent&quot; type=&quot;Double&quot; size=&quot;8&quot; mandatory=&quot;0&quot;/&gt;&lt;/group&gt;&lt;/group&gt;&lt;/paramsOut&gt;&lt;description&gt;Obtener informacion de los movimientos categorizados por&amp;#xD;
 * por Id Cliente y tips para mostrar&lt;/description&gt;&lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionMchft102_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "MCHFT102", tipo = 1, subtipo = 1, version = 1, configuracion = "default_apx",
        respuesta = RespuestaTransaccionMchft102_1.class, atributos = {@Atributo(nombre = "country", valor = "MX")})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMchft102_1 {

    /**
     * <p>
     * Campo <code>idClient</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "idClient", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
    private String idclient;

    /**
     * <p>
     * Campo <code>startPeriodDate</code>, &iacute;ndice: <code>2</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "startPeriodDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 30, signo = true, obligatorio = true)
    private String startperioddate;

    /**
     * <p>
     * Campo <code>endPeriodDate</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "endPeriodDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 30, signo = true)
    private String endperioddate;

}
