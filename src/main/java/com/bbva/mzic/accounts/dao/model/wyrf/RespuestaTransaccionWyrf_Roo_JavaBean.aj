// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.wyrf;

import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import java.lang.String;

privileged aspect RespuestaTransaccionWyrf_Roo_JavaBean {
    
    public String RespuestaTransaccionWyrf.getCodigoRetorno() {
        return this.codigoRetorno;
    }
    
    public void RespuestaTransaccionWyrf.setCodigoRetorno(String codigoRetorno) {
        this.codigoRetorno = codigoRetorno;
    }
    
    public String RespuestaTransaccionWyrf.getCodigoControl() {
        return this.codigoControl;
    }
    
    public void RespuestaTransaccionWyrf.setCodigoControl(String codigoControl) {
        this.codigoControl = codigoControl;
    }
    
    public void RespuestaTransaccionWyrf.setCuerpo(CuerpoMultiparte cuerpo) {
        this.cuerpo = cuerpo;
    }
    
}
