package com.bbva.mzic.accounts.dao.model.mbgdtcon_1;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * <p>
 * Bean fila para el campo tabular
 * <code>accountReconciliationRegistersList</code>, utilizado por la clase
 * <code>RespuestaTransaccionMbgdtcon_1</code>
 * </p>
 *
 * @see RespuestaTransaccionMbgdtcon_1
 *
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Accountreconciliationregisterslist {

	/**
	 * <p>
	 * Campo <code>accountType</code>, &iacute;ndice: <code>1</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1,
			nombre = "accountType",
			tipo = TipoCampo.ALFANUMERICO,
			longitudMaxima = 10,
			signo = true)
	private String accounttype;

	/**
	 * <p>
	 * Campo <code>triggerAccountKey</code>, &iacute;ndice: <code>2</code>,
	 * tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2,
			nombre = "triggerAccountKey",
			tipo = TipoCampo.ALFANUMERICO,
			longitudMaxima = 2,
			signo = true)
	private String triggeraccountkey;

	/**
	 * <p>
	 * Campo <code>accountOrSubaccount</code>, &iacute;ndice: <code>3</code>,
	 * tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3,
			nombre = "accountOrSubaccount",
			tipo = TipoCampo.ALFANUMERICO,
			longitudMaxima = 10,
			signo = true)
	private String accountorsubaccount;

	/**
	 * <p>
	 * Campo <code>operationNumberOfAccountOrSubaccount</code>, &iacute;ndice:
	 * <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4,
			nombre = "operationNumberOfAccountOrSubaccount",
			tipo = TipoCampo.ALFANUMERICO,
			longitudMaxima = 9,
			signo = true)
	private String operationnumberofaccountorsubaccount;

	/**
	 * <p>
	 * Campo <code>targetSubaccount</code>, &iacute;ndice: <code>5</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5,
			nombre = "targetSubaccount",
			tipo = TipoCampo.ALFANUMERICO,
			longitudMaxima = 10,
			signo = true)
	private String targetsubaccount;

	/**
	 * <p>
	 * Campo <code>operationNumberOfTargetSubaccount</code>, &iacute;ndice:
	 * <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6,
			nombre = "operationNumberOfTargetSubaccount",
			tipo = TipoCampo.ALFANUMERICO,
			longitudMaxima = 9,
			signo = true)
	private String operationnumberoftargetsubaccount;

	/**
	 * <p>
	 * Campo <code>payerSubaccount</code>, &iacute;ndice: <code>7</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7,
			nombre = "payerSubaccount",
			tipo = TipoCampo.ALFANUMERICO,
			longitudMaxima = 10,
			signo = true)
	private String payersubaccount;

	/**
	 * <p>
	 * Campo <code>operationNumberOfPayerSubaccount</code>, &iacute;ndice:
	 * <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8,
			nombre = "operationNumberOfPayerSubaccount",
			tipo = TipoCampo.ALFANUMERICO,
			longitudMaxima = 9,
			signo = true)
	private String operationnumberofpayersubaccount;

	/**
	 * <p>
	 * Campo <code>description</code>, &iacute;ndice: <code>9</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9,
			nombre = "description",
			tipo = TipoCampo.ALFANUMERICO,
			longitudMaxima = 25,
			signo = true)
	private String description;

	/**
	 * <p>
	 * Campo <code>reference</code>, &iacute;ndice: <code>10</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 10,
			nombre = "reference",
			tipo = TipoCampo.ALFANUMERICO,
			longitudMaxima = 15,
			signo = true)
	private String reference;

	/**
	 * <p>
	 * Campo <code>extendedReference</code>, &iacute;ndice: <code>11</code>,
	 * tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 11,
			nombre = "extendedReference",
			tipo = TipoCampo.ALFANUMERICO,
			longitudMaxima = 37,
			signo = true)
	private String extendedreference;

	/**
	 * <p>
	 * Campo <code>operationAmount</code>, &iacute;ndice: <code>12</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 12,
			nombre = "operationAmount",
			tipo = TipoCampo.ALFANUMERICO,
			longitudMaxima = 18,
			signo = true)
	private String operationamount;

	/**
	 * <p>
	 * Campo <code>operationDate</code>, &iacute;ndice: <code>13</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 13,
			nombre = "operationDate",
			tipo = TipoCampo.ALFANUMERICO,
			longitudMaxima = 10,
			signo = true)
	private String operationdate;

	/**
	 * <p>
	 * Campo <code>operationTimestamp</code>, &iacute;ndice: <code>14</code>,
	 * tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 14,
			nombre = "operationTimestamp",
			tipo = TipoCampo.ALFANUMERICO,
			longitudMaxima = 28,
			signo = true)
	private String operationtimestamp;

	/**
	 * <p>
	 * Campo <code>concentrationAccount</code>, &iacute;ndice: <code>15</code>,
	 * tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 15,
			nombre = "concentrationAccount",
			tipo = TipoCampo.ALFANUMERICO,
			longitudMaxima = 10,
			signo = true)
	private String concentrationaccount;

	/**
	 * <p>
	 * Campo <code>operationCurrency</code>, &iacute;ndice: <code>16</code>,
	 * tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 16,
			nombre = "operationCurrency",
			tipo = TipoCampo.ALFANUMERICO,
			longitudMaxima = 3,
			signo = true)
	private String operationcurrency;

}
