// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.apx.mtkdt019_1;

import com.bbva.mzic.accounts.dao.model.apx.mtkdt019_1.Paginationin;
import java.lang.String;
import java.util.List;

privileged aspect PeticionTransaccionMtkdt019_1_Roo_JavaBean {
    
    public String PeticionTransaccionMtkdt019_1.getAccountnumber() {
        return this.accountnumber;
    }
    
    public void PeticionTransaccionMtkdt019_1.setAccountnumber(String accountnumber) {
        this.accountnumber = accountnumber;
    }
    
    public String PeticionTransaccionMtkdt019_1.getHolderfirstname() {
        return this.holderfirstname;
    }
    
    public void PeticionTransaccionMtkdt019_1.setHolderfirstname(String holderfirstname) {
        this.holderfirstname = holderfirstname;
    }
    
    public String PeticionTransaccionMtkdt019_1.getHolderlastname() {
        return this.holderlastname;
    }
    
    public void PeticionTransaccionMtkdt019_1.setHolderlastname(String holderlastname) {
        this.holderlastname = holderlastname;
    }
    
    public String PeticionTransaccionMtkdt019_1.getHoldersecondlastname() {
        return this.holdersecondlastname;
    }
    
    public void PeticionTransaccionMtkdt019_1.setHoldersecondlastname(String holdersecondlastname) {
        this.holdersecondlastname = holdersecondlastname;
    }
    
    public String PeticionTransaccionMtkdt019_1.getCurrencyid() {
        return this.currencyid;
    }
    
    public void PeticionTransaccionMtkdt019_1.setCurrencyid(String currencyid) {
        this.currencyid = currencyid;
    }
    
    public List<Paginationin> PeticionTransaccionMtkdt019_1.getPaginationin() {
        return this.paginationin;
    }
    
    public void PeticionTransaccionMtkdt019_1.setPaginationin(List<Paginationin> paginationin) {
        this.paginationin = paginationin;
    }
    
}
