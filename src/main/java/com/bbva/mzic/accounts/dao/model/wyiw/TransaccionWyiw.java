package com.bbva.mzic.accounts.dao.model.wyiw;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>WYIW</code>
 * 
 * @see PeticionTransaccionWyiw
 * @see RespuestaTransaccionWyiw
 */
@Component
public class TransaccionWyiw implements InvocadorTransaccion<PeticionTransaccionWyiw, RespuestaTransaccionWyiw> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionWyiw invocar(PeticionTransaccionWyiw transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionWyiw.class, RespuestaTransaccionWyiw.class, transaccion);
	}

	@Override
	public RespuestaTransaccionWyiw invocarCache(PeticionTransaccionWyiw transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionWyiw.class, RespuestaTransaccionWyiw.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionWyiw.class, "vaciearCache");
	}
}
