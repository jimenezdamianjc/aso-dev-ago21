package com.bbva.mzic.accounts.dao.model.mtkdt052_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MTKDT052</code>
 *
 * @see PeticionTransaccionMtkdt052_1
 * @see RespuestaTransaccionMtkdt052_1
 */
@Component
public class TransaccionMtkdt052_1 implements InvocadorTransaccion<PeticionTransaccionMtkdt052_1, RespuestaTransaccionMtkdt052_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMtkdt052_1 invocar(final PeticionTransaccionMtkdt052_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt052_1.class, RespuestaTransaccionMtkdt052_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMtkdt052_1 invocarCache(final PeticionTransaccionMtkdt052_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt052_1.class, RespuestaTransaccionMtkdt052_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMtkdt052_1.class, "vaciearCache");
	}
}
