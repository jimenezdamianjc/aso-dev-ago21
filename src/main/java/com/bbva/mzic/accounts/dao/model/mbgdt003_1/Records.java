package com.bbva.mzic.accounts.dao.model.mbgdt003_1;

import java.math.BigDecimal;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Bean fila para el campo tabular <code>records</code>, utilizado por la clase
 * <code>RespuestaTransaccionMbgdt003_1</code>
 * </p>
 *
 * @see RespuestaTransaccionMbgdt003_1
 *
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Records {

    /**
     * <p>
     * Campo <code>id</code>, &iacute;ndice: <code>1</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 1, nombre = "id", tipo = TipoCampo.ENTERO, longitudMaxima = 13, signo = true)
    private Long id;

    /**
     * <p>
     * Campo <code>localAmount</code>, &iacute;ndice: <code>2</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 2, nombre = "localAmount", tipo = TipoCampo.DECIMAL, longitudMaxima = 16, signo = true)
    private BigDecimal localamount;

    /**
     * <p>
     * Campo <code>localAmountCurrency</code>, &iacute;ndice: <code>3</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "localAmountCurrency", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 3, signo = true)
    private String localamountcurrency;

    /**
     * <p>
     * Campo <code>moneyFlowId</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "moneyFlowId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 1, signo = true)
    private String moneyflowid;

    /**
     * <p>
     * Campo <code>concept</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "concept", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 25, signo = true)
    private String concept;

    /**
     * <p>
     * Campo <code>transactionTypeId</code>, &iacute;ndice: <code>6</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "transactionTypeId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 15, signo = true)
    private String transactiontypeid;

    /**
     * <p>
     * Campo <code>transactionTypeName</code>, &iacute;ndice: <code>7</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 7, nombre = "transactionTypeName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 8, signo = true)
    private String transactiontypename;

    /**
     * <p>
     * Campo <code>transactionTypeInternalCode</code>, &iacute;ndice: <code>8</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 8, nombre = "transactionTypeInternalCode", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 3, signo = true)
    private String transactiontypeinternalcode;

    /**
     * <p>
     * Campo <code>transactionTypeInternalCodeName</code>, &iacute;ndice: <code>9</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 9, nombre = "transactionTypeInternalCodeName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 34, signo = true)
    private String transactiontypeinternalcodename;

    /**
     * <p>
     * Campo <code>operationDate</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 10, nombre = "operationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 29, signo = true)
    private String operationdate;

    /**
     * <p>
     * Campo <code>valuationDate</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 11, nombre = "valuationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 29, signo = true)
    private String valuationdate;

    /**
     * <p>
     * Campo <code>accountedDate</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 12, nombre = "accountedDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 29, signo = true)
    private String accounteddate;

    /**
     * <p>
     * Campo <code>financingTypeId</code>, &iacute;ndice: <code>13</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 13, nombre = "financingTypeId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 13, signo = true)
    private String financingtypeid;

    /**
     * <p>
     * Campo <code>statusId</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 14, nombre = "statusId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 7, signo = true)
    private String statusid;

    /**
     * <p>
     * Campo <code>contractNumberTypeId</code>, &iacute;ndice: <code>15</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 15, nombre = "contractNumberTypeId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 3, signo = true)
    private String contractnumbertypeid;

    /**
     * <p>
     * Campo <code>contractProductId</code>, &iacute;ndice: <code>16</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 16, nombre = "contractProductId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true)
    private String contractproductid;

    /**
     * <p>
     * Campo <code>contractProductName</code>, &iacute;ndice: <code>17</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 17, nombre = "contractProductName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true)
    private String contractproductname;

    /**
     * <p>
     * Campo <code>contractAlias</code>, &iacute;ndice: <code>18</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 18, nombre = "contractAlias", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true)
    private String contractalias;

    /**
     * <p>
     * Campo <code>additionalInformationReference</code>, &iacute;ndice: <code>19</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 19, nombre = "additionalInformationReference", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 25, signo = true)
    private String additionalinformationreference;

    /**
     * <p>
     * Campo <code>additionalInformationAdditionalData</code>, &iacute;ndice: <code>20</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 20, nombre = "additionalInformationAdditionalData", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 40, signo = true)
    private String additionalinformationadditionaldata;

    /**
     * <p>
     * Campo <code>operationBalance</code>, &iacute;ndice: <code>21</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 21, nombre = "operationBalance", tipo = TipoCampo.DECIMAL, longitudMaxima = 16, signo = true)
    private BigDecimal operationbalance;

    /**
     * <p>
     * Campo <code>legendCode</code>, &iacute;ndice: <code>22</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 22, nombre = "legendCode", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 3, signo = true)
    private String legendcode;

}
