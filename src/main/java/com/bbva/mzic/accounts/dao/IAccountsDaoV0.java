package com.bbva.mzic.accounts.dao;

import java.util.List;

import com.bbva.mzic.accounts.business.dto.DtoIntAggregatedAvailableBalance;
import com.bbva.mzic.accounts.business.dto.DtoIntConditions;
import com.bbva.mzic.accounts.business.dto.DtoIntFilterAccount;

public interface IAccountsDaoV0 {

	DtoIntAggregatedAvailableBalance getAccountBGL4(DtoIntFilterAccount dtoIntFilterAccount);

	List<DtoIntConditions> getListConditionsBGL5(DtoIntFilterAccount dtoIntFilterAccount);

}
