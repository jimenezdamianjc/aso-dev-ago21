package com.bbva.mzic.accounts.dao.model.b459;


import java.math.BigDecimal;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>BGM459</code> de la transacci&oacute;n <code>B459</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BGM459")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoBGM459 {

    /**
     * <p>
     * Campo <code>CUENTA</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "CUENTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String cuenta;

    /**
     * <p>
     * Campo <code>PRICHEQ</code>, &iacute;ndice: <code>2</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 2, nombre = "PRICHEQ", tipo = TipoCampo.ENTERO, longitudMinima = 9, longitudMaxima = 9)
    private Integer pricheq;

    /**
     * <p>
     * Campo <code>ULTCHEQ</code>, &iacute;ndice: <code>3</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 3, nombre = "ULTCHEQ", tipo = TipoCampo.ENTERO, longitudMinima = 9, longitudMaxima = 9)
    private Integer ultcheq;

    /**
     * <p>
     * Campo <code>APLIORI</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "APLIORI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
    private String apliori;

    /**
     * <p>
     * Campo <code>USUARIO</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "USUARIO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
    private String usuario;

    /**
     * <p>
     * Campo <code>OPCION</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "OPCION", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String opcion;

    /**
     * <p>
     * Campo <code>IMPORTE</code>, &iacute;ndice: <code>7</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 7, nombre = "IMPORTE", tipo = TipoCampo.DECIMAL, longitudMinima = 15, longitudMaxima = 15, decimales = 2)
    private BigDecimal importe;

}
