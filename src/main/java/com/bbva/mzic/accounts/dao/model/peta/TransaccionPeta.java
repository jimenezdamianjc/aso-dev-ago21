package com.bbva.mzic.accounts.dao.model.peta;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>PETA</code>
 * 
 * @see PeticionTransaccionPeta
 * @see RespuestaTransaccionPeta
 */
@Component
public class TransaccionPeta implements InvocadorTransaccion<PeticionTransaccionPeta, RespuestaTransaccionPeta> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionPeta invocar(PeticionTransaccionPeta transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionPeta.class, RespuestaTransaccionPeta.class, transaccion);
	}

	@Override
	public RespuestaTransaccionPeta invocarCache(PeticionTransaccionPeta transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionPeta.class, RespuestaTransaccionPeta.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionPeta.class, "vaciearCache");
	}
}
