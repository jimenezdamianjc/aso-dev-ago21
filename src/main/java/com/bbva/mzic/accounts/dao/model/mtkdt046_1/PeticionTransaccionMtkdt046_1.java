package com.bbva.mzic.accounts.dao.model.mtkdt046_1;

import java.util.List;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

/**
 * <p>
 * Transacci&oacute;n <code>MTKDT046</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMtkdt046_1</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionMtkdt046_1</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: MTKDT046.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot;
 * transactionName=&quot;MTKDT046&quot; application=&quot;MTKD&quot; version=&quot;01&quot; country=&quot;MX&quot;
 * language=&quot;EN&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;accountNumber&quot; type=&quot;String&quot; size=&quot;6&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;group name=&quot;paginationIn&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;pageSize&quot; type=&quot;String&quot; size=&quot;4&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;paginationKey&quot; type=&quot;String&quot;
 * size=&quot;4&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;group name=&quot;alert&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;4&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;type&quot; type=&quot;String&quot; size=&quot;90&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;reason&quot; type=&quot;String&quot; size=&quot;100&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;date&quot; type=&quot;String&quot; size=&quot;10&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;/group&gt;
 * &lt;group name=&quot;paginationOut&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;hasMoreData&quot; type=&quot;String&quot; size=&quot;1&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;paginationKey&quot; type=&quot;String&quot; size=&quot;4&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Transaccion encargada de traer un listado de alertamientos
 * acerca de la cuenta CED&lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionMtkdt046_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "MTKDT046", tipo = 1, subtipo = 1, version = 1, configuracion = "default_apx",
        respuesta = RespuestaTransaccionMtkdt046_1.class, atributos = {@Atributo(nombre = "country", valor = "MX")})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMtkdt046_1 {

    /**
     * <p>
     * Campo <code>accountNumber</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "accountNumber", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 6, signo = true, obligatorio = true)
    private String accountnumber;

    /**
     * <p>
     * Campo <code>paginationIn</code>, &iacute;ndice: <code>2</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 2, nombre = "paginationIn", tipo = TipoCampo.TABULAR)
    private List<Paginationin> paginationin;

}
