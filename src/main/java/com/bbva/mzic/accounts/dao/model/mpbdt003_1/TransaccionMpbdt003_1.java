package com.bbva.mzic.accounts.dao.model.mpbdt003_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MPBDT003</code>
 * 
 * @see PeticionTransaccionMpbdt003_1
 * @see RespuestaTransaccionMpbdt003_1
 */
@Component
public class TransaccionMpbdt003_1 implements InvocadorTransaccion<PeticionTransaccionMpbdt003_1, RespuestaTransaccionMpbdt003_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMpbdt003_1 invocar(PeticionTransaccionMpbdt003_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpbdt003_1.class, RespuestaTransaccionMpbdt003_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMpbdt003_1 invocarCache(PeticionTransaccionMpbdt003_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMpbdt003_1.class, RespuestaTransaccionMpbdt003_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMpbdt003_1.class, "vaciearCache");
	}
}
