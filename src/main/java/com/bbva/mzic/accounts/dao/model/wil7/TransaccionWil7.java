package com.bbva.mzic.accounts.dao.model.wil7;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>WIL7</code>
 * 
 * @see PeticionTransaccionWil7
 * @see RespuestaTransaccionWil7
 */
@Component("transaccionWil7-v0")
public class TransaccionWil7 implements InvocadorTransaccion<PeticionTransaccionWil7, RespuestaTransaccionWil7> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionWil7 invocar(PeticionTransaccionWil7 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionWil7.class, RespuestaTransaccionWil7.class, transaccion);
	}

	@Override
	public RespuestaTransaccionWil7 invocarCache(PeticionTransaccionWil7 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionWil7.class, RespuestaTransaccionWil7.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionWil7.class, "vaciearCache");
	}
}
