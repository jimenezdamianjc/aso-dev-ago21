package com.bbva.mzic.accounts.dao.model.b458;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>B458</code>
 * 
 * @see PeticionTransaccionB458
 * @see RespuestaTransaccionB458
 */
@Component
public class TransaccionB458 implements InvocadorTransaccion<PeticionTransaccionB458, RespuestaTransaccionB458> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionB458 invocar(PeticionTransaccionB458 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionB458.class, RespuestaTransaccionB458.class, transaccion);
	}

	@Override
	public RespuestaTransaccionB458 invocarCache(PeticionTransaccionB458 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionB458.class, RespuestaTransaccionB458.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionB458.class, "vaciearCache");
	}
}
