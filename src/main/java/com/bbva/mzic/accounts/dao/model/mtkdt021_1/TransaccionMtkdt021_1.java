package com.bbva.mzic.accounts.dao.model.mtkdt021_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MTKDT021</code>
 * 
 * @see PeticionTransaccionMtkdt021_1
 * @see RespuestaTransaccionMtkdt021_1
 */
@Component("transaccion-mtkdt021")
public class TransaccionMtkdt021_1 implements InvocadorTransaccion<PeticionTransaccionMtkdt021_1, RespuestaTransaccionMtkdt021_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMtkdt021_1 invocar(PeticionTransaccionMtkdt021_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt021_1.class, RespuestaTransaccionMtkdt021_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMtkdt021_1 invocarCache(PeticionTransaccionMtkdt021_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt021_1.class, RespuestaTransaccionMtkdt021_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMtkdt021_1.class, "vaciearCache");
	}
}
