package com.bbva.mzic.accounts.dao.model.mchqt003_1;

privileged aspect RespuestaTransaccionMchqt003_1_Roo_JavaBean {

	public String RespuestaTransaccionMchqt003_1.getCodigoAviso() {
		return this.codigoAviso;
	}

	public void RespuestaTransaccionMchqt003_1.setCodigoAviso(String codigoAviso) {
		this.codigoAviso = codigoAviso;
	}

	public String RespuestaTransaccionMchqt003_1.getDescripcionAviso() {
		return this.descripcionAviso;
	}

	public void RespuestaTransaccionMchqt003_1.setDescripcionAviso(String descripcionAviso) {
		this.descripcionAviso = descripcionAviso;
	}

	public String RespuestaTransaccionMchqt003_1.getAplicacionAviso() {
		return this.aplicacionAviso;
	}

	public void RespuestaTransaccionMchqt003_1.setAplicacionAviso(String aplicacionAviso) {
		this.aplicacionAviso = aplicacionAviso;
	}

	public String RespuestaTransaccionMchqt003_1.getCodigoRetorno() {
		return this.codigoRetorno;
	}

	public void RespuestaTransaccionMchqt003_1.setCodigoRetorno(String codigoRetorno) {
		this.codigoRetorno = codigoRetorno;
	}

	public String RespuestaTransaccionMchqt003_1.getMessageresponse() {
		return this.messageresponse;
	}

	public void RespuestaTransaccionMchqt003_1.setMessageresponse(String messageresponse) {
		this.messageresponse = messageresponse;
	}

	public Long RespuestaTransaccionMchqt003_1.getIncomeid() {
		return this.incomeid;
	}

	public void RespuestaTransaccionMchqt003_1.setIncomeid(Long incomeid) {
		this.incomeid = incomeid;
	}

}
