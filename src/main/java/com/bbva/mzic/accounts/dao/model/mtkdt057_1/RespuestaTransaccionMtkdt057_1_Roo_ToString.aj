// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.mtkdt057_1;

import java.lang.String;

privileged aspect RespuestaTransaccionMtkdt057_1_Roo_ToString {
    
    public String RespuestaTransaccionMtkdt057_1.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AplicacionAviso: ").append(getAplicacionAviso()).append(", ");
        sb.append("CodigoAviso: ").append(getCodigoAviso()).append(", ");
        sb.append("CodigoRetorno: ").append(getCodigoRetorno()).append(", ");
        sb.append("DescripcionAviso: ").append(getDescripcionAviso()).append(", ");
        sb.append("Ticketid: ").append(getTicketid());
        return sb.toString();
    }
    
}
