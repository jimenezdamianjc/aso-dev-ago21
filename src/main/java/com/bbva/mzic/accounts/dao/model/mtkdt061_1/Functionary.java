package com.bbva.mzic.accounts.dao.model.mtkdt061_1;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * <p>Bean fila para el campo tabular <code>functionary</code>, utilizado por la clase <code>Datareport</code></p>
 * 
 * @see Datareport
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Functionary {
	
	/**
	 * <p>Campo <code>functionaryId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "functionaryId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true)
	private String functionaryid;
	
	/**
	 * <p>Campo <code>functionaryName</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "functionaryName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 90, signo = true)
	private String functionaryname;
	
	/**
	 * <p>Campo <code>functionaryType</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "functionaryType", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true)
	private String functionarytype;
	
	/**
	 * <p>Campo <code>functionaryDescription</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "functionaryDescription", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 90, signo = true)
	private String functionarydescription;
	
}
