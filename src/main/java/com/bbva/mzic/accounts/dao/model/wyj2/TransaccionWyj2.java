package com.bbva.mzic.accounts.dao.model.wyj2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>WYJ2</code>
 * 
 * @see PeticionTransaccionWyj2
 * @see RespuestaTransaccionWyj2
 */
@Component
public class TransaccionWyj2 implements InvocadorTransaccion<PeticionTransaccionWyj2, RespuestaTransaccionWyj2> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionWyj2 invocar(PeticionTransaccionWyj2 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionWyj2.class, RespuestaTransaccionWyj2.class, transaccion);
	}

	@Override
	public RespuestaTransaccionWyj2 invocarCache(PeticionTransaccionWyj2 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionWyj2.class, RespuestaTransaccionWyj2.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionWyj2.class, "vaciearCache");
	}
}
