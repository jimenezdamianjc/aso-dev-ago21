package com.bbva.mzic.accounts.dao.model.mtkdt032_1;


import java.math.BigDecimal;
import java.util.List;
import com.bbva.jee.arq.spring.core.host.Cabecera;
import com.bbva.jee.arq.spring.core.host.NombreCabecera;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.RespuestaTransaccion;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Bean de respuesta para la transacci&oacute;n <code>MTKDT032</code>
 * 
 * @see PeticionTransaccionMtkdt032_1
 */
@RespuestaTransaccion
@Formato(nombre = "1")
@RooJavaBean
@RooToString
@RooSerializable
public class RespuestaTransaccionMtkdt032_1 {
	
	/**
	 * <p>Cabecera <code>COD-AVISO</code></p>
	 */
	@Cabecera(nombre=NombreCabecera.CODIGO_AVISO)
	private String codigoAviso;
	
	/**
	 * <p>Cabecera <code>DES-AVISO</code></p>
	 */
	@Cabecera(nombre=NombreCabecera.DESCRIPCION_AVISO)
	private String descripcionAviso;
	
	/**
	 * <p>Cabecera <code>COD-UUAA-AVISO</code></p>
	 */
	@Cabecera(nombre=NombreCabecera.APLICACION_AVISO)
	private String aplicacionAviso;
	
	/**
	 * <p>Cabecera <code>COD-RETORNO</code></p>
	 */
	@Cabecera(nombre=NombreCabecera.CODIGO_RETORNO)
	private String codigoRetorno;
	
	/**
	 * <p>Campo <code>functionaryId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "functionaryId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
	private String functionaryid;
	
	/**
	 * <p>Campo <code>functionaryName</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "functionaryName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 90, signo = true)
	private String functionaryname;
	
	/**
	 * <p>Campo <code>bankId</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "bankId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true, obligatorio = true)
	private String bankid;
	
	/**
	 * <p>Campo <code>bankName</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "bankName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 13, signo = true)
	private String bankname;
	
	/**
	 * <p>Campo <code>branchId</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "branchId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true, obligatorio = true)
	private String branchid;
	
	/**
	 * <p>Campo <code>branchName</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "branchName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 13, signo = true)
	private String branchname;
	
	/**
	 * <p>Campo <code>withdrawalAmount</code>, &iacute;ndice: <code>7</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 7, nombre = "withdrawalAmount", tipo = TipoCampo.DECIMAL, longitudMaxima = 14, signo = true, obligatorio = true)
	private BigDecimal withdrawalamount;
	
	/**
	 * <p>Campo <code>currency</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "currency", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 3, signo = true, obligatorio = true)
	private String currency;
	
	/**
	 * <p>Campo <code>withdrawalMainReference</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "withdrawalMainReference", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 100, signo = true, obligatorio = true)
	private String withdrawalmainreference;
	
	/**
	 * <p>Campo <code>withdrawalCreationDate</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 10, nombre = "withdrawalCreationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
	private String withdrawalcreationdate;
	
	/**
	 * <p>Campo <code>withdrawalReason</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 11, nombre = "withdrawalReason", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 100, signo = true)
	private String withdrawalreason;
	
	/**
	 * <p>Campo <code>withdrawalTypeId</code>, &iacute;ndice: <code>12</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 12, nombre = "withdrawalTypeId", tipo = TipoCampo.ENTERO, longitudMaxima = 5, signo = true, obligatorio = true)
	private int withdrawaltypeid;
	
	/**
	 * <p>Campo <code>withdrawalTypeDesc</code>, &iacute;ndice: <code>13</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 13, nombre = "withdrawalTypeDesc", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 40, signo = true, obligatorio = true)
	private String withdrawaltypedesc;
	
	/**
	 * <p>Campo <code>withdrawalDocumentId</code>, &iacute;ndice: <code>14</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 14, nombre = "withdrawalDocumentId", tipo = TipoCampo.ENTERO, longitudMaxima = 5, signo = true, obligatorio = true)
	private int withdrawaldocumentid;
	
	/**
	 * <p>Campo <code>withdrawalDocumentDesc</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 15, nombre = "withdrawalDocumentDesc", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 40, signo = true, obligatorio = true)
	private String withdrawaldocumentdesc;
	
	/**
	 * <p>Campo <code>withdrawalApplicationDate</code>, &iacute;ndice: <code>16</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 16, nombre = "withdrawalApplicationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
	private String withdrawalapplicationdate;
	
	/**
	 * <p>Campo <code>references</code>, &iacute;ndice: <code>17</code>, tipo: <code>TABULAR</code>
	 */
	@Campo(indice = 17, nombre = "references", tipo = TipoCampo.TABULAR)
	private List<References> references;
	
}
