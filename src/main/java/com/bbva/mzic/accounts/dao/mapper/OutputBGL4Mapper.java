package com.bbva.mzic.accounts.dao.mapper;

import java.util.stream.Collectors;

import com.bbva.jee.arq.spring.core.host.IMapper;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.mzic.accounts.business.dto.DtoIntAggregatedAvailableBalance;
import com.bbva.mzic.accounts.dao.model.bgl4.FormatoBGML4TS;
import com.bbva.mzic.accounts.dao.model.bgl4.RespuestaTransaccionBgl4;

public class OutputBGL4Mapper implements IMapper<RespuestaTransaccionBgl4, DtoIntAggregatedAvailableBalance> {

	@Override
	public DtoIntAggregatedAvailableBalance map(RespuestaTransaccionBgl4 response) {

		if (response == null) {
			return null;
		}

		response.getCuerpo().getPartes().stream().filter(cs -> cs != null && cs.getClass().equals(CopySalida.class))
				.map(cs -> (CopySalida) cs).map(CopySalida::getCopy).collect(Collectors.toList());

		new FormatoBGML4TS();

		return null;
	}

}
