package com.bbva.mzic.accounts.dao.model.apx.mbgdt007_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MBGDT007</code>
 * 
 * @see PeticionTransaccionMbgdt007_1
 * @see RespuestaTransaccionMbgdt007_1
 */
@Component
public class TransaccionMbgdt007_1 implements InvocadorTransaccion<PeticionTransaccionMbgdt007_1, RespuestaTransaccionMbgdt007_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMbgdt007_1 invocar(PeticionTransaccionMbgdt007_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMbgdt007_1.class, RespuestaTransaccionMbgdt007_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMbgdt007_1 invocarCache(PeticionTransaccionMbgdt007_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMbgdt007_1.class, RespuestaTransaccionMbgdt007_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMbgdt007_1.class, "vaciearCache");
	}
}
