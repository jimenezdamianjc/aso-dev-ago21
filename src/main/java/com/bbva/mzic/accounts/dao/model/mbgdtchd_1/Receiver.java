package com.bbva.mzic.accounts.dao.model.mbgdtchd_1;

import java.util.List;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Bean fila para el campo tabular <code>receiver</code>, utilizado por la clase
 * <code>Digitalchecks</code>
 * </p>
 * 
 * @see Digitalchecks
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Receiver {

    /**
     * <p>
     * Campo <code>value</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "value", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
    private String value;

    /**
     * <p>
     * Campo <code>holder</code>, &iacute;ndice: <code>2</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 2, nombre = "holder", tipo = TipoCampo.TABULAR)
    private List<Holder> holder;

}
