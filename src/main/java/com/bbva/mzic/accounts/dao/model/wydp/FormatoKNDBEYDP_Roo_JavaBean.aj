// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.wydp;

import java.lang.String;

privileged aspect FormatoKNDBEYDP_Roo_JavaBean {
    
    public String FormatoKNDBEYDP.getNumclie() {
        return this.numclie;
    }
    
    public void FormatoKNDBEYDP.setNumclie(String numclie) {
        this.numclie = numclie;
    }
    
    public String FormatoKNDBEYDP.getTipcuen() {
        return this.tipcuen;
    }
    
    public void FormatoKNDBEYDP.setTipcuen(String tipcuen) {
        this.tipcuen = tipcuen;
    }
    
    public String FormatoKNDBEYDP.getNumcuen() {
        return this.numcuen;
    }
    
    public void FormatoKNDBEYDP.setNumcuen(String numcuen) {
        this.numcuen = numcuen;
    }
    
    public String FormatoKNDBEYDP.getAlias() {
        return this.alias;
    }
    
    public void FormatoKNDBEYDP.setAlias(String alias) {
        this.alias = alias;
    }
    
}
