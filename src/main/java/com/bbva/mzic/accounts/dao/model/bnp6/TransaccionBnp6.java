package com.bbva.mzic.accounts.dao.model.bnp6;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>BNP6</code>
 * 
 * @see PeticionTransaccionBnp6
 * @see RespuestaTransaccionBnp6
 */
@Component
public class TransaccionBnp6 implements InvocadorTransaccion<PeticionTransaccionBnp6, RespuestaTransaccionBnp6> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionBnp6 invocar(PeticionTransaccionBnp6 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionBnp6.class, RespuestaTransaccionBnp6.class, transaccion);
	}

	@Override
	public RespuestaTransaccionBnp6 invocarCache(PeticionTransaccionBnp6 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionBnp6.class, RespuestaTransaccionBnp6.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionBnp6.class, "vaciearCache");
	}
}
