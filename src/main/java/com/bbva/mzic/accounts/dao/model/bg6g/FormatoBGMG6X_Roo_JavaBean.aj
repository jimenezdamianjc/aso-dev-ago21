// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.bg6g;

import java.lang.String;

privileged aspect FormatoBGMG6X_Roo_JavaBean {
    
    public String FormatoBGMG6X.getCuenta() {
        return this.cuenta;
    }
    
    public void FormatoBGMG6X.setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }
    
    public String FormatoBGMG6X.getOpcion() {
        return this.opcion;
    }
    
    public void FormatoBGMG6X.setOpcion(String opcion) {
        this.opcion = opcion;
    }
    
}
