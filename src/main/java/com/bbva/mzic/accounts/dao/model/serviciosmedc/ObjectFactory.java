
package com.bbva.mzic.accounts.dao.model.serviciosmedc;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the
 * com.bbva.mzic.accounts.dao.model.serviciosmedc package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

	/**
	 * Create a new ObjectFactory that can be used to create new instances of schema
	 * derived classes for package: com.bbva.mzic.accounts.dao.model.serviciosmedc
	 * 
	 */
	public ObjectFactory() {
		super();
	}

	/**
	 * Create an instance of {@link GetPDFG22Response }
	 * 
	 */
	public GetPDFG22Response createGetPDFG22Response() {
		return new GetPDFG22Response();
	}

	/**
	 * Create an instance of {@link EnvioMailRUC }
	 * 
	 */
	public EnvioMailRUC createEnvioMailRUC() {
		return new EnvioMailRUC();
	}

	/**
	 * Create an instance of {@link ArrayOf17917237521440052060NillableString }
	 * 
	 */
	public ArrayOf17917237521440052060NillableString createArrayOf17917237521440052060NillableString() {
		return new ArrayOf17917237521440052060NillableString();
	}

	/**
	 * Create an instance of {@link EnvioMailRUCResponse }
	 * 
	 */
	public EnvioMailRUCResponse createEnvioMailRUCResponse() {
		return new EnvioMailRUCResponse();
	}

	/**
	 * Create an instance of {@link GetGeneralDocument }
	 * 
	 */
	public GetGeneralDocument createGetGeneralDocument() {
		return new GetGeneralDocument();
	}

	/**
	 * Create an instance of {@link ArrayOfElemento }
	 * 
	 */
	public ArrayOfElemento createArrayOfElemento() {
		return new ArrayOfElemento();
	}

	/**
	 * Create an instance of {@link GetGeneralDocumentResponse }
	 * 
	 */
	public GetGeneralDocumentResponse createGetGeneralDocumentResponse() {
		return new GetGeneralDocumentResponse();
	}

	/**
	 * Create an instance of {@link SendGenericMail }
	 * 
	 */
	public SendGenericMail createSendGenericMail() {
		return new SendGenericMail();
	}

	/**
	 * Create an instance of {@link ArrayOfDocumento }
	 * 
	 */
	public ArrayOfDocumento createArrayOfDocumento() {
		return new ArrayOfDocumento();
	}

	/**
	 * Create an instance of {@link ArrayOfDato }
	 * 
	 */
	public ArrayOfDato createArrayOfDato() {
		return new ArrayOfDato();
	}

	/**
	 * Create an instance of {@link SendGenericMailResponse }
	 * 
	 */
	public SendGenericMailResponse createSendGenericMailResponse() {
		return new SendGenericMailResponse();
	}

	/**
	 * Create an instance of {@link GetCatalogo }
	 * 
	 */
	public GetCatalogo createGetCatalogo() {
		return new GetCatalogo();
	}

	/**
	 * Create an instance of {@link GetCatalogoResponse }
	 * 
	 */
	public GetCatalogoResponse createGetCatalogoResponse() {
		return new GetCatalogoResponse();
	}

	/**
	 * Create an instance of {@link GetPDFG22 }
	 * 
	 */
	public GetPDFG22 createGetPDFG22() {
		return new GetPDFG22();
	}

	/**
	 * Create an instance of {@link Elemento }
	 * 
	 */
	public Elemento createElemento() {
		return new Elemento();
	}

	/**
	 * Create an instance of {@link Dato }
	 * 
	 */
	public Dato createDato() {
		return new Dato();
	}

	/**
	 * Create an instance of {@link Documento }
	 * 
	 */
	public Documento createDocumento() {
		return new Documento();
	}

}
