package com.bbva.mzic.accounts.dao.model.mtkdt053_1;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

/**
 * <p>Transacci&oacute;n <code>MTKDT053</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMtkdt053_1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMtkdt053_1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: MTKDT053-01-MX.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot;
 * transactionName=&quot;MTKDT053&quot; application=&quot;MTKD&quot; version=&quot;01&quot; country=&quot;MX&quot;
 * language=&quot;EN&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;ttypeid&quot; type=&quot;Long&quot; size=&quot;3&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;ticketId&quot; type=&quot;String&quot; size=&quot;28&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;opedate&quot; type=&quot;String&quot; size=&quot;10&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;accountId&quot; type=&quot;String&quot; size=&quot;6&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;ticketId&quot; type=&quot;String&quot; size=&quot;13&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;amount&quot; type=&quot;String&quot; size=&quot;14&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;reason&quot; type=&quot;String&quot; size=&quot;100&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;creationDate&quot; type=&quot;String&quot; size=&quot;19&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;applicationDate&quot; type=&quot;String&quot;
 * size=&quot;19&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;7&quot; name=&quot;updateDate&quot; type=&quot;String&quot; size=&quot;19&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;8&quot; name=&quot;typeId&quot; type=&quot;String&quot; size=&quot;5&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;9&quot; name=&quot;typeDescription&quot; type=&quot;String&quot;
 * size=&quot;50&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;10&quot; name=&quot;mainDescription&quot; type=&quot;String&quot;
 * size=&quot;50&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;12&quot; name=&quot;bankId&quot; type=&quot;String&quot; size=&quot;4&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;13&quot; name=&quot;bankName&quot; type=&quot;String&quot; size=&quot;20&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;14&quot; name=&quot;branchId&quot; type=&quot;String&quot; size=&quot;5&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;15&quot; name=&quot;branchName&quot; type=&quot;String&quot; size=&quot;35&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;16&quot; name=&quot;userId&quot; type=&quot;String&quot; size=&quot;10&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;17&quot; name=&quot;internalCodeId&quot; type=&quot;String&quot;
 * size=&quot;4&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;18&quot; name=&quot;lastUpdate&quot; type=&quot;String&quot; size=&quot;19&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;19&quot; name=&quot;origin&quot; type=&quot;String&quot; size=&quot;7&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;group name=&quot;references&quot; order=&quot;11&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;referencesId&quot; type=&quot;String&quot;
 * size=&quot;18&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;referencesName&quot; type=&quot;String&quot;
 * size=&quot;50&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;operativeDescription&quot; type=&quot;String&quot;
 * size=&quot;50&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;group name=&quot;businessAgent&quot; order=&quot;20&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;userId&quot; type=&quot;String&quot; size=&quot;13&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;userName&quot; type=&quot;String&quot; size=&quot;90&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;userType&quot; type=&quot;String&quot; size=&quot;20&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Transaccion utilizada para obtener el detalle de un retiro
 * por Comision en Ticket APX&lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionMtkdt053_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MTKDT053",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_apx",
	respuesta = RespuestaTransaccionMtkdt053_1.class,
	atributos = {@Atributo(nombre = "country", valor = "MX")}
)
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMtkdt053_1 {
		
		/**
	 * <p>Campo <code>ttypeid</code>, &iacute;ndice: <code>1</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 1, nombre = "ttypeid", tipo = TipoCampo.ENTERO, longitudMaxima = 3, signo = true)
	private Integer ttypeid;
	
	/**
	 * <p>Campo <code>ticketId</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "ticketId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 28, signo = true, obligatorio = true)
	private String ticketid;
	
	/**
	 * <p>Campo <code>opedate</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "opedate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true)
	private String opedate;
	
	/**
	 * <p>Campo <code>accountId</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "accountId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 6, signo = true, obligatorio = true)
	private String accountid;
	
}
