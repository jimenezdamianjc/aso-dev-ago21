package com.bbva.mzic.accounts.dao.model.bg40;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.mzic.serviceutils.rm.utils.tx.IFormat;


/**
 * Formato de datos <code>BGMF40A</code> de la transacci&oacute;n <code>BG40</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BGMF40A")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoBGMF40A implements IFormat {

    /**
     * <p>
     * Campo <code>CUENTA</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "CUENTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String cuenta;

    /**
     * <p>
     * Campo <code>SECUENC</code>, &iacute;ndice: <code>2</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 2, nombre = "SECUENC", tipo = TipoCampo.ENTERO, longitudMinima = 10, longitudMaxima = 10)
    private Long secuenc;

    /**
     * <p>
     * Campo <code>INDESTA</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "INDESTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String indesta;

    /**
     * <p>
     * Campo <code>CONTRAT</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "CONTRAT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String contrat;

}
