package com.bbva.mzic.accounts.dao.model.mtkdt055_1;

import java.util.List;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * <p>Bean fila para el campo tabular <code>functionary</code>, utilizado por la clase <code>Branch</code></p>
 * 
 * @see Branch
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Functionary {
	
	/**
	 * <p>Campo <code>functionaryId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "functionaryId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true, obligatorio = true)
	private String functionaryid;
	
	/**
	 * <p>Campo <code>functionaryName</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "functionaryName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 90, signo = true, obligatorio = true)
	private String functionaryname;
	
	/**
	 * <p>Campo <code>pldNationalStatus</code>, &iacute;ndice: <code>3</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 3, nombre = "pldNationalStatus", tipo = TipoCampo.ENTERO, longitudMaxima = 1, signo = true, obligatorio = true)
	private int pldnationalstatus;
	
	/**
	 * <p>Campo <code>pldInternationalStatus</code>, &iacute;ndice: <code>4</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 4, nombre = "pldInternationalStatus", tipo = TipoCampo.ENTERO, longitudMaxima = 1, signo = true, obligatorio = true)
	private int pldinternationalstatus;
	
	/**
	 * <p>Campo <code>totalAccountsByFunctionary</code>, &iacute;ndice: <code>5</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 5, nombre = "totalAccountsByFunctionary", tipo = TipoCampo.ENTERO, longitudMaxima = 5, signo = true, obligatorio = true)
	private int totalaccountsbyfunctionary;
	
	/**
	 * <p>Campo <code>account</code>, &iacute;ndice: <code>6</code>, tipo: <code>TABULAR</code>
	 */
	@Campo(indice = 6, nombre = "account", tipo = TipoCampo.TABULAR)
	private List<Account> account;
	
}
