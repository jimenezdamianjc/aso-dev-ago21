package com.bbva.mzic.accounts.dao.model.yha4;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>YHA4</code>
 * 
 * @see PeticionTransaccionYha4
 * @see RespuestaTransaccionYha4
 */
@Component
public class TransaccionYha4 implements InvocadorTransaccion<PeticionTransaccionYha4, RespuestaTransaccionYha4> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionYha4 invocar(PeticionTransaccionYha4 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionYha4.class, RespuestaTransaccionYha4.class, transaccion);
	}

	@Override
	public RespuestaTransaccionYha4 invocarCache(PeticionTransaccionYha4 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionYha4.class, RespuestaTransaccionYha4.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionYha4.class, "vaciearCache");
	}
}
