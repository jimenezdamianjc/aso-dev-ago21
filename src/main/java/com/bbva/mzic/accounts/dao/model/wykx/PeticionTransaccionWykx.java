package com.bbva.mzic.accounts.dao.model.wykx;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * <p>Transacci&oacute;n <code>WYKX</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionWykx</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionWykx</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: MBVD.QG.FIX.QGDTCCT.WYKX.txt
 * WYKXCONSULTA ESTRUCTURA DE PARAMETROS  KN        KN1CWYKX        KNECGKXE            WYKX  SS0069CNNNNN    SSTS    C   NNNSNNNN  NN                2014-03-13CICSDM112019-12-1917.22.32XMY3698 2014-03-13-13.54.38.697702CICSDM110001-01-010001-01-01
 * FICHERO: MBVD.QG.FIX.QGDTFDF.WYKX1.txt
 * KNECGKXE�                              �F�06�00043�01�00001�ENTIDAD�                    �A�004�0�R�        �
 * KNECGKXE�                              �F�06�00043�02�00005�CENTRO �                    �A�004�0�O�        �
 * KNECGKXE�                              �F�06�00043�03�00009�CUENTA �                    �A�010�0�R�        �
 * KNECGKXE�                              �F�06�00043�04�00019�BUSCTA �                    �A�001�0�R�        �
 * KNECGKXE�                              �F�06�00043�05�00020�PAGKEY �                    �A�021�0�O�        �
 * KNECGKXE�                              �F�06�00043�06�00041�PAGSIZE�                    �A�003�0�O�        �
 * FICHERO: MBVD.QG.FIX.QGDTFDF.WYKX2.txt
 * KNECGKXS�                              �X�23�00287�01�00001�CTACEN �                    �A�010�0�S�        �
 * KNECGKXS�                              �X�23�00287�02�00011�NIVEL  �                    �A�001�0�S�        �
 * KNECGKXS�                              �X�23�00287�03�00012�CTAPER �                    �A�010�0�S�        �
 * KNECGKXS�                              �X�23�00287�04�00022�IMPTECA�                    �A�020�0�S�        �
 * KNECGKXS�                              �X�23�00287�05�00042�IMPTECD�                    �A�020�0�S�        �
 * KNECGKXS�                              �X�23�00287�06�00062�IMPTECH�                    �A�020�0�S�        �
 * KNECGKXS�                              �X�23�00287�07�00082�IMPTDI �                    �A�020�0�S�        �
 * KNECGKXS�                              �X�23�00287�08�00102�CTAPERI�                    �A�010�0�S�        �
 * KNECGKXS�                              �X�23�00287�09�00112�TECHOCO�                    �A�021�0�S�        �
 * KNECGKXS�                              �X�23�00287�10�00133�IMPSADI�                    �A�021�0�S�        �
 * KNECGKXS�                              �X�23�00287�11�00154�FECHATA�                    �A�010�0�S�        �
 * KNECGKXS�                              �X�23�00287�12�00164�FECHA  �                    �A�010�0�S�        �
 * KNECGKXS�                              �X�23�00287�13�00174�HORA   �                    �A�005�0�S�        �
 * KNECGKXS�                              �X�23�00287�14�00179�CVEREV �                    �A�001�0�S�        �
 * KNECGKXS�                              �X�23�00287�15�00180�NUMOPPC�                    �A�006�0�S�        �
 * KNECGKXS�                              �X�23�00287�16�00186�NUMOSIT�                    �A�006�0�S�        �
 * KNECGKXS�                              �X�23�00287�17�00192�ICMPSIT�                    �A�021�0�S�        �
 * KNECGKXS�                              �X�23�00287�18�00213�ITTCOMP�                    �A�021�0�S�        �
 * KNECGKXS�                              �X�23�00287�19�00234�ITTDIS �                    �A�021�0�S�        �
 * KNECGKXS�                              �X�23�00287�20�00255�ITTDSP �                    �A�021�0�S�        �
 * KNECGKXS�                              �X�23�00287�21�00276�TIPONAT�                    �A�001�0�S�        �
 * KNECGKXS�                              �X�23�00287�22�00277�CTACNTR�                    �A�010�0�S�        �
 * KNECGKXS�                              �X�23�00287�23�00287�TIPOREL�                    �A�001�0�S�        �
 * FICHERO: MBVD.QG.FIX.QGDTFDF.WYKX3.txt
 * KNECXAAA�                              �X�02�00022�01�00001�INDDATO�                    �A�001�0�S�        �
 * KNECXAAA�                              �X�02�00022�02�00002�PAGKEY �                    �A�021�0�S�        �
 * FICHERO: MBVD.QG.FIX.QGDTFDX.WYKX.txt
 * WYKXKNECXAAACOPY    KN1CWYKX1S0022S000                     XM05251 2020-01-30-16.50.16.273563XM05251 2020-01-30-16.50.16.273577
 * WYKXKNECGKXSCOPY    KN1CWYKX1S0287S100                     XMY3698 2019-10-24-12.50.44.909987XMY3698 2019-10-24-12.50.44.910055
</pre></code>
 * 
 * @see RespuestaTransaccionWykx
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "WYKX",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionWykx.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoKNECGKXE.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionWykx implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}
