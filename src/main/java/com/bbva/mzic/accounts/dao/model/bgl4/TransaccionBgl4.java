package com.bbva.mzic.accounts.dao.model.bgl4;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>BGL4</code>
 * 
 * @see PeticionTransaccionBgl4
 * @see RespuestaTransaccionBgl4
 */
@Component
public class TransaccionBgl4 implements InvocadorTransaccion<PeticionTransaccionBgl4, RespuestaTransaccionBgl4> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionBgl4 invocar(PeticionTransaccionBgl4 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionBgl4.class, RespuestaTransaccionBgl4.class, transaccion);
	}

	@Override
	public RespuestaTransaccionBgl4 invocarCache(PeticionTransaccionBgl4 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionBgl4.class, RespuestaTransaccionBgl4.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionBgl4.class, "vaciearCache");
	}
}
