package com.bbva.mzic.accounts.dao.model.pejw;

import java.io.Serializable;

privileged aspect PeticionTransaccionPejw_Roo_Serializable {
    
    declare parents: PeticionTransaccionPejw implements Serializable;
    
    private static final long PeticionTransaccionPejw.serialVersionUID = 1L;
    
}
