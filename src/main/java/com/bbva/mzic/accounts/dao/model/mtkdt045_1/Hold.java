package com.bbva.mzic.accounts.dao.model.mtkdt045_1;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * <p>Bean fila para el campo tabular <code>hold</code>, utilizado por la clase <code>RespuestaTransaccionMtkdt045_1</code></p>
 *
 * @see RespuestaTransaccionMtkdt045_1
 *
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Hold {

	/**
	 * <p>Campo <code>accountId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "accountId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 6, signo = true, obligatorio = true)
	private String accountid;

	/**
	 * <p>Campo <code>numberTypeId</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "numberTypeId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 3, signo = true, obligatorio = true)
	private String numbertypeid;

	/**
	 * <p>Campo <code>numberTypeName</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "numberTypeName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 25, signo = true)
	private String numbertypename;

	/**
	 * <p>Campo <code>amount</code>, &iacute;ndice: <code>4</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 4, nombre = "amount", tipo = TipoCampo.DECIMAL, longitudMaxima = 14, signo = true, obligatorio = true)
	private BigDecimal amount;

	/**
	 * <p>Campo <code>currency</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "currency", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 3, signo = true, obligatorio = true)
	private String currency;

	/**
	 * <p>Campo <code>creationDate</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "creationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
	private String creationdate;

	/**
	 * <p>Campo <code>expirationDate</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "expirationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true)
	private String expirationdate;

	/**
	 * <p>Campo <code>operationDate</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "operationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true)
	private String operationdate;

	/**
	 * <p>Campo <code>typeHold</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "typeHold", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 25, signo = true, obligatorio = true)
	private String typehold;

	/**
	 * <p>Campo <code>typeDesc</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 10, nombre = "typeDesc", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 25, signo = true)
	private String typedesc;

	/**
	 * <p>Campo <code>productId</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 11, nombre = "productId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true, obligatorio = true)
	private String productid;

	/**
	 * <p>Campo <code>productName</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 12, nombre = "productName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true)
	private String productname;

	/**
	 * <p>Campo <code>ticketHold</code>, &iacute;ndice: <code>13</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 13, nombre = "ticketHold", tipo = TipoCampo.ENTERO, longitudMaxima = 13, signo = true, obligatorio = true)
	private long tickethold;

	/**
	 * <p>Campo <code>ticketMoneyMovement</code>, &iacute;ndice: <code>14</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 14, nombre = "ticketMoneyMovement", tipo = TipoCampo.ENTERO, longitudMaxima = 13, signo = true, obligatorio = true)
	private long ticketmoneymovement;

	/**
	 * <p>Campo <code>statusProductId</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 15, nombre = "statusProductId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 15, signo = true, obligatorio = true)
	private String statusproductid;

	/**
	 * <p>Campo <code>statusProductDescription</code>, &iacute;ndice: <code>16</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 16, nombre = "statusProductDescription", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 15, signo = true)
	private String statusproductdescription;

	/**
	 * <p>Campo <code>statusMovementId</code>, &iacute;ndice: <code>17</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 17, nombre = "statusMovementId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 25, signo = true, obligatorio = true)
	private String statusmovementid;

	/**
	 * <p>Campo <code>statusMovementDescription</code>, &iacute;ndice: <code>18</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 18, nombre = "statusMovementDescription", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 25, signo = true, obligatorio = true)
	private String statusmovementdescription;

	/**
	 * <p>Campo <code>moneyMovementType</code>, &iacute;ndice: <code>19</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 19, nombre = "moneyMovementType", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 15, signo = true, obligatorio = true)
	private String moneymovementtype;

	/**
	 * <p>Campo <code>moneyMovementTypeDescription</code>, &iacute;ndice: <code>20</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 20, nombre = "moneyMovementTypeDescription", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true)
	private String moneymovementtypedescription;

	/**
	 * <p>Campo <code>reason</code>, &iacute;ndice: <code>21</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 21, nombre = "reason", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 100, signo = true, obligatorio = true)
	private String reason;

	/**
	 * <p>Campo <code>authorization</code>, &iacute;ndice: <code>22</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 22, nombre = "authorization", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 1, signo = true)
	private String authorization;

	/**
	 * <p>Campo <code>movementReferences</code>, &iacute;ndice: <code>23</code>, tipo: <code>TABULAR</code>
	 */
	@Campo(indice = 23, nombre = "movementReferences", tipo = TipoCampo.TABULAR)
	private List<Movementreferences> movementreferences;

}
