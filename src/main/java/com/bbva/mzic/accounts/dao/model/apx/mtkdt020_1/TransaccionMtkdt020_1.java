package com.bbva.mzic.accounts.dao.model.apx.mtkdt020_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MTKDT020</code>
 *
 * @see PeticionTransaccionMtkdt020_1
 * @see RespuestaTransaccionMtkdt020_1
 */
@Component
public class TransaccionMtkdt020_1 implements InvocadorTransaccion<PeticionTransaccionMtkdt020_1, RespuestaTransaccionMtkdt020_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMtkdt020_1 invocar(final PeticionTransaccionMtkdt020_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt020_1.class, RespuestaTransaccionMtkdt020_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMtkdt020_1 invocarCache(final PeticionTransaccionMtkdt020_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt020_1.class, RespuestaTransaccionMtkdt020_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMtkdt020_1.class, "vaciearCache");
	}
}
