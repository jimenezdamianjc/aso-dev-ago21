package com.bbva.mzic.accounts.dao.mapper;

import com.bbva.jee.arq.spring.core.host.IMapper;
import com.bbva.mzic.accounts.business.dto.DtoIntFilterAccount;
import com.bbva.mzic.accounts.dao.model.bgl4.FormatoBGML4E;
import com.bbva.mzic.accounts.dao.model.bgl4.PeticionTransaccionBgl4;

public class InputBGL4Mapper implements IMapper<DtoIntFilterAccount, PeticionTransaccionBgl4> {

	@Override
	public PeticionTransaccionBgl4 map(DtoIntFilterAccount filter) {

		final PeticionTransaccionBgl4 peticion = new PeticionTransaccionBgl4();
		final FormatoBGML4E formatoBGML4E = new FormatoBGML4E();
//		peticion.setFamprod(filter.getAccountFamilyId());
//		peticion.setClienpu(filter.getClientId());
		// Pasar Datos
		peticion.getCuerpo().getPartes().add(formatoBGML4E);

		return peticion;
	}
}
