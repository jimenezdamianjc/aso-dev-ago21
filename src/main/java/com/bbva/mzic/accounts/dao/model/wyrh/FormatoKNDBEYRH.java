package com.bbva.mzic.accounts.dao.model.wyrh;


import java.util.Date;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.mzic.serviceutils.rm.utils.tx.IFormat;

/**
 * Formato de datos <code>KNDBEYRH</code> de la transacci&oacute;n <code>WYRH</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KNDBEYRH")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoKNDBEYRH implements IFormat {

    /**
     * <p>
     * Campo <code>CLIEPU</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "CLIEPU", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
    private String cliepu;

    /**
     * <p>
     * Campo <code>NUMCTA</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "NUMCTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String numcta;

    /**
     * <p>
     * Campo <code>FOLIOCA</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "FOLIOCA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String folioca;

    /**
     * <p>
     * Campo <code>FOLIOCH</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "FOLIOCH", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 12, longitudMaxima = 12)
    private String folioch;

    /**
     * <p>
     * Campo <code>TIPCHEQ</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "TIPCHEQ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String tipcheq;

    /**
     * <p>
     * Campo <code>FECHALT</code>, &iacute;ndice: <code>6</code>, tipo: <code>FECHA</code>
     */
    @Campo(indice = 6, nombre = "FECHALT", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
    private Date fechalt;

}
