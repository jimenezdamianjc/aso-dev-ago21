package com.bbva.mzic.accounts.dao.model.mtkdt021_1;

import java.math.BigDecimal;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Bean fila para el campo tabular <code>movement</code>, utilizado por la clase
 * <code>RespuestaTransaccionMtkdt021_1</code>
 * </p>
 * 
 * @see RespuestaTransaccionMtkdt021_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Movement {

    /**
     * <p>
     * Campo <code>ticketId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 1, nombre = "ticketId", tipo = TipoCampo.ENTERO, longitudMaxima = 13, signo = true, obligatorio = true)
    private long ticketid;

    /**
     * <p>
     * Campo <code>amount</code>, &iacute;ndice: <code>2</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 2, nombre = "amount", tipo = TipoCampo.DECIMAL, longitudMaxima = 14, signo = true, obligatorio = true)
    private BigDecimal amount;

    /**
     * <p>
     * Campo <code>amountCurrency</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "amountCurrency", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 3, signo = true, obligatorio = true)
    private String amountcurrency;

    /**
     * <p>
     * Campo <code>description</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "description", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 100, signo = true, obligatorio = true)
    private String description;

    /**
     * <p>
     * Campo <code>transactionTypeId</code>, &iacute;ndice: <code>5</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "transactionTypeId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 24, signo = true, obligatorio = true)
    private String transactiontypeid;

    /**
     * <p>
     * Campo <code>transactionTypeName</code>, &iacute;ndice: <code>6</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "transactionTypeName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 100, signo = true,
            obligatorio = true)
    private String transactiontypename;

    /**
     * <p>
     * Campo <code>creationDate</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 7, nombre = "creationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 19, signo = true, obligatorio = true)
    private String creationdate;

    /**
     * <p>
     * Campo <code>applicationDate</code>, &iacute;ndice: <code>8</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 8, nombre = "applicationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 19, signo = true, obligatorio = true)
    private String applicationdate;

    /**
     * <p>
     * Campo <code>valuationDate</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 9, nombre = "valuationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 19, signo = true, obligatorio = true)
    private String valuationdate;

    /**
     * <p>
     * Campo <code>financingTypeId</code>, &iacute;ndice: <code>10</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 10, nombre = "financingTypeId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true, obligatorio = true)
    private String financingtypeid;

    /**
     * <p>
     * Campo <code>financingTypeName</code>, &iacute;ndice: <code>11</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 11, nombre = "financingTypeName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 50, signo = true, obligatorio = true)
    private String financingtypename;

    /**
     * <p>
     * Campo <code>status</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 12, nombre = "status", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 21, signo = true, obligatorio = true)
    private String status;

    /**
     * <p>
     * Campo <code>statusName</code>, &iacute;ndice: <code>13</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 13, nombre = "statusName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 50, signo = true, obligatorio = true)
    private String statusname;

    /**
     * <p>
     * Campo <code>statusUpdateDate</code>, &iacute;ndice: <code>14</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 14, nombre = "statusUpdateDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true)
    private String statusupdatedate;

    /**
     * <p>
     * Campo <code>contractId</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 15, nombre = "contractId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 6, signo = true, obligatorio = true)
    private String contractid;

    /**
     * <p>
     * Campo <code>contractNumber</code>, &iacute;ndice: <code>16</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 16, nombre = "contractNumber", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
    private String contractnumber;

    /**
     * <p>
     * Campo <code>numberTypeId</code>, &iacute;ndice: <code>17</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 17, nombre = "numberTypeId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 3, signo = true, obligatorio = true)
    private String numbertypeid;

    /**
     * <p>
     * Campo <code>numberTypeName</code>, &iacute;ndice: <code>18</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 18, nombre = "numberTypeName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 21, signo = true, obligatorio = true)
    private String numbertypename;

    /**
     * <p>
     * Campo <code>contractProductId</code>, &iacute;ndice: <code>19</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 19, nombre = "contractProductId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 8, signo = true, obligatorio = true)
    private String contractproductid;

    /**
     * <p>
     * Campo <code>contractProductName</code>, &iacute;ndice: <code>20</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 20, nombre = "contractProductName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 8, signo = true, obligatorio = true)
    private String contractproductname;

    /**
     * <p>
     * Campo <code>productTypeId</code>, &iacute;ndice: <code>21</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 21, nombre = "productTypeId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true, obligatorio = true)
    private String producttypeid;

    /**
     * <p>
     * Campo <code>productTypeName</code>, &iacute;ndice: <code>22</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 22, nombre = "productTypeName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 35, signo = true, obligatorio = true)
    private String producttypename;

    /**
     * <p>
     * Campo <code>alias</code>, &iacute;ndice: <code>23</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 23, nombre = "alias", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true, obligatorio = true)
    private String alias;

    /**
     * <p>
     * Campo <code>bankId</code>, &iacute;ndice: <code>24</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 24, nombre = "bankId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true, obligatorio = true)
    private String bankid;

    /**
     * <p>
     * Campo <code>bankName</code>, &iacute;ndice: <code>25</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 25, nombre = "bankName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 15, signo = true, obligatorio = true)
    private String bankname;

    /**
     * <p>
     * Campo <code>tags</code>, &iacute;ndice: <code>26</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 26, nombre = "tags", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 300, signo = true)
    private String tags;

    /**
     * <p>
     * Campo <code>movementType</code>, &iacute;ndice: <code>27</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 27, nombre = "movementType", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 5, signo = true, obligatorio = true)
    private String movementtype;

    /**
     * <p>
     * Campo <code>movementName</code>, &iacute;ndice: <code>28</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 28, nombre = "movementName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 50, signo = true, obligatorio = true)
    private String movementname;

}
