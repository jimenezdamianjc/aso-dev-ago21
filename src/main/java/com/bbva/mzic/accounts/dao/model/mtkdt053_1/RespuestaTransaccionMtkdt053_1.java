package com.bbva.mzic.accounts.dao.model.mtkdt053_1;

import java.util.List;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Cabecera;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.NombreCabecera;
import com.bbva.jee.arq.spring.core.host.RespuestaTransaccion;
import com.bbva.jee.arq.spring.core.host.TipoCampo;


/**
 * Bean de respuesta para la transacci&oacute;n <code>MTKDT053</code>
 * 
 * @see PeticionTransaccionMtkdt053_1
 */
@RespuestaTransaccion
@Formato(nombre = "1")
@RooJavaBean
@RooToString
@RooSerializable
public class RespuestaTransaccionMtkdt053_1 {
	
	/**
	 * <p>Cabecera <code>COD-AVISO</code></p>
	 */
	@Cabecera(nombre=NombreCabecera.CODIGO_AVISO)
	private String codigoAviso;
	
	/**
	 * <p>Cabecera <code>DES-AVISO</code></p>
	 */
	@Cabecera(nombre=NombreCabecera.DESCRIPCION_AVISO)
	private String descripcionAviso;
	
	/**
	 * <p>Cabecera <code>COD-UUAA-AVISO</code></p>
	 */
	@Cabecera(nombre=NombreCabecera.APLICACION_AVISO)
	private String aplicacionAviso;
	
	/**
	 * <p>Cabecera <code>COD-RETORNO</code></p>
	 */
	@Cabecera(nombre=NombreCabecera.CODIGO_RETORNO)
	private String codigoRetorno;
	
	/**
	 * <p>Campo <code>ticketId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "ticketId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 13, signo = true, obligatorio = true)
	private String campo_1_ticketid;
	
	/**
	 * <p>Campo <code>amount</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "amount", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 14, signo = true, obligatorio = true)
	private String amount;
	
	/**
	 * <p>Campo <code>currency</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "currency", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 3, signo = true, obligatorio = true)
	private String currency;
	
	/**
	 * <p>Campo <code>reason</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "reason", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 100, signo = true, obligatorio = true)
	private String reason;
	
	/**
	 * <p>Campo <code>creationDate</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "creationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 19, signo = true, obligatorio = true)
	private String creationdate;
	
	/**
	 * <p>Campo <code>applicationDate</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "applicationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 19, signo = true)
	private String applicationdate;
	
	/**
	 * <p>Campo <code>updateDate</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "updateDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 19, signo = true)
	private String updatedate;
	
	/**
	 * <p>Campo <code>typeId</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "typeId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 5, signo = true)
	private String typeid;
	
	/**
	 * <p>Campo <code>typeDescription</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "typeDescription", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 50, signo = true)
	private String typedescription;
	
	/**
	 * <p>Campo <code>mainDescription</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 10, nombre = "mainDescription", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 50, signo = true, obligatorio = true)
	private String maindescription;
	
	/**
	 * <p>Campo <code>references</code>, &iacute;ndice: <code>11</code>, tipo: <code>TABULAR</code>
	 */
	@Campo(indice = 11, nombre = "references", tipo = TipoCampo.TABULAR)
	private List<References> references;
	
	/**
	 * <p>Campo <code>bankId</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 12, nombre = "bankId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true, obligatorio = true)
	private String bankid;
	
	/**
	 * <p>Campo <code>bankName</code>, &iacute;ndice: <code>13</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 13, nombre = "bankName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true)
	private String bankname;
	
	/**
	 * <p>Campo <code>branchId</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 14, nombre = "branchId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 5, signo = true, obligatorio = true)
	private String branchid;
	
	/**
	 * <p>Campo <code>branchName</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 15, nombre = "branchName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 35, signo = true)
	private String branchname;
	
	/**
	 * <p>Campo <code>userId</code>, &iacute;ndice: <code>16</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 16, nombre = "userId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
	private String userid;
	
	/**
	 * <p>Campo <code>internalCodeId</code>, &iacute;ndice: <code>17</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 17, nombre = "internalCodeId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true, obligatorio = true)
	private String internalcodeid;
	
	/**
	 * <p>Campo <code>lastUpdate</code>, &iacute;ndice: <code>18</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 18, nombre = "lastUpdate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 19, signo = true, obligatorio = true)
	private String lastupdate;
	
	/**
	 * <p>Campo <code>origin</code>, &iacute;ndice: <code>19</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 19, nombre = "origin", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 7, signo = true, obligatorio = true)
	private String origin;
	
	/**
	 * <p>Campo <code>businessAgent</code>, &iacute;ndice: <code>20</code>, tipo: <code>TABULAR</code>
	 */
	@Campo(indice = 20, nombre = "businessAgent", tipo = TipoCampo.TABULAR)
	private List<Businessagent> businessagent;
	
}
