package com.bbva.mzic.accounts.dao.model.pejw;

import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import java.lang.String;

privileged aspect RespuestaTransaccionPejw_Roo_JavaBean {
    
    public String RespuestaTransaccionPejw.getCodigoRetorno() {
        return this.codigoRetorno;
    }
    
    public void RespuestaTransaccionPejw.setCodigoRetorno(String codigoRetorno) {
        this.codigoRetorno = codigoRetorno;
    }
    
    public String RespuestaTransaccionPejw.getCodigoControl() {
        return this.codigoControl;
    }
    
    public void RespuestaTransaccionPejw.setCodigoControl(String codigoControl) {
        this.codigoControl = codigoControl;
    }
    
    public void RespuestaTransaccionPejw.setCuerpo(CuerpoMultiparte cuerpo) {
        this.cuerpo = cuerpo;
    }
    
}
