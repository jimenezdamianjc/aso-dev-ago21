package com.bbva.mzic.accounts.dao.model.peh8;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>PEH8</code>
 * 
 * @see PeticionTransaccionPeh8
 * @see RespuestaTransaccionPeh8
 * 
 * @author Arquitectura Spring BBVA
 */
@Component("transaccionPeh8-v0")
public class TransaccionPeh8 implements InvocadorTransaccion<PeticionTransaccionPeh8, RespuestaTransaccionPeh8> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionPeh8 invocar(PeticionTransaccionPeh8 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionPeh8.class, RespuestaTransaccionPeh8.class, transaccion);
	}

	@Override
	public RespuestaTransaccionPeh8 invocarCache(PeticionTransaccionPeh8 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionPeh8.class, RespuestaTransaccionPeh8.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionPeh8.class, "vaciearCache");
	}
}
