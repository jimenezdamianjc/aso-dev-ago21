package com.bbva.mzic.accounts.dao.model.mtkdt045_1;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

/**
 * <p>Transacci&oacute;n <code>MTKDT045</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMtkdt045_1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMtkdt045_1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: MTKDT045.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot;
 * transactionName=&quot;MTKDT045&quot; application=&quot;MTKD&quot; version=&quot;01&quot; country=&quot;MX&quot;
 * language=&quot;ES&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;accountId&quot; type=&quot;String&quot; size=&quot;6&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;ticketId&quot; type=&quot;Long&quot; size=&quot;13&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;group name=&quot;hold&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;accountId&quot; type=&quot;String&quot; size=&quot;6&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;numberTypeId&quot; type=&quot;String&quot;
 * size=&quot;3&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;numberTypeName&quot; type=&quot;String&quot;
 * size=&quot;25&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;amount&quot; type=&quot;Double&quot; size=&quot;14&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;creationDate&quot; type=&quot;String&quot;
 * size=&quot;10&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;7&quot; name=&quot;expirationDate&quot; type=&quot;String&quot;
 * size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;8&quot; name=&quot;operationDate&quot; type=&quot;String&quot;
 * size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;9&quot; name=&quot;typeHold&quot; type=&quot;String&quot; size=&quot;25&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;10&quot; name=&quot;typeDesc&quot; type=&quot;String&quot; size=&quot;25&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;11&quot; name=&quot;productId&quot; type=&quot;String&quot; size=&quot;20&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;12&quot; name=&quot;productName&quot; type=&quot;String&quot;
 * size=&quot;20&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;13&quot; name=&quot;ticketHold&quot; type=&quot;Long&quot; size=&quot;13&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;14&quot; name=&quot;ticketMoneyMovement&quot; type=&quot;Long&quot;
 * size=&quot;13&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;15&quot; name=&quot;statusProductId&quot; type=&quot;String&quot;
 * size=&quot;15&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;16&quot; name=&quot;statusProductDescription&quot; type=&quot;String&quot;
 * size=&quot;15&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;17&quot; name=&quot;statusMovementId&quot; type=&quot;String&quot;
 * size=&quot;25&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;18&quot; name=&quot;statusMovementDescription&quot;
 * type=&quot;String&quot; size=&quot;25&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;19&quot; name=&quot;moneyMovementType&quot; type=&quot;String&quot;
 * size=&quot;15&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;20&quot; name=&quot;moneyMovementTypeDescription&quot;
 * type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;21&quot; name=&quot;reason&quot; type=&quot;String&quot; size=&quot;100&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;22&quot; name=&quot;authorization&quot; type=&quot;String&quot;
 * size=&quot;1&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;group name=&quot;movementReferences&quot; order=&quot;23&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;firstDescription&quot; type=&quot;String&quot;
 * size=&quot;300&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;secondDescription&quot; type=&quot;String&quot;
 * size=&quot;300&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;thirdDescription&quot; type=&quot;String&quot;
 * size=&quot;300&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;fourthDescription&quot; type=&quot;String&quot;
 * size=&quot;300&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/group&gt;
 * &lt;/group&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Transaccion utilizada para obtener la consulta del detalle
 * de una retencion asociada a una cuenta en dolares.&lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 *
 * @see RespuestaTransaccionMtkdt045_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
		nombre = "MTKDT045",
		tipo = 1,
		subtipo = 1,
		version = 1,
		configuracion = "default_apx",
		respuesta = RespuestaTransaccionMtkdt045_1.class,
		atributos = {@Atributo(nombre = "country", valor = "MX")}
		)
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMtkdt045_1 {

	/**
	 * <p>Campo <code>accountId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "accountId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 6, signo = true, obligatorio = true)
	private String accountid;

	/**
	 * <p>Campo <code>ticketId</code>, &iacute;ndice: <code>2</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 2, nombre = "ticketId", tipo = TipoCampo.ENTERO, longitudMaxima = 13, signo = true, obligatorio = true)
	private long ticketid;

}
