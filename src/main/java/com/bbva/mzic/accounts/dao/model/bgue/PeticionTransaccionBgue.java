package com.bbva.mzic.accounts.dao.model.bgue;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

/**
 * <p>
 * Transacci&oacute;n <code>BGUE</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionBgue</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionBgue</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: QGDTCCT.BGUE.txt
 * BGUEENTREGA AGIL DE CHEQUERAS          BG        BG2CGDUE     01 BGNCGUE             BGUE  NS0000CNNNNN    SSTN    C   NNNSNNNN  NN                2019-04-26XMZ1948 2019-05-1411.08.46XMZ1948 2019-04-26-10.11.41.061390XMZ1948 0001-01-010001-01-01
 * FICHERO: QGDTFDF.BGUE.txt
 * BGNCGUE �ENTREGA AGIL DE CHEQUERAS     �F�10�00183�01�00001�CUENTA �NUMERO DE CUENTA    �A�010�0�R�        �
 * BGNCGUE �ENTREGA AGIL DE CHEQUERAS     �F�10�00183�02�00011�PRICHEQ�PRIMER CHEQUE       �N�009�0�R�        �
 * BGNCGUE �ENTREGA AGIL DE CHEQUERAS     �F�10�00183�03�00020�ULTCHEQ�ULTIMO CHEQUE       �N�009�0�R�        �
 * BGNCGUE �ENTREGA AGIL DE CHEQUERAS     �F�10�00183�04�00029�NOMBRS �NOMBRE(S) RECIBE    �A�040�0�R�        �
 * BGNCGUE �ENTREGA AGIL DE CHEQUERAS     �F�10�00183�05�00069�APEPAT �APELLIDO PATERNO    �A�040�0�R�        �
 * BGNCGUE �ENTREGA AGIL DE CHEQUERAS     �F�10�00183�06�00109�APEMAT �APELLIDO MATERNO    �A�040�0�R�        �
 * BGNCGUE �ENTREGA AGIL DE CHEQUERAS     �F�10�00183�07�00149�CODIDE �CODIGO IDENTIF.     �N�002�0�R�        �
 * BGNCGUE �ENTREGA AGIL DE CHEQUERAS     �F�10�00183�08�00151�NUMIDE �NUMERO DE INDENTIF. �N�013�0�R�        �
 * BGNCGUE �ENTREGA AGIL DE CHEQUERAS     �F�10�00183�09�00164�FECVEN �FECHA VENC. IDENTIF.�A�010�0�O�        �
 * BGNCGUE �ENTREGA AGIL DE CHEQUERAS     �F�10�00183�10�00174�FECEXP �FECHA EXP. IDENTIF. �A�010�0�O�        �
</pre></code>
 * 
 * @see RespuestaTransaccionBgue
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "BGUE", tipo = 1, subtipo = 1, version = 1, configuracion = "default_ps9_mx",
        respuesta = RespuestaTransaccionBgue.class,
        atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")})
@Multiformato(formatos = {FormatoBGNCGUE.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionBgue implements MensajeMultiparte {

    /**
     * <p>
     * Cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Cuerpo
    private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

    /**
     * <p>
     * Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Override
    public CuerpoMultiparte getCuerpo() {
        return cuerpo;
    }

}
