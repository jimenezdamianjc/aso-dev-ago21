package com.bbva.mzic.accounts.dao.model.wydp;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>KNDBSYDP</code> de la transacci&oacute;n <code>WYDP</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KNDBSYDP")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoKNDBSYDP {

    /**
     * <p>
     * Campo <code>FOLIOCN</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "FOLIOCN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String foliocn;

}
