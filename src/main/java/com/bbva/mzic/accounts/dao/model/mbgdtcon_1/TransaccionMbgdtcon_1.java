package com.bbva.mzic.accounts.dao.model.mbgdtcon_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MBGDTCON</code>
 *
 * @see PeticionTransaccionMbgdtcon_1
 * @see RespuestaTransaccionMbgdtcon_1
 */
@Component
public class TransaccionMbgdtcon_1 implements InvocadorTransaccion<PeticionTransaccionMbgdtcon_1, RespuestaTransaccionMbgdtcon_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMbgdtcon_1 invocar(final PeticionTransaccionMbgdtcon_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMbgdtcon_1.class, RespuestaTransaccionMbgdtcon_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMbgdtcon_1 invocarCache(final PeticionTransaccionMbgdtcon_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMbgdtcon_1.class, RespuestaTransaccionMbgdtcon_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMbgdtcon_1.class, "vaciearCache");
	}
}
