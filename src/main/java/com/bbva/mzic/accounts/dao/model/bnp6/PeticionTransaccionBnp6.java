package com.bbva.mzic.accounts.dao.model.bnp6;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

/**
 * <p>
 * Transacci&oacute;n <code>BNP6</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionBnp6</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionBnp6</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: BNP6-CCT.txt
 * BNP6CONSULTA CTAS DEL CLIENTE X PRELAR BN        BN2CCAN1     01 BNRBNP6             BNP6  NN3000CNNNNN    SSTN    C   NNNNNNNN  NN                2019-02-06CICSDM112019-03-1222.32.19CICSDM112019-02-06-09.32.00.746060CICSDM110001-01-010001-01-01
 * FICHERO: BNRBNP6-FDF.txt
 * BNRBNP6 �CONSULTA DE PRELACION         �F�03�00029�01�00001�CLIENTE�CLIENTE             �A�008�0�R�        �
 * BNRBNP6 �CONSULTA DE PRELACION         �F�03�00029�02�00009�PRODCTA�CUENTA PRODUCTO     �A�001�0�O�        �
 * BNRBNP6 �CONSULTA DE PRELACION         �F�03�00029�03�00010�TIPPROD�TIPO DE PRODUCTO    �A�020�0�O�        �
 * FICHERO: BNRBNPS-FDF.txt
 * BNRBNPS �CONSULTA DE PRELACION         �X�15�00215�01�00001�FOLIO  �FOLIO SOLICITUD CRED�A�020�0�S�        �
 * BNRBNPS �CONSULTA DE PRELACION         �X�15�00215�02�00021�CONTRPU�CONTRATO PU         �A�020�0�S�        �
 * BNRBNPS �CONSULTA DE PRELACION         �X�15�00215�03�00041�DESCCTA�DESCRIPCION DE CTA  �A�030�0�S�        �
 * BNRBNPS �CONSULTA DE PRELACION         �X�15�00215�04�00071�CREDITO�NUMERO DE CREDITO   �A�040�0�S�        �
 * BNRBNPS �CONSULTA DE PRELACION         �X�15�00215�05�00111�TIPCRED�TIPO DE CREDITO     �A�002�0�S�        �
 * BNRBNPS �CONSULTA DE PRELACION         �X�15�00215�06�00113�DESCCRE�DESC DEL CREDITO    �A�030�0�S�        �
 * BNRBNPS �CONSULTA DE PRELACION         �X�15�00215�07�00143�MONTO  �MONTO MENSUAL CREDIT�A�011�0�S�        �
 * BNRBNPS �CONSULTA DE PRELACION         �X�15�00215�08�00154�PRELACI�PRIORIDAD DE COBRO  �N�002�0�S�        �
 * BNRBNPS �CONSULTA DE PRELACION         �X�15�00215�09�00156�ESTATUS�ESTATUS DEL CREDITO �A�001�0�S�        �
 * BNRBNPS �CONSULTA DE PRELACION         �X�15�00215�10�00157�TIPCTA �TIPO DE CUENTA      �A�003�0�S�        �
 * BNRBNPS �CONSULTA DE PRELACION         �X�15�00215�11�00160�IDFREC �FRECUENCIA DE PAGO  �A�003�0�S�        �
 * BNRBNPS �CONSULTA DE PRELACION         �X�15�00215�12�00163�IDCTAPG�IDENT DE CTA DE PGO �A�005�0�S�        �
 * BNRBNPS �CONSULTA DE PRELACION         �X�15�00215�13�00168�TPRODOM�TIPO DE PROD CON DOM�A�015�0�S�        �
 * BNRBNPS �CONSULTA DE PRELACION         �X�15�00215�14�00183�DESCEST�DESC DE ESTATUS PREL�A�030�0�S�        �
 * BNRBNPS �CONSULTA DE PRELACION         �X�15�00215�15�00213�TCTACRE�TIPO CUENTA CREDITO �A�003�0�S�        �
 * FICHERO: BNP6-FDX.txt
 * BNP6BNRBNPS BNP600S BN2CCAN11S                             CICSDM112019-02-12-15.05.37.582252CICSDM112019-02-12-15.05.37.582293
</pre></code>
 *
 * @see RespuestaTransaccionBnp6
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "BNP6", tipo = 1, subtipo = 1, version = 1, configuracion = "default_ps9_mx",
        respuesta = RespuestaTransaccionBnp6.class,
        atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")})
@Multiformato(formatos = {FormatoBNRBNP6.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionBnp6 implements MensajeMultiparte {

    /**
     * <p>
     * Cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Cuerpo
    private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

    /**
     * <p>
     * Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Override
    public CuerpoMultiparte getCuerpo() {
        return cuerpo;
    }

}
