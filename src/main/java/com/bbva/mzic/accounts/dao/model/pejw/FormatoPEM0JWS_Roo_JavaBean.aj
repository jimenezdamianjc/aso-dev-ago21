package com.bbva.mzic.accounts.dao.model.pejw;

import java.lang.String;

privileged aspect FormatoPEM0JWS_Roo_JavaBean {
    
    public String FormatoPEM0JWS.getContrac() {
        return this.contrac;
    }
    
    public void FormatoPEM0JWS.setContrac(String contrac) {
        this.contrac = contrac;
    }
    
    public String FormatoPEM0JWS.getProduid() {
        return this.produid;
    }
    
    public void FormatoPEM0JWS.setProduid(String produid) {
        this.produid = produid;
    }
    
    public String FormatoPEM0JWS.getProduna() {
        return this.produna;
    }
    
    public void FormatoPEM0JWS.setProduna(String produna) {
        this.produna = produna;
    }
    
    public String FormatoPEM0JWS.getStatusi() {
        return this.statusi;
    }
    
    public void FormatoPEM0JWS.setStatusi(String statusi) {
        this.statusi = statusi;
    }
    
    public String FormatoPEM0JWS.getStatusn() {
        return this.statusn;
    }
    
    public void FormatoPEM0JWS.setStatusn(String statusn) {
        this.statusn = statusn;
    }
    
}
