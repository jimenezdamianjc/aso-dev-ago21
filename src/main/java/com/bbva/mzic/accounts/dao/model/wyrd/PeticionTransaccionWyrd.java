package com.bbva.mzic.accounts.dao.model.wyrd;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

/**
 * <p>Transacci&oacute;n <code>WYRD</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionWyrd</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionWyrd</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: QGDTFDF.WYRD.TXT
 * KNDBWYRD�                              �F�09�00192�01�00001�CLIEPU �                    �A�008�0�R�        �
 * KNDBWYRD�                              �F�09�00192�02�00009�NUMCEL �                    �A�010�0�R�        �
 * KNDBWYRD�                              �F�09�00192�03�00019�NUMCTA �                    �A�020�0�R�        �
 * KNDBWYRD�                              �F�09�00192�04�00039�CIACEL �                    �A�010�0�R�        �
 * KNDBWYRD�                              �F�09�00192�05�00049�IMPOPER�                    �S�017�2�R�        �
 * KNDBWYRD�                              �F�09�00192�06�00066�ALIAS  �                    �A�060�0�R�        �
 * KNDBWYRD�                              �F�09�00192�07�00126�CONCEPT�                    �A�055�0�R�        �
 * KNDBWYRD�                              �F�09�00192�08�00181�FECPROG�                    �A�010�0�O�        �
 * KNDBWYRD�                              �F�09�00192�09�00191�TIPCHEQ�                    �A�002�0�O�        �
 * KNDBSYRD�                              �X�10�00111�01�00001�CVEOTP �                    �A�016�0�S�        �
 * KNDBSYRD�                              �X�10�00111�02�00017�IMPOPE �                    �S�017�2�S�        �
 * KNDBSYRD�                              �X�10�00111�03�00034�DIVISA �                    �A�003�0�S�        �
 * KNDBSYRD�                              �X�10�00111�04�00037�FOLIOCA�                    �A�010�0�S�        �
 * KNDBSYRD�                              �X�10�00111�05�00047�FOLIOCH�                    �A�012�0�S�        �
 * KNDBSYRD�                              �X�10�00111�06�00059�FECOPER�                    �A�010�0�S�        �
 * KNDBSYRD�                              �X�10�00111�07�00069�FECEXP �                    �A�010�0�S�        �
 * KNDBSYRD�                              �X�10�00111�08�00079�HROPERA�                    �A�008�0�S�        �
 * KNDBSYRD�                              �X�10�00111�09�00087�HREXPIR�                    �A�008�0�S�        �
 * KNDBSYRD�                              �X�10�00111�10�00095�SDODISP�                    �N�017�2�S�        �
 * FICHERO: QGDTFDX.WYRD.TXT
 * WYRDKNDBSYRDKNDBSYRDKN1CWYRD1S0111N000                     CICSDM112017-08-22-00.23.27.702877XM01735 2019-10-11-13.00.25.280984
 * FICHERO: QGDTCCT.WYRD.TXT
 * WYRDALTA CHEQUE DIGITAL                KN        KN1CWYRD     01 KNDBWYRD            WYRD  SS0192CNNNNN    SSTS A      NNNSNNNN  NN                2017-08-22CICSDM112018-07-1011.13.50XMZ1542 2017-08-22-00.21.38.249822CICSDM110001-01-010001-01-01
</pre></code>
 * 
 * @see RespuestaTransaccionWyrd
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "WYRD",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionWyrd.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoKNDBWYRD.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionWyrd implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}
