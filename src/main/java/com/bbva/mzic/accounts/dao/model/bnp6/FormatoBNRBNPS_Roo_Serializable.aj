package com.bbva.mzic.accounts.dao.model.bnp6;

import java.io.Serializable;

privileged aspect FormatoBNRBNPS_Roo_Serializable {
    
    declare parents: FormatoBNRBNPS implements Serializable;
    
    private static final long FormatoBNRBNPS.serialVersionUID = 1L;
    
}
