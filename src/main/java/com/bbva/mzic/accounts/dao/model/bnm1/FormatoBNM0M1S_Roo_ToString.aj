// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.bnm1;

import java.lang.String;

privileged aspect FormatoBNM0M1S_Roo_ToString {
    
    public String FormatoBNM0M1S.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Bndesed: ").append(getBndesed()).append(", ");
        sb.append("Bndivis: ").append(getBndivis()).append(", ");
        sb.append("Bnempco: ").append(getBnempco()).append(", ");
        sb.append("Bnfhafc: ").append(getBnfhafc()).append(", ");
        sb.append("Bnnucta: ").append(getBnnucta()).append(", ");
        sb.append("Bnregct: ").append(getBnregct()).append(", ");
        sb.append("Bnregfi: ").append(getBnregfi()).append(", ");
        sb.append("Bnstcam: ").append(getBnstcam()).append(", ");
        sb.append("Bnsubpr: ").append(getBnsubpr()).append(", ");
        sb.append("Bntipct: ").append(getBntipct()).append(", ");
        sb.append("Bntipse: ").append(getBntipse());
        return sb.toString();
    }
    
}
