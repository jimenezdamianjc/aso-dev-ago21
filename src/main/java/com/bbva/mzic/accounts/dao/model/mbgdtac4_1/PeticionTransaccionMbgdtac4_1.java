package com.bbva.mzic.accounts.dao.model.mbgdtac4_1;

import java.util.List;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MBGDTAC4</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMbgdtac4_1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMbgdtac4_1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: MBGDTAC4-01-MX.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot;
 * transactionName=&quot;MBGDTAC4&quot; application=&quot;MBGD&quot; version=&quot;01&quot; country=&quot;MX&quot;
 * language=&quot;EN&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;folioRequest&quot; type=&quot;Long&quot; size=&quot;8&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;group name=&quot;paginationIn&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;paginationKey&quot; type=&quot;String&quot;
 * size=&quot;9&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;paginationSize&quot; type=&quot;Long&quot;
 * size=&quot;9&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;group name=&quot;outputParticipates&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;clientNumber&quot; type=&quot;String&quot;
 * size=&quot;8&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;IdParticipation&quot; type=&quot;String&quot;
 * size=&quot;1&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;IdCategory&quot; type=&quot;String&quot; size=&quot;1&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;combinationId&quot; type=&quot;String&quot;
 * size=&quot;5&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;limitDesc&quot; type=&quot;Long&quot; size=&quot;20&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;/group&gt;
 * &lt;group name=&quot;paginationOut&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;paginationKey&quot; type=&quot;String&quot;
 * size=&quot;9&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;hasMoreData&quot; type=&quot;Long&quot; size=&quot;1&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;/group&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Consulta de participes asignados a un folio.&lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionMbgdtac4_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MBGDTAC4",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_apx",
	respuesta = RespuestaTransaccionMbgdtac4_1.class,
	atributos = {@Atributo(nombre = "country", valor = "MX")}
)
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMbgdtac4_1 {
		
		/**
	 * <p>Campo <code>folioRequest</code>, &iacute;ndice: <code>1</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 1, nombre = "folioRequest", tipo = TipoCampo.ENTERO, longitudMaxima = 8, signo = true, obligatorio = true)
	private int foliorequest;
	
	/**
	 * <p>Campo <code>paginationIn</code>, &iacute;ndice: <code>2</code>, tipo: <code>TABULAR</code>
	 */
	@Campo(indice = 2, nombre = "paginationIn", tipo = TipoCampo.TABULAR)
	private List<Paginationin> paginationin;
	
}
