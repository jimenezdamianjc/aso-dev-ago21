package com.bbva.mzic.accounts.dao.model.wyrh;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.mzic.serviceutils.rm.utils.tx.IFormat;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>KNDBS1RH</code> de la transacci&oacute;n <code>WYRH</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KNDBS1RH")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoKNDBS1RH implements IFormat {

    /**
     * <p>
     * Campo <code>FOLIOCH</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "FOLIOCH", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 12, longitudMaxima = 12)
    private String folioch;

    /**
     * <p>
     * Campo <code>TIPCHEQ</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "TIPCHEQ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String tipcheq;

}
