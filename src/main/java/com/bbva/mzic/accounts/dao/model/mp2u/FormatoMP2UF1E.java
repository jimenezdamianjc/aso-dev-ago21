package com.bbva.mzic.accounts.dao.model.mp2u;


import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>MP2UF1E</code> de la transacci&oacute;n <code>MP2U</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MP2UF1E")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMP2UF1E {

	/**
	 * <p>Campo <code>NUMCTA</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "NUMCTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String numcta;
	
	/**
	 * <p>Campo <code>NUMTAR</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "NUMTAR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 16, longitudMaxima = 16)
	private String numtar;
	
	/**
	 * <p>Campo <code>PRODID</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "PRODID", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
	private String prodid;
	
	/**
	 * <p>Campo <code>RELTYPE</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "RELTYPE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 50, longitudMaxima = 50)
	private String reltype;
	
	/**
	 * <p>Campo <code>APPID</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "APPID", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 13, longitudMaxima = 13)
	private String appid;
	
}
