// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.mbgdtscc_1;

import com.bbva.mzic.accounts.dao.model.mbgdtscc_1.Paginationin;
import java.lang.String;
import java.util.List;

privileged aspect PeticionTransaccionMbgdtscc_1_Roo_JavaBean {
    
    public String PeticionTransaccionMbgdtscc_1.getAccount() {
        return this.account;
    }
    
    public void PeticionTransaccionMbgdtscc_1.setAccount(String account) {
        this.account = account;
    }
    
    public List<Paginationin> PeticionTransaccionMbgdtscc_1.getPaginationin() {
        return this.paginationin;
    }
    
    public void PeticionTransaccionMbgdtscc_1.setPaginationin(List<Paginationin> paginationin) {
        this.paginationin = paginationin;
    }
    
}
