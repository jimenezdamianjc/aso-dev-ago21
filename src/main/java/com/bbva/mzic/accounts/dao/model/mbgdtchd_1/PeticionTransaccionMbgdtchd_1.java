package com.bbva.mzic.accounts.dao.model.mbgdtchd_1;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Transacci&oacute;n <code>MBGDTCHD</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMbgdtchd_1</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionMbgdtchd_1</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: MBGDTCHD-01-MX.xml
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot; transactionName=&quot;MBGDTCHD&quot; application=&quot;MBGD&quot; version=&quot;01&quot; country=&quot;MX&quot; language=&quot;ES&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;account&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;fromOperationDate&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;toOperationDate&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;paginationKey&quot; type=&quot;Long&quot; size=&quot;8&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;group name=&quot;digitalChecks&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;13&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;number&quot; type=&quot;Long&quot; size=&quot;13&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;concept&quot; type=&quot;String&quot; size=&quot;55&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;operationDate&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;dueDate&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;operationNumber&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;group name=&quot;receiver&quot; order=&quot;7&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;value&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;group name=&quot;holder&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;60&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/group&gt;
 * &lt;/group&gt;
 * &lt;group name=&quot;sentMoney&quot; order=&quot;8&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;amount&quot; type=&quot;Double&quot; size=&quot;15&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/group&gt;
 * &lt;group name=&quot;digitalCheckStatus&quot; order=&quot;9&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;2&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/group&gt;
 * &lt;group name=&quot;digitalCheckType&quot; order=&quot;10&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;1&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/group&gt;
 * &lt;group name=&quot;cashOut&quot; order=&quot;11&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;atmId&quot; type=&quot;String&quot; size=&quot;8&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;date&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/group&gt;
 * &lt;/group&gt;
 * &lt;group name=&quot;paginationOut&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;paginationKey&quot; type=&quot;Long&quot; size=&quot;8&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;hasMoreData&quot; type=&quot;String&quot; size=&quot;1&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/group&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Consulta Historico de cheques digitales en APX &lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionMbgdtchd_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "MBGDTCHD", tipo = 1, subtipo = 1, version = 1, configuracion = "default_apx",
        respuesta = RespuestaTransaccionMbgdtchd_1.class, atributos = {@Atributo(nombre = "country", valor = "MX")})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMbgdtchd_1 {

    /**
     * <p>
     * Campo <code>account</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "account", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true)
    private String account;

    /**
     * <p>
     * Campo <code>fromOperationDate</code>, &iacute;ndice: <code>2</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "fromOperationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true)
    private String fromoperationdate;

    /**
     * <p>
     * Campo <code>toOperationDate</code>, &iacute;ndice: <code>3</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "toOperationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true)
    private String tooperationdate;

    /**
     * <p>
     * Campo <code>paginationKey</code>, &iacute;ndice: <code>4</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 4, nombre = "paginationKey", tipo = TipoCampo.ENTERO, longitudMaxima = 8, signo = true)
    private Integer paginationkey;

}
