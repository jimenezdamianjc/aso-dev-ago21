package com.bbva.mzic.accounts.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.accounts.business.dto.DtoIntAggregatedAvailableBalance;
import com.bbva.mzic.accounts.business.dto.DtoIntConditions;
import com.bbva.mzic.accounts.business.dto.DtoIntFilterAccount;
import com.bbva.mzic.accounts.dao.IAccountsDaoV0;
import com.bbva.mzic.accounts.dao.model.bgl4.PeticionTransaccionBgl4;
import com.bbva.mzic.accounts.dao.model.bgl5.PeticionTransaccionBgl5;

@Repository
public class AccountsDaoV0 implements IAccountsDaoV0 {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public DtoIntAggregatedAvailableBalance getAccountBGL4(DtoIntFilterAccount dtoIntFilterAccount) {

		return servicioTransacciones.invoke(PeticionTransaccionBgl4.class, dtoIntFilterAccount);
	}

	@Override
	public List<DtoIntConditions> getListConditionsBGL5(DtoIntFilterAccount dtoIntFilterAccount) {

		return servicioTransacciones.invoke(PeticionTransaccionBgl5.class, dtoIntFilterAccount);
	}

}
