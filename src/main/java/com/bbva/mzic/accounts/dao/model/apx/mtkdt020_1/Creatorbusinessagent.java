package com.bbva.mzic.accounts.dao.model.apx.mtkdt020_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Bean fila para el campo tabular <code>creatorBusinessAgent</code>, utilizado por la clase
 * <code>Account</code>
 * </p>
 * 
 * @see Account
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Creatorbusinessagent {

    /**
     * <p>
     * Campo <code>fullName</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "fullName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 60, signo = true)
    private String fullname;

}
