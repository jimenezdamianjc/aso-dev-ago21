// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.wyis;

import java.io.Serializable;

privileged aspect FormatoKNDBSIS2_Roo_Serializable {
    
    declare parents: FormatoKNDBSIS2 implements Serializable;
    
    private static final long FormatoKNDBSIS2.serialVersionUID = 1L;
    
}
