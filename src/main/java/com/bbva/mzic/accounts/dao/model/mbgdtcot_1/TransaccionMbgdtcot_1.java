package com.bbva.mzic.accounts.dao.model.mbgdtcot_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MBGDTCOT</code>
 * 
 * @see PeticionTransaccionMbgdtcot_1
 * @see RespuestaTransaccionMbgdtcot_1
 */
@Component
public class TransaccionMbgdtcot_1 implements InvocadorTransaccion<PeticionTransaccionMbgdtcot_1, RespuestaTransaccionMbgdtcot_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMbgdtcot_1 invocar(PeticionTransaccionMbgdtcot_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMbgdtcot_1.class, RespuestaTransaccionMbgdtcot_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMbgdtcot_1 invocarCache(PeticionTransaccionMbgdtcot_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMbgdtcot_1.class, RespuestaTransaccionMbgdtcot_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMbgdtcot_1.class, "vaciearCache");
	}
}
