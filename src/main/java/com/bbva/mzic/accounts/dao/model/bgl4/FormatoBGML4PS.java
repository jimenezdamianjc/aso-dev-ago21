package com.bbva.mzic.accounts.dao.model.bgl4;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.mzic.serviceutils.rm.utils.tx.IFormat;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>BGML4PS</code> de la transacci&oacute;n <code>BGL4</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BGML4PS")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoBGML4PS implements IFormat {

    /**
     * <p>
     * Campo <code>AREAPAG</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "AREAPAG", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 48, longitudMaxima = 48)
    private String areapag;

}
