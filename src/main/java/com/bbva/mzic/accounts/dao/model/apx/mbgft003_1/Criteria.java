package com.bbva.mzic.accounts.dao.model.apx.mbgft003_1;

import java.math.BigDecimal;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Bean fila para el campo tabular <code>criteria</code>, utilizado por la clase
 * <code>PeticionTransaccionMbgdt003_1</code>
 * </p>
 * 
 * @see PeticionTransaccionMbgdt003_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Criteria {

    /**
     * <p>
     * Campo <code>order</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "order", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 1, signo = true, obligatorio = true)
    private String order;

    /**
     * <p>
     * Campo <code>contractNumber</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "contractNumber", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
    private String contractnumber;

    /**
     * <p>
     * Campo <code>operationDate</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "operationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true)
    private String operationdate;

    /**
     * <p>
     * Campo <code>fromOperationDate</code>, &iacute;ndice: <code>4</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "fromOperationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true)
    private String fromoperationdate;

    /**
     * <p>
     * Campo <code>toOperationDate</code>, &iacute;ndice: <code>5</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "toOperationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true)
    private String tooperationdate;

    /**
     * <p>
     * Campo <code>localAmount</code>, &iacute;ndice: <code>6</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 6, nombre = "localAmount", tipo = TipoCampo.DECIMAL, longitudMaxima = 16, signo = true)
    private BigDecimal localamount;

    /**
     * <p>
     * Campo <code>fromLocalAmount</code>, &iacute;ndice: <code>7</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 7, nombre = "fromLocalAmount", tipo = TipoCampo.DECIMAL, longitudMaxima = 16, signo = true)
    private BigDecimal fromlocalamount;

    /**
     * <p>
     * Campo <code>toLocalAmount</code>, &iacute;ndice: <code>8</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 8, nombre = "toLocalAmount", tipo = TipoCampo.DECIMAL, longitudMaxima = 16, signo = true)
    private BigDecimal tolocalamount;

    /**
     * <p>
     * Campo <code>fromMovementNumber</code>, &iacute;ndice: <code>9</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 9, nombre = "fromMovementNumber", tipo = TipoCampo.ENTERO, longitudMaxima = 13, signo = true)
    private Long frommovementnumber;

    /**
     * <p>
     * Campo <code>toMovementNumber</code>, &iacute;ndice: <code>10</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 10, nombre = "toMovementNumber", tipo = TipoCampo.ENTERO, longitudMaxima = 13, signo = true)
    private Long tomovementnumber;

    /**
     * <p>
     * Campo <code>moneyFlowId</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 11, nombre = "moneyFlowId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 1, signo = true)
    private String moneyflowid;

}
