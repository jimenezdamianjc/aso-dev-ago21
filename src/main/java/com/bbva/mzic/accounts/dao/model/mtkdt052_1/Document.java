package com.bbva.mzic.accounts.dao.model.mtkdt052_1;

import java.util.List;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * <p>Bean fila para el campo tabular <code>document</code>, utilizado por la clase <code>RespuestaTransaccionMtkdt052_1</code></p>
 *
 * @see RespuestaTransaccionMtkdt052_1
 *
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Document {

	/**
	 * <p>Campo <code>id</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "id", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true, obligatorio = true)
	private String id;

	/**
	 * <p>Campo <code>description</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "description", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 40, signo = true, obligatorio = true)
	private String description;

	/**
	 * <p>Campo <code>fields</code>, &iacute;ndice: <code>3</code>, tipo: <code>TABULAR</code>
	 */
	@Campo(indice = 3, nombre = "fields", tipo = TipoCampo.TABULAR)
	private List<Fields> fields;

}
