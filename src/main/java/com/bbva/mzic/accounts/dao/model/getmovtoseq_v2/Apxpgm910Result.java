
package com.bbva.mzic.accounts.dao.model.getmovtoseq_v2;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Clase Java para apxpgm910Result complex type.
 * 
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que
 * haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="apxpgm910Result">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WK_PAGINAMOS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WK_TOTALPAG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WK_MASDATOS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ARREGLO" type="{http://getmovtoseq_v2.wsbeans.iseries/}arreglo" maxOccurs="unbounded"/>
 *         &lt;element name="WK_DESCRERR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WK_CUENTAS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WK_TOTREG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(	name = "apxpgm910Result",
			propOrder = { "wkpaginamos", "wktotalpag", "wkmasdatos", "arreglo", "wkdescrerr", "wkcuentas", "wktotreg" })
public class Apxpgm910Result {

	@XmlElement(name = "WK_PAGINAMOS", required = true)
	protected String wkpaginamos;
	@XmlElement(name = "WK_TOTALPAG", required = true)
	protected String wktotalpag;
	@XmlElement(name = "WK_MASDATOS", required = true)
	protected String wkmasdatos;
	@XmlElement(name = "ARREGLO", required = true)
	protected List<Arreglo> arreglo;
	@XmlElement(name = "WK_DESCRERR", required = true)
	protected String wkdescrerr;
	@XmlElement(name = "WK_CUENTAS", required = true)
	protected String wkcuentas;
	@XmlElement(name = "WK_TOTREG", required = true)
	protected String wktotreg;

	/**
	 * Obtiene el valor de la propiedad wkpaginamos.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWKPAGINAMOS() {
		return wkpaginamos;
	}

	/**
	 * Define el valor de la propiedad wkpaginamos.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWKPAGINAMOS(String value) {
		this.wkpaginamos = value;
	}

	/**
	 * Obtiene el valor de la propiedad wktotalpag.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWKTOTALPAG() {
		return wktotalpag;
	}

	/**
	 * Define el valor de la propiedad wktotalpag.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWKTOTALPAG(String value) {
		this.wktotalpag = value;
	}

	/**
	 * Obtiene el valor de la propiedad wkmasdatos.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWKMASDATOS() {
		return wkmasdatos;
	}

	/**
	 * Define el valor de la propiedad wkmasdatos.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWKMASDATOS(String value) {
		this.wkmasdatos = value;
	}

	/**
	 * Gets the value of the arreglo property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the arreglo property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getARREGLO().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link Arreglo }
	 * 
	 * 
	 */
	public List<Arreglo> getARREGLO() {
		if (arreglo == null) {
			arreglo = new ArrayList<>();
		}
		return this.arreglo;
	}

	/**
	 * Obtiene el valor de la propiedad wkdescrerr.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWKDESCRERR() {
		return wkdescrerr;
	}

	/**
	 * Define el valor de la propiedad wkdescrerr.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWKDESCRERR(String value) {
		this.wkdescrerr = value;
	}

	/**
	 * Obtiene el valor de la propiedad wkcuentas.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWKCUENTAS() {
		return wkcuentas;
	}

	/**
	 * Define el valor de la propiedad wkcuentas.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWKCUENTAS(String value) {
		this.wkcuentas = value;
	}

	/**
	 * Obtiene el valor de la propiedad wktotreg.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWKTOTREG() {
		return wktotreg;
	}

	/**
	 * Define el valor de la propiedad wktotreg.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWKTOTREG(String value) {
		this.wktotreg = value;
	}

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(150);
        builder.append("Apxpgm910Result [wkpaginamos=").append(wkpaginamos).append(", wktotalpag=").append(wktotalpag)
                .append(", wkmasdatos=").append(wkmasdatos).append(", arreglo=").append(arreglo).append(", wkdescrerr=").append(wkdescrerr)
                .append(", wkcuentas=").append(wkcuentas).append(", wktotreg=").append(wktotreg).append(']');
        return builder.toString();
    }

}
