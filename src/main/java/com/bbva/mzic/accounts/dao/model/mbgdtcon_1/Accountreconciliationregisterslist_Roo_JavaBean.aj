// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.mbgdtcon_1;

import java.lang.String;

privileged aspect Accountreconciliationregisterslist_Roo_JavaBean {
    
    public String Accountreconciliationregisterslist.getAccounttype() {
        return this.accounttype;
    }
    
    public void Accountreconciliationregisterslist.setAccounttype(String accounttype) {
        this.accounttype = accounttype;
    }
    
    public String Accountreconciliationregisterslist.getTriggeraccountkey() {
        return this.triggeraccountkey;
    }
    
    public void Accountreconciliationregisterslist.setTriggeraccountkey(String triggeraccountkey) {
        this.triggeraccountkey = triggeraccountkey;
    }
    
    public String Accountreconciliationregisterslist.getAccountorsubaccount() {
        return this.accountorsubaccount;
    }
    
    public void Accountreconciliationregisterslist.setAccountorsubaccount(String accountorsubaccount) {
        this.accountorsubaccount = accountorsubaccount;
    }
    
    public String Accountreconciliationregisterslist.getOperationnumberofaccountorsubaccount() {
        return this.operationnumberofaccountorsubaccount;
    }
    
    public void Accountreconciliationregisterslist.setOperationnumberofaccountorsubaccount(String operationnumberofaccountorsubaccount) {
        this.operationnumberofaccountorsubaccount = operationnumberofaccountorsubaccount;
    }
    
    public String Accountreconciliationregisterslist.getTargetsubaccount() {
        return this.targetsubaccount;
    }
    
    public void Accountreconciliationregisterslist.setTargetsubaccount(String targetsubaccount) {
        this.targetsubaccount = targetsubaccount;
    }
    
    public String Accountreconciliationregisterslist.getOperationnumberoftargetsubaccount() {
        return this.operationnumberoftargetsubaccount;
    }
    
    public void Accountreconciliationregisterslist.setOperationnumberoftargetsubaccount(String operationnumberoftargetsubaccount) {
        this.operationnumberoftargetsubaccount = operationnumberoftargetsubaccount;
    }
    
    public String Accountreconciliationregisterslist.getPayersubaccount() {
        return this.payersubaccount;
    }
    
    public void Accountreconciliationregisterslist.setPayersubaccount(String payersubaccount) {
        this.payersubaccount = payersubaccount;
    }
    
    public String Accountreconciliationregisterslist.getOperationnumberofpayersubaccount() {
        return this.operationnumberofpayersubaccount;
    }
    
    public void Accountreconciliationregisterslist.setOperationnumberofpayersubaccount(String operationnumberofpayersubaccount) {
        this.operationnumberofpayersubaccount = operationnumberofpayersubaccount;
    }
    
    public String Accountreconciliationregisterslist.getDescription() {
        return this.description;
    }
    
    public void Accountreconciliationregisterslist.setDescription(String description) {
        this.description = description;
    }
    
    public String Accountreconciliationregisterslist.getReference() {
        return this.reference;
    }
    
    public void Accountreconciliationregisterslist.setReference(String reference) {
        this.reference = reference;
    }
    
    public String Accountreconciliationregisterslist.getExtendedreference() {
        return this.extendedreference;
    }
    
    public void Accountreconciliationregisterslist.setExtendedreference(String extendedreference) {
        this.extendedreference = extendedreference;
    }
    
    public String Accountreconciliationregisterslist.getOperationamount() {
        return this.operationamount;
    }
    
    public void Accountreconciliationregisterslist.setOperationamount(String operationamount) {
        this.operationamount = operationamount;
    }
    
    public String Accountreconciliationregisterslist.getOperationdate() {
        return this.operationdate;
    }
    
    public void Accountreconciliationregisterslist.setOperationdate(String operationdate) {
        this.operationdate = operationdate;
    }
    
    public String Accountreconciliationregisterslist.getOperationtimestamp() {
        return this.operationtimestamp;
    }
    
    public void Accountreconciliationregisterslist.setOperationtimestamp(String operationtimestamp) {
        this.operationtimestamp = operationtimestamp;
    }
    
    public String Accountreconciliationregisterslist.getConcentrationaccount() {
        return this.concentrationaccount;
    }
    
    public void Accountreconciliationregisterslist.setConcentrationaccount(String concentrationaccount) {
        this.concentrationaccount = concentrationaccount;
    }
    
    public String Accountreconciliationregisterslist.getOperationcurrency() {
        return this.operationcurrency;
    }
    
    public void Accountreconciliationregisterslist.setOperationcurrency(String operationcurrency) {
        this.operationcurrency = operationcurrency;
    }
    
}
