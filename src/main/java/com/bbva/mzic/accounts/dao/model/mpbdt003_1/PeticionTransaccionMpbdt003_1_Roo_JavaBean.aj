// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.mpbdt003_1;

import java.lang.String;
import java.util.List;

privileged aspect PeticionTransaccionMpbdt003_1_Roo_JavaBean {
    
    public String PeticionTransaccionMpbdt003_1.getNumerocliente() {
        return this.numerocliente;
    }
    
    public void PeticionTransaccionMpbdt003_1.setNumerocliente(String numerocliente) {
        this.numerocliente = numerocliente;
    }
    
    public String PeticionTransaccionMpbdt003_1.getTipoevidencia() {
        return this.tipoevidencia;
    }
    
    public void PeticionTransaccionMpbdt003_1.setTipoevidencia(String tipoevidencia) {
        this.tipoevidencia = tipoevidencia;
    }
    
    public String PeticionTransaccionMpbdt003_1.getPlantillaruc() {
        return this.plantillaruc;
    }
    
    public void PeticionTransaccionMpbdt003_1.setPlantillaruc(String plantillaruc) {
        this.plantillaruc = plantillaruc;
    }
    
    public List<Variableevidencia> PeticionTransaccionMpbdt003_1.getVariableevidencia() {
        return this.variableevidencia;
    }
    
    public void PeticionTransaccionMpbdt003_1.setVariableevidencia(List<Variableevidencia> variableevidencia) {
        this.variableevidencia = variableevidencia;
    }
    
}
