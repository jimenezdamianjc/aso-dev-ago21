package com.bbva.mzic.accounts.dao.model.mchft102_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Bean fila para el campo tabular <code>whyHealth</code>, utilizado por la clase <code>Month</code>
 * </p>
 * 
 * @see Month
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Whyhealth {

    /**
     * <p>
     * Campo <code>idWhyHealth</code>, &iacute;ndice: <code>1</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 1, nombre = "idWhyHealth", tipo = TipoCampo.ENTERO, longitudMaxima = 10, signo = true)
    private Long idwhyhealth;

    /**
     * <p>
     * Campo <code>whyHealthDetail</code>, &iacute;ndice: <code>2</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "whyHealthDetail", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 125, signo = true)
    private String whyhealthdetail;

}
