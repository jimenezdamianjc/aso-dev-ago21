package com.bbva.mzic.accounts.dao.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.bbva.jee.arq.spring.core.host.IMapper;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.mzic.accounts.business.dto.DtoIntAccount;
import com.bbva.mzic.accounts.dao.model.bgl5.FormatoBGML5YS;
import com.bbva.mzic.accounts.dao.model.bgl5.RespuestaTransaccionBgl5;
import com.bbva.mzic.serviceutils.rm.utils.tx.FilterResponseHost;

public class OutputBGML5Mapper implements IMapper<RespuestaTransaccionBgl5, DtoIntAccount> {
	@Override
	public DtoIntAccount map(RespuestaTransaccionBgl5 response) {

		if (response == null) {
			return null;
		}

		final List<Object> formats = response.getCuerpo().getPartes().stream()
				.filter(cs -> cs != null && cs.getClass().equals(CopySalida.class)).map(cs -> (CopySalida) cs)
				.map(CopySalida::getCopy).collect(Collectors.toList());

		final FormatoBGML5YS formato = FilterResponseHost.findFirstFormat(formats, FormatoBGML5YS.class);

		final DtoIntAccount dto = new DtoIntAccount();
		// ...
		return dto;
	}
}
