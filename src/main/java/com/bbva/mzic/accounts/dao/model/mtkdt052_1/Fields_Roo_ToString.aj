// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.mtkdt052_1;

import java.lang.String;

privileged aspect Fields_Roo_ToString {
    
    public String Fields.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Container: ").append(getContainer()).append(", ");
        sb.append("Datatype: ").append(getDatatype()).append(", ");
        sb.append("Description: ").append(getDescription()).append(", ");
        sb.append("Id: ").append(getId()).append(", ");
        sb.append("Position: ").append(getPosition());
        return sb.toString();
    }
    
}
