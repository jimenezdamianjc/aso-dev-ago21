package com.bbva.mzic.accounts.dao.model.peah;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>PEM0AHE</code> de la transacci&oacute;n <code>PEAH</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "PEM0AHE")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoPEM0AHE {

	/**
	 * <p>Campo <code>NUMCLIE</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "NUMCLIE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
	private String numclie;
	
	/**
	 * <p>Campo <code>IDENTIF</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "IDENTIF", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 14, longitudMaxima = 14)
	private String identif;
	
}
