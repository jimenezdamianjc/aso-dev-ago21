package com.bbva.mzic.accounts.dao.model.mbgdtchd_1;

import java.util.List;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Bean fila para el campo tabular <code>digitalChecks</code>, utilizado por la clase
 * <code>RespuestaTransaccionMbgdtchd_1</code>
 * </p>
 * 
 * @see RespuestaTransaccionMbgdtchd_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Digitalchecks {

    /**
     * <p>
     * Campo <code>id</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "id", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 13, signo = true, obligatorio = true)
    private String id;

    /**
     * <p>
     * Campo <code>number</code>, &iacute;ndice: <code>2</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 2, nombre = "number", tipo = TipoCampo.ENTERO, longitudMaxima = 13, signo = true, obligatorio = true)
    private long number;

    /**
     * <p>
     * Campo <code>concept</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "concept", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 55, signo = true, obligatorio = true)
    private String concept;

    /**
     * <p>
     * Campo <code>operationDate</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "operationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
    private String operationdate;

    /**
     * <p>
     * Campo <code>dueDate</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "dueDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
    private String duedate;

    /**
     * <p>
     * Campo <code>operationNumber</code>, &iacute;ndice: <code>6</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "operationNumber", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
    private String operationnumber;

    /**
     * <p>
     * Campo <code>receiver</code>, &iacute;ndice: <code>7</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 7, nombre = "receiver", tipo = TipoCampo.TABULAR)
    private List<Receiver> receiver;

    /**
     * <p>
     * Campo <code>sentMoney</code>, &iacute;ndice: <code>8</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 8, nombre = "sentMoney", tipo = TipoCampo.TABULAR)
    private List<Sentmoney> sentmoney;

    /**
     * <p>
     * Campo <code>digitalCheckStatus</code>, &iacute;ndice: <code>9</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 9, nombre = "digitalCheckStatus", tipo = TipoCampo.TABULAR)
    private List<Digitalcheckstatus> digitalcheckstatus;

    /**
     * <p>
     * Campo <code>digitalCheckType</code>, &iacute;ndice: <code>10</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 10, nombre = "digitalCheckType", tipo = TipoCampo.TABULAR)
    private List<Digitalchecktype> digitalchecktype;

    /**
     * <p>
     * Campo <code>cashOut</code>, &iacute;ndice: <code>11</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 11, nombre = "cashOut", tipo = TipoCampo.TABULAR)
    private List<Cashout> cashout;

}
