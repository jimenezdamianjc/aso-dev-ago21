package com.bbva.mzic.accounts.dao.mapper;

import com.bbva.jee.arq.spring.core.host.IMapper;
import com.bbva.mzic.accounts.business.dto.DtoIntFilterAccount;
import com.bbva.mzic.accounts.dao.model.bgl5.FormatoBGML5E;
import com.bbva.mzic.accounts.dao.model.bgl5.PeticionTransaccionBgl5;

public class InputBGML5Mapper implements IMapper<DtoIntFilterAccount, PeticionTransaccionBgl5> {

	@Override
	public PeticionTransaccionBgl5 map(DtoIntFilterAccount filter) {

		if (filter == null) {
			return null;
		}

		final PeticionTransaccionBgl5 peticionTransaccionBgl5 = new PeticionTransaccionBgl5();

		final FormatoBGML5E formatoBGML5E = new FormatoBGML5E();
		// formatoBGML5E.setTipoasu(filter.getAccountType());
		// formatoBGML5E.setAsunto(filter.getAccountNumber());
		// if (!StringUtils.isEmpty(filter.getClientId())) {
		// formatoBGML5E.setClienpu(filter.getClientId());
		// }

		peticionTransaccionBgl5.getCuerpo().getPartes().add(formatoBGML5E);

		return peticionTransaccionBgl5;

	}
}
