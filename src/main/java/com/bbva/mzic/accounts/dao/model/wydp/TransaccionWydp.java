package com.bbva.mzic.accounts.dao.model.wydp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>WYDP</code>
 * 
 * @see PeticionTransaccionWydp
 * @see RespuestaTransaccionWydp
 */
@Component("transaccionWydp-v0")
public class TransaccionWydp implements InvocadorTransaccion<PeticionTransaccionWydp, RespuestaTransaccionWydp> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionWydp invocar(PeticionTransaccionWydp transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionWydp.class, RespuestaTransaccionWydp.class, transaccion);
	}

	@Override
	public RespuestaTransaccionWydp invocarCache(PeticionTransaccionWydp transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionWydp.class, RespuestaTransaccionWydp.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionWydp.class, "vaciearCache");
	}
}
