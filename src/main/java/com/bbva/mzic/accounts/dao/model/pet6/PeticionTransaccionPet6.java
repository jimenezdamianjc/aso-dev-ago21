package com.bbva.mzic.accounts.dao.model.pet6;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

/**
 * <p>
 * Transacci&oacute;n <code>PET6</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionPet6</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionPet6</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: PET6CCT.txt
 * PET6ALTA DE SPEI MOVIL                 PE        PE2C00T6     01 PEM0T6E             PET6  NN3000CNNNNN    SSTN A      NNNSNNNN  NN                2016-07-18XMZ1728 2017-05-2410.30.47XM01BAW 2016-07-18-16.12.26.573790XMZ1728 0001-01-010001-01-01
 * 
 * FICHERO: PET6FDF.txt
 * PEM0T6E �CAMPOS ENTRADA ALTA SPEI      �F�09�00483�01�00001�NUMCLIE�NUMERO DE CLIENTE   �A�008�0�R�        �
 * PEM0T6E �CAMPOS ENTRADA ALTA SPEI      �F�09�00483�02�00009�CUENTA �NUMERO DE CUENTA    �A�018�0�R�        �
 * PEM0T6E �CAMPOS ENTRADA ALTA SPEI      �F�09�00483�03�00027�TELEFON�NUMERO TELEFONICO   �A�010�0�R�        �
 * PEM0T6E �CAMPOS ENTRADA ALTA SPEI      �F�09�00483�04�00037�FOLIO  �NUMERO DE FOLIO     �A�010�0�O�        �
 * PEM0T6E �CAMPOS ENTRADA ALTA SPEI      �F�09�00483�05�00047�HASH1  �HASH DATOS PARTE UNO�A�080�0�O�        �
 * PEM0T6E �CAMPOS ENTRADA ALTA SPEI      �F�09�00483�06�00127�HASH2  �HASH DATOS PARTE DOS�A�080�0�O�        �
 * PEM0T6E �CAMPOS ENTRADA ALTA SPEI      �F�09�00483�07�00207�LOGAST �LOG DE AST          �A�100�0�O�        �
 * PEM0T6E �CAMPOS ENTRADA ALTA SPEI      �F�09�00483�08�00307�LOGWAS1�LOG WAS PARTE 1     �A�090�0�O�        �
 * PEM0T6E �CAMPOS ENTRADA ALTA SPEI      �F�09�00483�09�00397�LOGWAS2�LOG WAS PARTE 2     �A�087�0�O�        �
 * PEM0T6S �DATOS SALIDA ALTA SPEI        �X�07�00116�01�00001�FOLIO  �NUMERO DE FOLIO     �A�010�0�S�        �
 * PEM0T6S �DATOS SALIDA ALTA SPEI        �X�07�00116�02�00011�DIA    �DIA                 �A�002�0�S�        �
 * PEM0T6S �DATOS SALIDA ALTA SPEI        �X�07�00116�03�00013�MES    �MES                 �A�010�0�S�        �
 * PEM0T6S �DATOS SALIDA ALTA SPEI        �X�07�00116�04�00023�ANIO   �ANIO                �A�004�0�S�        �
 * PEM0T6S �DATOS SALIDA ALTA SPEI        �X�07�00116�05�00027�CUENTA �CUENTA              �A�020�0�S�        �
 * PEM0T6S �DATOS SALIDA ALTA SPEI        �X�07�00116�06�00047�CELULAR�CELULAR             �A�010�0�S�        �
 * PEM0T6S �DATOS SALIDA ALTA SPEI        �X�07�00116�07�00057�NOMCTE �NOMCTE              �A�060�0�S�        �
 * 
 * FICHERO: PET6FDX.txt
 * PET6PEM0T6S PENC0T6SPE2C00T61S                             CICSDM112016-10-12-18.47.42.303556CICSDM112016-10-12-18.47.42.303577
 * 
</pre></code>
 * 
 * @see RespuestaTransaccionPet6
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "PET6", tipo = 1, subtipo = 1, version = 1, configuracion = "default_ps9_mx",
        respuesta = RespuestaTransaccionPet6.class,
        atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")})
@Multiformato(formatos = {FormatoPEM0T6E.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionPet6 implements MensajeMultiparte {

    /**
     * <p>
     * Cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Cuerpo
    private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

    /**
     * <p>
     * Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Override
    public CuerpoMultiparte getCuerpo() {
        return cuerpo;
    }

}
