package com.bbva.mzic.accounts.dao.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.bbva.jee.arq.spring.core.host.IMapper;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.mzic.accounts.business.dto.DtoIntConditions;
import com.bbva.mzic.accounts.dao.model.bgl5.RespuestaTransaccionBgl5;
import com.bbva.mzic.accounts.rm.enums.ConditionEnum;

public class OutputBGML5Mapper1 implements IMapper<RespuestaTransaccionBgl5, List<DtoIntConditions>> {

	@Override
	public List<DtoIntConditions> map(RespuestaTransaccionBgl5 response) {

		List<DtoIntConditions> listResponse = null;

		if (response == null) {
			return listResponse;
		}

		response.getCuerpo().getPartes().stream().filter(cs -> cs != null && cs.getClass().equals(CopySalida.class))
				.map(cs -> (CopySalida) cs).map(CopySalida::getCopy).collect(Collectors.toList());

		listResponse = new ArrayList<>();

		final DtoIntConditions item1 = new DtoIntConditions();
		item1.setConditionId(ConditionEnum.MIN_BALANCE_FEE);
		item1.setName("Minimun balance fee");
		listResponse.add(item1);

		final DtoIntConditions item2 = new DtoIntConditions();
		item2.setConditionId(ConditionEnum.MEMBERSHIP_FEE);
		item2.setName("Membership Fee");
		listResponse.add(item2);

		return listResponse;
	}

}
