package com.bbva.mzic.accounts.dao.model.bgl4;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.mzic.serviceutils.rm.utils.tx.IFormat;

/**
 * Formato de datos <code>BGML4E</code> de la transacci&oacute;n
 * <code>BGL4</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BGML4E")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoBGML4E implements IFormat {

	/**
	 * <p>
	 * Campo <code>AREAPAG</code>, &iacute;ndice: <code>1</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1,
			nombre = "AREAPAG",
			tipo = TipoCampo.ALFANUMERICO,
			longitudMinima = 48,
			longitudMaxima = 48)
	private String areapag;

	/**
	 * <p>
	 * Campo <code>FAMPROD</code>, &iacute;ndice: <code>2</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2,
			nombre = "FAMPROD",
			tipo = TipoCampo.ALFANUMERICO,
			longitudMinima = 24,
			longitudMaxima = 24)
	private String famprod;

	/**
	 * <p>
	 * Campo <code>CLIENPU</code>, &iacute;ndice: <code>3</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3,
			nombre = "CLIENPU",
			tipo = TipoCampo.ALFANUMERICO,
			longitudMinima = 8,
			longitudMaxima = 8)
	private String clienpu;

	/**
	 * <p>
	 * Campo <code>USUARIO</code>, &iacute;ndice: <code>4</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4,
			nombre = "USUARIO",
			tipo = TipoCampo.ALFANUMERICO,
			longitudMinima = 8,
			longitudMaxima = 8)
	private String usuario;

	/**
	 * <p>
	 * Campo <code>BUSINID</code>, &iacute;ndice: <code>5</code>, tipo:
	 * <code>ENTERO</code>
	 */
	@Campo(indice = 5,
			nombre = "BUSINID",
			tipo = TipoCampo.ENTERO,
			longitudMinima = 4,
			longitudMaxima = 4)
	private Integer businid;

}
