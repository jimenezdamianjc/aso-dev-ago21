package com.bbva.mzic.accounts.dao.model.apx.mtkdt019_1;

import java.util.List;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Bean fila para el campo tabular <code>account</code>, utilizado por la clase
 * <code>RespuestaTransaccionMtkdt019_1</code>
 * </p>
 * 
 * @see RespuestaTransaccionMtkdt019_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Account {

    /**
     * <p>
     * Campo <code>accountNumber</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "accountNumber", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 6, signo = true)
    private String accountnumber;

    /**
     * <p>
     * Campo <code>holderFirstName</code>, &iacute;ndice: <code>2</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "holderFirstName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 60, signo = true)
    private String holderfirstname;

    /**
     * <p>
     * Campo <code>holderLastName</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "holderLastName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 40, signo = true)
    private String holderlastname;

    /**
     * <p>
     * Campo <code>holderSecondLastName</code>, &iacute;ndice: <code>4</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "holderSecondLastName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 40, signo = true)
    private String holdersecondlastname;

    /**
     * <p>
     * Campo <code>customerId</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "customerId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 8, signo = true)
    private String customerid;

    /**
     * <p>
     * Campo <code>holderMiddleName</code>, &iacute;ndice: <code>6</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "holderMiddleName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 16, signo = true)
    private String holdermiddlename;

    /**
     * <p>
     * Campo <code>suffix</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 7, nombre = "suffix", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 5, signo = true)
    private String suffix;

    /**
     * <p>
     * Campo <code>formats</code>, &iacute;ndice: <code>8</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 8, nombre = "formats", tipo = TipoCampo.TABULAR)
    private List<Formats> formats;

    /**
     * <p>
     * Campo <code>accountFamily</code>, &iacute;ndice: <code>9</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 9, nombre = "accountFamily", tipo = TipoCampo.TABULAR)
    private List<Accountfamily> accountfamily;

    /**
     * <p>
     * Campo <code>tittle</code>, &iacute;ndice: <code>10</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 10, nombre = "tittle", tipo = TipoCampo.TABULAR)
    private List<Tittle> tittle;

    /**
     * <p>
     * Campo <code>status</code>, &iacute;ndice: <code>11</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 11, nombre = "status", tipo = TipoCampo.TABULAR)
    private List<Status> status;

    /**
     * <p>
     * Campo <code>participantType</code>, &iacute;ndice: <code>12</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 12, nombre = "participantType", tipo = TipoCampo.TABULAR)
    private List<Participanttype> participanttype;

    /**
     * <p>
     * Campo <code>numberType</code>, &iacute;ndice: <code>13</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 13, nombre = "numberType", tipo = TipoCampo.TABULAR)
    private List<Numbertype> numbertype;

    /**
     * <p>
     * Campo <code>accountType</code>, &iacute;ndice: <code>14</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 14, nombre = "accountType", tipo = TipoCampo.TABULAR)
    private List<Accounttype> accounttype;

}
