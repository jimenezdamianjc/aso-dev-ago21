package com.bbva.mzic.accounts.dao.model.mchqt003_1;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

/**
 * <p>
 * Transacci&oacute;n <code>MCHQT003</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMchqt003_1</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionMchqt003_1</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: MCHQT003-01-MX.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;&lt;transaction xmlns=&quot;http://www.w3schools.com&quot; transactionName=&quot;MCHQT003&quot; application=&quot;MCHQ&quot; version=&quot;01&quot; country=&quot;MX&quot; language=&quot;EN&quot;&gt;&lt;paramsIn&gt;&lt;parameter order=&quot;1&quot; name=&quot;incomeId&quot; type=&quot;Long&quot; size=&quot;18&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;2&quot; name=&quot;accountId&quot; type=&quot;String&quot; size=&quot;18&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;3&quot; name=&quot;userId&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;4&quot; name=&quot;statusId&quot; type=&quot;String&quot; size=&quot;30&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;5&quot; name=&quot;reasonId&quot; type=&quot;String&quot; size=&quot;30&quot; mandatory=&quot;0&quot;/&gt;&lt;parameter order=&quot;6&quot; name=&quot;reasonDescription&quot; type=&quot;String&quot; size=&quot;255&quot; mandatory=&quot;0&quot;/&gt;&lt;parameter order=&quot;7&quot; name=&quot;reasonDetail&quot; type=&quot;String&quot; size=&quot;255&quot; mandatory=&quot;0&quot;/&gt;&lt;/paramsIn&gt;&lt;paramsOut&gt;&lt;parameter order=&quot;1&quot; name=&quot;messageResponse&quot; type=&quot;String&quot; size=&quot;100&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;2&quot; name=&quot;incomeId&quot; type=&quot;Long&quot; size=&quot;18&quot; mandatory=&quot;0&quot;/&gt;&lt;/paramsOut&gt;&lt;description&gt;Actualizacion de la operacion del cheque&lt;/description&gt;&lt;/transaction&gt;
</pre></code>
 *
 * @see RespuestaTransaccionMchqt003_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "MCHQT003", tipo = 1, subtipo = 1, version = 1, configuracion = "default_apx",
        respuesta = RespuestaTransaccionMchqt003_1.class, atributos = {@Atributo(nombre = "country", valor = "MX")})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMchqt003_1 {

    /**
     * <p>
     * Campo <code>incomeId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 1, nombre = "incomeId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 18, signo = true, obligatorio = true)
    private String incomeid;

    /**
     * <p>
     * Campo <code>accountId</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "accountId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 18, signo = true, obligatorio = true)
    private String accountid;

    /**
     * <p>
     * Campo <code>userId</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "userId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 50, signo = true)
    private String userid;

    /**
     * <p>
     * Campo <code>statusId</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "statusId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 30, signo = true, obligatorio = true)
    private String statusid;

    /**
     * <p>
     * Campo <code>reasonId</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "reasonId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 30, signo = true)
    private String reasonid;

    /**
     * <p>
     * Campo <code>reasonDescription</code>, &iacute;ndice: <code>6</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "reasonDescription", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 255, signo = true)
    private String reasondescription;

    /**
     * <p>
     * Campo <code>reasonDetail</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 7, nombre = "reasonDetail", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 255, signo = true)
    private String reasondetail;

}
