package com.bbva.mzic.accounts.dao.model.wyis;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;


/**
 * Formato de datos <code>KNDBSIS3</code> de la transacci&oacute;n <code>WYIS</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KNDBSIS3")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoKNDBSIS3 {

    /**
     * <p>
     * Campo <code>CTANOM</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "CTANOM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String ctanom;

    /**
     * <p>
     * Campo <code>CLAVDOC</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "CLAVDOC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
    private String clavdoc;

    /**
     * <p>
     * Campo <code>DESCDOC</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "DESCDOC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 19, longitudMaxima = 19)
    private String descdoc;

    /**
     * <p>
     * Campo <code>TIPODOC</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "TIPODOC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String tipodoc;

    /**
     * <p>
     * Campo <code>DESCTPD</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "DESCTPD", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
    private String desctpd;

    /**
     * <p>
     * Campo <code>PRODUCT</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "PRODUCT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String product;

    /**
     * <p>
     * Campo <code>DESCPRO</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 7, nombre = "DESCPRO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
    private String descpro;

    /**
     * <p>
     * Campo <code>SECREFE</code>, &iacute;ndice: <code>8</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 8, nombre = "SECREFE", tipo = TipoCampo.ENTERO, longitudMinima = 3, longitudMaxima = 3)
    private Integer secrefe;

    /**
     * <p>
     * Campo <code>REFEREN</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 9, nombre = "REFEREN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 40, longitudMaxima = 40)
    private String referen;

    /**
     * <p>
     * Campo <code>MATRIZ</code>, &iacute;ndice: <code>10</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 10, nombre = "MATRIZ", tipo = TipoCampo.ENTERO, longitudMinima = 6, longitudMaxima = 6)
    private Integer matriz;

}
