package com.bbva.mzic.accounts.dao.model.mcnht020_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MCNHT020</code>
 * 
 * @see PeticionTransaccionMcnht020_1
 * @see RespuestaTransaccionMcnht020_1
 */
@Component
public class TransaccionMcnht020_1 implements InvocadorTransaccion<PeticionTransaccionMcnht020_1, RespuestaTransaccionMcnht020_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMcnht020_1 invocar(PeticionTransaccionMcnht020_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMcnht020_1.class, RespuestaTransaccionMcnht020_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMcnht020_1 invocarCache(PeticionTransaccionMcnht020_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMcnht020_1.class, RespuestaTransaccionMcnht020_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMcnht020_1.class, "vaciearCache");
	}
}
