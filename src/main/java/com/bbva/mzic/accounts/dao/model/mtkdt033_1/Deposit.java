package com.bbva.mzic.accounts.dao.model.mtkdt033_1;

import java.util.List;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * <p>
 * Bean fila para el campo tabular <code>deposit</code>, utilizado por la clase
 * <code>PeticionTransaccionMtkdt033_1</code>
 * </p>
 * 
 * @see PeticionTransaccionMtkdt033_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Deposit {

    /**
     * <p>
     * Campo <code>amount</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "amount", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 14, signo = true, obligatorio = true)
    private String amount;

    /**
     * <p>
     * Campo <code>mainReference</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "mainReference", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 30, signo = true, obligatorio = true)
    private String mainreference;

    /**
     * <p>
     * Campo <code>creationDate</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "creationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true)
    private String creationdate;

    /**
     * <p>
     * Campo <code>reason</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "reason", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 100, signo = true)
    private String reason;

    /**
     * <p>
     * Campo <code>applicationDate</code>, &iacute;ndice: <code>5</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "applicationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
    private String applicationdate;

    /**
     * <p>
     * Campo <code>typeId</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "typeId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 5, signo = true, obligatorio = true)
    private String typeid;

    /**
     * <p>
     * Campo <code>documentId</code>, &iacute;ndice: <code>7</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 7, nombre = "documentId", tipo = TipoCampo.ENTERO, longitudMaxima = 5, signo = true, obligatorio = true)
    private int documentid;

    /**
     * <p>
     * Campo <code>movementReferences</code>, &iacute;ndice: <code>8</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 8, nombre = "movementReferences", tipo = TipoCampo.TABULAR)
    private List<Movementreferences> movementreferences;

}
