package com.bbva.mzic.accounts.dao.model.bnm2;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>BNM0M2S</code> de la transacci&oacute;n <code>BNM2</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BNM0M2S")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoBNM0M2S {

    /**
     * <p>
     * Campo <code>BNSTCAM</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "BNSTCAM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
    private String bnstcam;

}
