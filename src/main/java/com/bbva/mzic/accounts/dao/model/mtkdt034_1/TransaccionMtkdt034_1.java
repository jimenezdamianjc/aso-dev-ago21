package com.bbva.mzic.accounts.dao.model.mtkdt034_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MTKDT034</code>
 * 
 * @see PeticionTransaccionMtkdt034_1
 * @see RespuestaTransaccionMtkdt034_1
 */
@Component
public class TransaccionMtkdt034_1 implements InvocadorTransaccion<PeticionTransaccionMtkdt034_1, RespuestaTransaccionMtkdt034_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMtkdt034_1 invocar(PeticionTransaccionMtkdt034_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt034_1.class, RespuestaTransaccionMtkdt034_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMtkdt034_1 invocarCache(PeticionTransaccionMtkdt034_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt034_1.class, RespuestaTransaccionMtkdt034_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMtkdt034_1.class, "vaciearCache");
	}
}
