package com.bbva.mzic.accounts.dao.model.mchqt003_1;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Cabecera;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.NombreCabecera;
import com.bbva.jee.arq.spring.core.host.RespuestaTransaccion;
import com.bbva.jee.arq.spring.core.host.TipoCampo;


/**
 * Bean de respuesta para la transacci&oacute;n <code>MCHQT003</code>
 *
 * @see PeticionTransaccionMchqt003_1
 */
@RespuestaTransaccion
@Formato(nombre = "1")
@RooJavaBean
@RooToString
@RooSerializable
public class RespuestaTransaccionMchqt003_1 {

    /**
     * <p>
     * Cabecera <code>COD-AVISO</code>
     * </p>
     */
    @Cabecera(nombre = NombreCabecera.CODIGO_AVISO)
    private String codigoAviso;

    /**
     * <p>
     * Cabecera <code>DES-AVISO</code>
     * </p>
     */
    @Cabecera(nombre = NombreCabecera.DESCRIPCION_AVISO)
    private String descripcionAviso;

    /**
     * <p>
     * Cabecera <code>COD-UUAA-AVISO</code>
     * </p>
     */
    @Cabecera(nombre = NombreCabecera.APLICACION_AVISO)
    private String aplicacionAviso;

    /**
     * <p>
     * Cabecera <code>COD-RETORNO</code>
     * </p>
     */
    @Cabecera(nombre = NombreCabecera.CODIGO_RETORNO)
    private String codigoRetorno;

    /**
     * <p>
     * Campo <code>messageResponse</code>, &iacute;ndice: <code>1</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "messageResponse", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 100, signo = true, obligatorio = true)
    private String messageresponse;

    /**
     * <p>
     * Campo <code>campo_2_incomeId</code>, &iacute;ndice: <code>2</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 2, nombre = "incomeId", tipo = TipoCampo.ENTERO, longitudMaxima = 18, signo = true)
    private Long incomeid;

}
