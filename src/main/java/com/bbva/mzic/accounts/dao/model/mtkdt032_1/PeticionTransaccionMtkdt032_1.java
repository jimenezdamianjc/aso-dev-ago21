package com.bbva.mzic.accounts.dao.model.mtkdt032_1;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MTKDT032</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMtkdt032_1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMtkdt032_1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: MTKDT032.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot;
 * transactionName=&quot;MTKDT032&quot; application=&quot;MTKD&quot; version=&quot;01&quot; country=&quot;MX&quot;
 * language=&quot;ES&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;ticketId&quot; type=&quot;Long&quot; size=&quot;13&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;accountId&quot; type=&quot;String&quot; size=&quot;6&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;functionaryId&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;functionaryName&quot; type=&quot;String&quot; size=&quot;90&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;bankId&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;bankName&quot; type=&quot;String&quot; size=&quot;13&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;branchId&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;branchName&quot; type=&quot;String&quot; size=&quot;13&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;7&quot; name=&quot;withdrawalAmount&quot; type=&quot;Double&quot; size=&quot;14&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;8&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;9&quot; name=&quot;withdrawalMainReference&quot; type=&quot;String&quot; size=&quot;100&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;10&quot; name=&quot;withdrawalCreationDate&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;11&quot; name=&quot;withdrawalReason&quot; type=&quot;String&quot; size=&quot;100&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;12&quot; name=&quot;withdrawalTypeId&quot; type=&quot;Long&quot; size=&quot;5&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;13&quot; name=&quot;withdrawalTypeDesc&quot; type=&quot;String&quot; size=&quot;40&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;14&quot; name=&quot;withdrawalDocumentId&quot; type=&quot;Long&quot; size=&quot;5&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;15&quot; name=&quot;withdrawalDocumentDesc&quot; type=&quot;String&quot; size=&quot;40&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;16&quot; name=&quot;withdrawalApplicationDate&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;group name=&quot;references&quot; order=&quot;17&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;firstDescription&quot; type=&quot;String&quot; size=&quot;300&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;secondDescription&quot; type=&quot;String&quot; size=&quot;300&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;thirdDescription&quot; type=&quot;String&quot; size=&quot;300&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;fourthDescription&quot; type=&quot;String&quot; size=&quot;300&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Transaccion utilizada para obtener el detalle de una
 * operacion de retiro.&lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionMtkdt032_1
 *
 * @author Arquitectura Spring BBVA
 */
@SuppressWarnings("serial")
@Transaccion(
    nombre = "MTKDT032",
    tipo = 1, 
    subtipo = 1,    
    version = 1,
    configuracion = "default_apx",
    respuesta = RespuestaTransaccionMtkdt032_1.class,
    atributos = {@Atributo(nombre = "country", valor = "MX")}
)
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMtkdt032_1 {

    /**
     * <p>Campo <code>ticketId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 1, nombre = "ticketId", tipo = TipoCampo.ENTERO, longitudMaxima = 13, signo = true, obligatorio = true)
    private long ticketid;

    /**
     * <p>Campo <code>accountId</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "accountId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 6, signo = true, obligatorio = true)
    private String accountid;

}
