package com.bbva.mzic.accounts.dao.model.host.bgpx;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Transacci&oacute;n <code>BGPX</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionBgpx</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionBgpx</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: BGPX-CCT-v3.TXT
 * BGPXCONSULTA DE MOVIMIENTOS API        BG        BG2CJBG2     01 BGMFPX              BGPX  SN0000NNNNNN    SSTN A      NNNSNNNN  NN                2017-05-11XM01AUX 2017-06-2910.51.36CICSDM112017-05-11-11.42.44.083164XM01AUX 0001-01-010001-01-01
 * FICHERO: BGPX-FDF-v3.TXT
 * BGMFPX  �CONSULTA MOVTOS. API          �F�14�00145�01�00001�PAGKEY �LLAVE DE PAGINACION �N�008�0�R�        �
 * BGMFPX  �CONSULTA MOVTOS. API          �F�14�00145�02�00009�PAGSIZE�TAMANO DE LA PAGINA �A�003�0�O�        �
 * BGMFPX  �CONSULTA MOVTOS. API          �F�14�00145�03�00012�PAGSDO �SALDO DE PAGINACION �S�015�2�O�        �
 * BGMFPX  �CONSULTA MOVTOS. API          �F�14�00145�04�00027�ORDER  �ORDEN DEL LISTADO   �A�001�0�R�        �
 * BGMFPX  �CONSULTA MOVTOS. API          �F�14�00145�05�00028�CONTRAC�CONTRATO A 10       �A�010�0�R�        �
 * BGMFPX  �CONSULTA MOVTOS. API          �F�14�00145�06�00038�OPEDATE�FECHA DE OPERACION  �A�010�0�O�        �
 * BGMFPX  �CONSULTA MOVTOS. API          �F�14�00145�07�00048�FOPDATE�FEC OPERACION DESDE �A�010�0�O�        �
 * BGMFPX  �CONSULTA MOVTOS. API          �F�14�00145�08�00058�TOPDATE�FEC OPERACION HASTA �A�010�0�O�        �
 * BGMFPX  �CONSULTA MOVTOS. API          �F�14�00145�09�00068�AMOUNT �IMPORTE             �A�017�0�O�        �
 * BGMFPX  �CONSULTA MOVTOS. API          �F�14�00145�10�00085�FAMOUNT�IMPORTE DESDE       �A�017�0�O�        �
 * BGMFPX  �CONSULTA MOVTOS. API          �F�14�00145�11�00102�TAMOUNT�IMPORTE HASTA       �A�017�0�O�        �
 * BGMFPX  �CONSULTA MOVTOS. API          �F�14�00145�12�00119�FMOVMT �NUMERO MOV. DESDE   �N�013�0�O�        �
 * BGMFPX  �CONSULTA MOVTOS. API          �F�14�00145�13�00132�TMOVMT �NUMERO MOV. HASTA   �N�013�0�O�        �
 * BGMFPX  �CONSULTA MOVTOS. API          �F�14�00145�14�00145�MONEYFL�FLUJO CARGO/ABONO   �A�001�0�O�        �
 * BGMFPXA �CONSULTA MOVTOS. API DATOS    �X�21�00345�01�00001�ID     �NUMERO DE MOVIMIENTO�N�013�0�S�        �
 * BGMFPXA �CONSULTA MOVTOS. API DATOS    �X�21�00345�02�00014�AMOUNT �IMPORTE             �S�015�2�S�        �
 * BGMFPXA �CONSULTA MOVTOS. API DATOS    �X�21�00345�03�00029�CURENCY�DIVISA              �A�003�0�S�        �
 * BGMFPXA �CONSULTA MOVTOS. API DATOS    �X�21�00345�04�00032�MONEYFL�FLUJO CARGO/ABONO   �A�001�0�S�        �
 * BGMFPXA �CONSULTA MOVTOS. API DATOS    �X�21�00345�05�00033�CONCEPT�CONCEPTO            �A�025�0�S�        �
 * BGMFPXA �CONSULTA MOVTOS. API DATOS    �X�21�00345�06�00058�TTYPEID�TIPO TRANSACCION    �A�015�0�S�        �
 * BGMFPXA �CONSULTA MOVTOS. API DATOS    �X�21�00345�07�00073�TTYPENA�NOMBRE TRANSACCION  �A�008�0�S�        �
 * BGMFPXA �CONSULTA MOVTOS. API DATOS    �X�21�00345�08�00081�TTYPEIC�CODIGO DE OPERACON  �A�003�0�S�        �
 * BGMFPXA �CONSULTA MOVTOS. API DATOS    �X�21�00345�09�00084�TTYPEIN�DESCRIP COD OPERACIO�A�034�0�S�        �
 * BGMFPXA �CONSULTA MOVTOS. API DATOS    �X�21�00345�10�00118�OPEDATE�FECHA DE OPERACION  �A�029�0�S�        �
 * BGMFPXA �CONSULTA MOVTOS. API DATOS    �X�21�00345�11�00147�VALDATE�FECHA DE VALOR      �A�029�0�S�        �
 * BGMFPXA �CONSULTA MOVTOS. API DATOS    �X�21�00345�12�00176�ACCDATE�FECHA CONTABLE      �A�029�0�S�        �
 * BGMFPXA �CONSULTA MOVTOS. API DATOS    �X�21�00345�13�00205�FTYPEID�TIPO FINANCIACION   �A�013�0�S�        �
 * BGMFPXA �CONSULTA MOVTOS. API DATOS    �X�21�00345�14�00218�STATUSI�ESTATUS             �A�007�0�S�        �
 * BGMFPXA �CONSULTA MOVTOS. API DATOS    �X�21�00345�15�00225�CNTYPEI�TIPO CONTRATO       �A�003�0�S�        �
 * BGMFPXA �CONSULTA MOVTOS. API DATOS    �X�21�00345�16�00228�COPROID�IDENT. PRODUCTO     �A�008�0�S�        �
 * BGMFPXA �CONSULTA MOVTOS. API DATOS    �X�21�00345�17�00236�COPRONA�NOMBRE PRODUCTO     �A�010�0�S�        �
 * BGMFPXA �CONSULTA MOVTOS. API DATOS    �X�21�00345�18�00246�COALIAS�ALIAS DEL CONTRATO  �A�020�0�S�        �
 * BGMFPXA �CONSULTA MOVTOS. API DATOS    �X�21�00345�19�00266�REFEREN�ADD. DATA REFERENCIA�A�025�0�S�        �
 * BGMFPXA �CONSULTA MOVTOS. API DATOS    �X�21�00345�20�00291�ADDDATA�ADD. DATA MOV REF   �A�040�0�S�        �
 * BGMFPXA �CONSULTA MOVTOS. API DATOS    �X�21�00345�21�00331�OPBALAN�SALDO OPERATIVO     �S�015�2�S�        �
 * BGMFPXB �CONSULTA MOVTOS. API PAG      �X�03�00024�01�00001�PAGKEY �LLAVE DE PAGINACION �N�008�0�S�        �
 * BGMFPXB �CONSULTA MOVTOS. API PAG      �X�03�00024�02�00009�MOREDAT�INDICADOR MAS DATOS �A�001�0�S�        �
 * BGMFPXB �CONSULTA MOVTOS. API PAG      �X�03�00024�03�00010�PAGSDO �SALDO DE PAGINACION �S�015�2�S�        �
 * 
 * FICHERO: BGPX-FDX-v3.TXT
 * BGPXBGMFPXB BGMFPXB BG2CGPX01S                             CICSDM112017-06-02-15.47.58.410373CICSDM112017-06-02-15.47.58.410404
 * BGPXBGMFPXA BGMFPXA BG2CGPX01S                             CICSDM112017-06-02-15.47.27.803302CICSDM112017-06-02-15.47.27.803329
</pre></code>
 * 
 * @see RespuestaTransaccionBgpx
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "BGPX", tipo = 1, subtipo = 1, version = 1, configuracion = "default_ps9_mx",
        respuesta = RespuestaTransaccionBgpx.class,
        atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")})
@Multiformato(formatos = {FormatoBGMFPX.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionBgpx implements MensajeMultiparte {

    /**
     * <p>
     * Cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Cuerpo
    private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

    /**
     * <p>
     * Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Override
    public CuerpoMultiparte getCuerpo() {
        return cuerpo;
    }

}
