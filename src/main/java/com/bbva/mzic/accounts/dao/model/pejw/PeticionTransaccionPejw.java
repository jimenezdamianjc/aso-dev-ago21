package com.bbva.mzic.accounts.dao.model.pejw;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

/**
 * <p>
 * Transacci&oacute;n <code>PEJW</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionPejw</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionPejw</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: PEJW.QGDTCCT.txt
 * PEJWCONSULTA PRODUCTOS ASOCIADOS A CUENPE        PE2C00JW     01 PEM0JWE             PEJR  NN3000CNNNNN    SSTN ABMC   NNNNNNNN  NN                2019-04-16MB75889 2019-04-1609.11.30MB75889 2019-04-16-08.32.56.327474MB75889 0001-01-010001-01-01
 * FICHERO: PEJW.QGDTFDF.txt
 * PEM0JWE �ENT CONSULTA DE CUENTAS       �F�02�00028�01�00001�NUMCLIE�NUMERO CLIENTE      �A�008�0�O�        �
 * PEM0JWE �ENT CONSULTA DE CUENTAS       �F�02�00028�02�00009�CUENTA �CUENTA A VALIDAR    �A�020�0�O�        �
 * PEM0JWS �SALIDA CANCELACION CUENTAS    �X�05�00140�01�00001�CONTRAC�CONTRATO            �A�020�0�S�        �
 * PEM0JWS �SALIDA CANCELACION CUENTAS    �X�05�00140�02�00021�PRODUID�PRODUCTO ID         �A�030�0�S�        �
 * PEM0JWS �SALIDA CANCELACION CUENTAS    �X�05�00140�03�00051�PRODUNA�PRODUCTO NOMBRE     �A�030�0�S�        �
 * PEM0JWS �SALIDA CANCELACION CUENTAS    �X�05�00140�04�00081�STATUSI�ESTATUS ID          �A�030�0�S�        �
 * PEM0JWS �SALIDA CANCELACION CUENTAS    �X�05�00140�05�00111�STATUSN�ESTATUS NOMBRE      �A�030�0�S�        �
 * FICHERO: PEJW.QGDTFDX.txt
 * PEJWPEM0JWS PENC0JWSPE2C00JW1S0140S030                     MB75889 2019-04-16-08.41.42.970203MB75889 2019-04-16-08.41.42.970234
</pre></code>
 * 
 * @see RespuestaTransaccionPejw
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "PEJW", tipo = 1, subtipo = 1, version = 1, configuracion = "default_ps9_mx",
        respuesta = RespuestaTransaccionPejw.class,
        atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")})
@Multiformato(formatos = {FormatoPEM0JWE.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionPejw implements MensajeMultiparte {

    /**
     * <p>
     * Cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Cuerpo
    private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

    /**
     * <p>
     * Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Override
    public CuerpoMultiparte getCuerpo() {
        return cuerpo;
    }

}
