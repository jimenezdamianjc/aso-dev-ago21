package com.bbva.mzic.accounts.dao.model.bg6c;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>BG6C</code>
 * 
 * @see PeticionTransaccionBg6c
 * @see RespuestaTransaccionBg6c
 */
@Component
public class TransaccionBg6c implements InvocadorTransaccion<PeticionTransaccionBg6c, RespuestaTransaccionBg6c> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionBg6c invocar(PeticionTransaccionBg6c transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionBg6c.class, RespuestaTransaccionBg6c.class, transaccion);
	}

	@Override
	public RespuestaTransaccionBg6c invocarCache(PeticionTransaccionBg6c transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionBg6c.class, RespuestaTransaccionBg6c.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionBg6c.class, "vaciearCache");
	}
}
