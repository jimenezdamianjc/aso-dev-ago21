package com.bbva.mzic.accounts.dao.model.host.feds;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>FEDS</code>
 * 
 * @see PeticionTransaccionFeds
 * @see RespuestaTransaccionFeds
 */
@Component
public class TransaccionFeds implements InvocadorTransaccion<PeticionTransaccionFeds, RespuestaTransaccionFeds> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionFeds invocar(PeticionTransaccionFeds transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionFeds.class, RespuestaTransaccionFeds.class, transaccion);
	}

	@Override
	public RespuestaTransaccionFeds invocarCache(PeticionTransaccionFeds transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionFeds.class, RespuestaTransaccionFeds.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionFeds.class, "vaciearCache");
	}
}
