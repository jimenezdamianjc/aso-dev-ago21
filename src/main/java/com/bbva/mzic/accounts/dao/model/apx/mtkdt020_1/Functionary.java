package com.bbva.mzic.accounts.dao.model.apx.mtkdt020_1;

import java.util.List;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Bean fila para el campo tabular <code>functionary</code>, utilizado por la clase
 * <code>Account</code>
 * </p>
 * 
 * @see Account
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Functionary {

    /**
     * <p>
     * Campo <code>id</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "id", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 8, signo = true)
    private String id;

    /**
     * <p>
     * Campo <code>crManager</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "crManager", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true)
    private String crmanager;

    /**
     * <p>
     * Campo <code>crName</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "crName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 50, signo = true)
    private String crname;

    /**
     * <p>
     * Campo <code>bank</code>, &iacute;ndice: <code>4</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 4, nombre = "bank", tipo = TipoCampo.TABULAR)
    private List<Bank> bank;

}
