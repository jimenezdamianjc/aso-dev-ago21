package com.bbva.mzic.accounts.dao.model.bgl1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>BGL1</code>
 * 
 * @see PeticionTransaccionBgl1
 * @see RespuestaTransaccionBgl1
 */
@Component("transaccionBgl1-v0")
public class TransaccionBgl1 implements InvocadorTransaccion<PeticionTransaccionBgl1, RespuestaTransaccionBgl1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionBgl1 invocar(PeticionTransaccionBgl1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionBgl1.class, RespuestaTransaccionBgl1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionBgl1 invocarCache(PeticionTransaccionBgl1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionBgl1.class, RespuestaTransaccionBgl1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionBgl1.class, "vaciearCache");
	}
}
