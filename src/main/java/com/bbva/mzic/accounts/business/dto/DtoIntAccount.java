package com.bbva.mzic.accounts.business.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.jee.arq.spring.core.servicing.annotations.CasContract;
import com.bbva.jee.arq.spring.core.servicing.annotations.SecurityFunction;
import com.bbva.jee.arq.spring.core.servicing.utils.ShortDateAdapter;
import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "accountResponse", namespace = "urn:com:bbva:zic:accounts:facade:v0:dto")
@XmlType(name = "account", namespace = "urn:com:bbva:zic:accounts:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoIntAccount {

	/* ***** implementar propiedades, getters y setters faltantes***** */
	@ApiModelProperty(value = "Account identifier")
	@SecurityFunction(outFunction = "cypher")
	private String accountId;

	@ApiModelProperty(value = "Account number. This number consists in an alpha-numerical sequence to identify an account.")
	@DatoAuditable(omitir = true)
	@CasContract
	private String number;

	@ApiModelProperty(value = "Type of financial product")
	private DtoIntAccountType accountType;

	@ApiModelProperty(value = "User-customizable alias assigned to the account to name the account the way the user likes")
	private String alias;

	@ApiModelProperty(value = "String based on ISO-8601 date format for specifying the account cancellation date")
	@XmlJavaTypeAdapter(value = ShortDateAdapter.class)
	private Date cancellationDate;

	@ApiModelProperty(value = "String based on ISO-8601 date format for specifying the account last modification date.")
	@XmlJavaTypeAdapter(value = ShortDateAdapter.class)
	private Date lastModificationDate;

	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * @return the accountType
	 */
	public DtoIntAccountType getAccountType() {
		return accountType;
	}

	/**
	 * @param accountType the accountType to set
	 */
	public void setAccountType(DtoIntAccountType accountType) {
		this.accountType = accountType;
	}

	/**
	 * @return the alias
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * @param alias the alias to set
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}

	/**
	 * @return the cancellationDate
	 */
	public Date getCancellationDate() {
		return cancellationDate;
	}

	/**
	 * @param cancellationDate the cancellationDate to set
	 */
	public void setCancellationDate(Date cancellationDate) {
		this.cancellationDate = cancellationDate;
	}

	/**
	 * @return the lastModificationDate
	 */
	public Date getLastModificationDate() {
		return lastModificationDate;
	}

	/**
	 * @param lastModificationDate the lastModificationDate to set
	 */
	public void setLastModificationDate(Date lastModificationDate) {
		this.lastModificationDate = lastModificationDate;
	}

}
