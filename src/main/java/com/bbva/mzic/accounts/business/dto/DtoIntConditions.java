package com.bbva.mzic.accounts.business.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.bbva.mzic.accounts.rm.enums.ConditionEnum;

@XmlRootElement(name = "conditionsResponse", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlType(name = "conditions", namespace = "urn:com:bbva:zic:accounts:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoIntConditions implements Serializable {

	private static final long serialVersionUID = 1L;

	private ConditionEnum conditionId;

	private String name;

	public ConditionEnum getConditionId() {
		return conditionId;
	}

	public void setConditionId(ConditionEnum conditionId) {
		this.conditionId = conditionId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
