package com.bbva.mzic.accounts.business.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.mzic.accounts.business.ISrvIntAccountsV0;
import com.bbva.mzic.accounts.business.dto.DtoIntAccount;
import com.bbva.mzic.accounts.business.dto.DtoIntAggregatedAvailableBalance;
import com.bbva.mzic.accounts.business.dto.DtoIntConditions;
import com.bbva.mzic.accounts.business.dto.DtoIntFilterAccount;
import com.bbva.mzic.accounts.dao.IAccountsDaoV0;

@Service
public class SrvIntAccountsV0 implements ISrvIntAccountsV0 {

	@Autowired
	private IAccountsDaoV0 accountsDaoV0;

	@Override
	public DtoIntAccount getAccount(DtoIntFilterAccount filterAccount) {

		if (filterAccount == null) {
			return null;
		}

		return new DtoIntAccount();
	}

	@Override
	public DtoIntAggregatedAvailableBalance getBalance(DtoIntFilterAccount filterAccount) {

		if (filterAccount == null) {
			return null;
		}

		return new DtoIntAggregatedAvailableBalance();
	}

	@Override
	public List<DtoIntConditions> listConditions(DtoIntFilterAccount filterAccount) {
		if (filterAccount == null) {
			return null;
		}

		return accountsDaoV0.getListConditionsBGL5(filterAccount);
	}

}
