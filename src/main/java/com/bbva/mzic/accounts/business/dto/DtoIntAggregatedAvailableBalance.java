package com.bbva.mzic.accounts.business.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "aggregatedAvailableBalance", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlType(name = "aggregatedAvailableBalance", namespace = "urn:com:bbva:zic:accounts:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoIntAggregatedAvailableBalance implements Serializable {

	private static final long serialVersionUID = 1L;

	private DtoIntAvailableBalance aggregatedAvailableBalance;

	public DtoIntAvailableBalance getAggregatedAvailableBalance() {
		return aggregatedAvailableBalance;
	}

	public void setAggregatedAvailableBalance(DtoIntAvailableBalance aggregatedAvailableBalance) {
		this.aggregatedAvailableBalance = aggregatedAvailableBalance;
	}

}
