package com.bbva.mzic.accounts.business.dto;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "availableBalanceResponse", namespace = "urn:com:bbva:mzic:accounts:business:dto")
@XmlType(name = "availableBalance", namespace = "urn:com:bbva:zic:accounts:business:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoIntAvailableBalance implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "This balance means the maximum expendable amount at the moment of retrieval")
	private List<DtoIntBalance> currentBalances;

	@ApiModelProperty(value = "This balance is the maximum expendable amount calculated at the end of the last business day")
	private List<DtoIntBalance> postedBalances;

	@ApiModelProperty(value = "This balance is the aggregated amount of all pending transactions")
	private List<DtoIntBalance> pendingBalances;

	public List<DtoIntBalance> getCurrentBalances() {
		return currentBalances;
	}

	public void setCurrentBalances(List<DtoIntBalance> currentBalances) {
		this.currentBalances = currentBalances;
	}

	public List<DtoIntBalance> getPostedBalances() {
		return postedBalances;
	}

	public void setPostedBalances(List<DtoIntBalance> postedBalances) {
		this.postedBalances = postedBalances;
	}

	public List<DtoIntBalance> getPendingBalances() {
		return pendingBalances;
	}

	public void setPendingBalances(List<DtoIntBalance> pendingBalances) {
		this.pendingBalances = pendingBalances;
	}

}
