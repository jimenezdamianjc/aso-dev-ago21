package com.bbva.mzic.accounts.business;

import java.util.List;

import com.bbva.mzic.accounts.business.dto.DtoIntAccount;
import com.bbva.mzic.accounts.business.dto.DtoIntAggregatedAvailableBalance;
import com.bbva.mzic.accounts.business.dto.DtoIntConditions;
import com.bbva.mzic.accounts.business.dto.DtoIntFilterAccount;

public interface ISrvIntAccountsV0 {
	DtoIntAccount getAccount(DtoIntFilterAccount filterAccount);

	DtoIntAggregatedAvailableBalance getBalance(DtoIntFilterAccount filterAccount);

	List<DtoIntConditions> listConditions(DtoIntFilterAccount filterAccount);

}
