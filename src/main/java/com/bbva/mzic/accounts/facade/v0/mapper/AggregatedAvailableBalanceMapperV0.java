package com.bbva.mzic.accounts.facade.v0.mapper;

import org.mapstruct.Mapper;

import com.bbva.mzic.accounts.business.dto.DtoIntAggregatedAvailableBalance;
import com.bbva.mzic.accounts.facade.v0.dto.DtoAggregatedAvailableBalance;
import com.bbva.mzic.accounts.rm.Constants;

@Mapper(componentModel = "spring", implementationPackage = Constants.IMPL_PACKAGE_MAPPER, uses = AvailableBalanceMapperV0.class)
public interface AggregatedAvailableBalanceMapperV0 {

	DtoIntAggregatedAvailableBalance mapToInner(DtoAggregatedAvailableBalance from);

	DtoAggregatedAvailableBalance mapToOuter(DtoIntAggregatedAvailableBalance from);

}
