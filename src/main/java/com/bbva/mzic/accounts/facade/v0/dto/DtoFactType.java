package com.bbva.mzic.accounts.facade.v0.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.bbva.mzic.accounts.rm.enums.FactTypeEnum;
import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "factType", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlType(name = "factType", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoFactType implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Fact type identification.")
	private FactTypeEnum id;

	@ApiModelProperty(value = "Fact type name or description.")
	private String name;

	public FactTypeEnum getId() {
		return id;
	}

	public void setId(FactTypeEnum id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
