package com.bbva.mzic.accounts.facade.v0.mapper;

import org.mapstruct.Mapper;

import com.bbva.mzic.accounts.business.dto.DtoIntConditions;
import com.bbva.mzic.accounts.facade.v0.dto.DtoConditions;
import com.bbva.mzic.accounts.rm.Constants;

@Mapper(componentModel = "spring", implementationPackage = Constants.IMPL_PACKAGE_MAPPER)
public interface ConditionsMapperV0 {

	DtoIntConditions mapToInner(DtoConditions in);

	DtoConditions mapToOuter(DtoIntConditions out);

}
