package com.bbva.mzic.accounts.facade.v0.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "conditionAmount", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlType(name = "conditionAmount", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoConditionAmount implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Fact amount value.")
	// @XmlJavaTypeAdapter(BigDecimalFlatAdapter.class)
	private BigDecimal amount;

	@ApiModelProperty(value = "String based on ISO-4217 for specifying the Fact amount amount.")
	private String currency;

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

}
