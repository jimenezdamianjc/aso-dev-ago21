package com.bbva.mzic.accounts.facade.v0.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "facts", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlType(name = "facts", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoFacts implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Fact type.")
	private DtoFactType factType;

	@ApiModelProperty(value = "Fact amount.")
	private DtoConditionAmount conditionAmount;

	@ApiModelProperty(value = "Property or value over which Fact value applies.")
	private DtoApply apply;

	public DtoFactType getFactType() {
		return factType;
	}

	public void setFactType(DtoFactType factType) {
		this.factType = factType;
	}

	public DtoConditionAmount getConditionAmount() {
		return conditionAmount;
	}

	public void setConditionAmount(DtoConditionAmount conditionAmount) {
		this.conditionAmount = conditionAmount;
	}

	public DtoApply getApply() {
		return apply;
	}

	public void setApply(DtoApply apply) {
		this.apply = apply;
	}

}
