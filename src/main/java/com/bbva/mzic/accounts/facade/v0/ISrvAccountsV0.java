package com.bbva.mzic.accounts.facade.v0;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseInterface;

public interface ISrvAccountsV0 {
	ServiceResponseInterface getAccounts(String accountId, String expands, String fields);

//	DtoAggregatedAvailableBalance getBalance(String accountFamilyId);
	ServiceResponseInterface getBalance(String accountFamilyId);

	ServiceResponseInterface listSavingsGoals(String consumerId);

	ServiceResponseInterface listConditions(String accountId);
}
