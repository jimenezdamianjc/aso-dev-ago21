package com.bbva.mzic.accounts.facade.v0.dto;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.bbva.mzic.accounts.rm.enums.ConditionEnum;
import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "conditionsResponse", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlType(name = "conditions", namespace = "urn:com:bbva:zic:accounts:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoConditions implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Condition identifier.")
	private ConditionEnum conditionId;

	@ApiModelProperty(value = "Condition identifier.")
	private String name;

	@ApiModelProperty(value = "Condition periodical information. It includes the date when the condition will be evaluated.")
	private DtoPeriod period;

	@ApiModelProperty(value = "Facts that must be met to apply the condition. All the facts will need to be fulfilled in order to apply this condition.")
	private List<DtoFacts> facts;

	@ApiModelProperty(value = "Condition identifier.")
	private List<DtoOutcomes> outcomes;

	public ConditionEnum getConditionId() {
		return conditionId;
	}

	public void setConditionId(ConditionEnum conditionId) {
		this.conditionId = conditionId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public DtoPeriod getPeriod() {
		return period;
	}

	public void setPeriod(DtoPeriod period) {
		this.period = period;
	}

	public DtoFacts getFacts() {
		return facts;
	}

	public void setFacts(DtoFacts facts) {
		this.facts = facts;
	}

	public DtoOutcomes getOutcomes() {
		return outcomes;
	}

	public void setOutcomes(DtoOutcomes outcomes) {
		this.outcomes = outcomes;
	}

}
