package com.bbva.mzic.accounts.facade.v0.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "outcomes", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlType(name = "outcomes", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoOutcomes implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Outcome percental value.")
	// @XmlJavaTypeAdapter(BigDecimalFlatAdapter.class)
	private BigDecimal feePercentage;

	@ApiModelProperty(value = "String based on ISO-8601 date format for specifying the next date when the Outcome will be generated.")
	// @XmlJavaTypeAdapter(ShortDateAdapter.class)
	private Date dueDate;

	@ApiModelProperty(value = "Outcome type.")
	private DtoOutcomeType outcomeType;

	@ApiModelProperty(value = "Outcome amount")
	private DtoFeeAmount feeAmount;

	@ApiModelProperty(value = "Property or value over which Outcome fee applies.")
	private DtoApply apply;

	public BigDecimal getFeePercentage() {
		return feePercentage;
	}

	public void setFeePercentage(BigDecimal feePercentage) {
		this.feePercentage = feePercentage;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public DtoOutcomeType getOutcomeType() {
		return outcomeType;
	}

	public void setOutcomeType(DtoOutcomeType outcomeType) {
		this.outcomeType = outcomeType;
	}

	public DtoFeeAmount getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(DtoFeeAmount feeAmount) {
		this.feeAmount = feeAmount;
	}

	public DtoApply getApply() {
		return apply;
	}

	public void setApply(DtoApply apply) {
		this.apply = apply;
	}

}
