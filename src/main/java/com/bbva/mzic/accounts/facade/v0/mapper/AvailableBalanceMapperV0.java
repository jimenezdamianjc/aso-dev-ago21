package com.bbva.mzic.accounts.facade.v0.mapper;

import org.mapstruct.Mapper;

import com.bbva.mzic.accounts.business.dto.DtoIntAvailableBalance;
import com.bbva.mzic.accounts.facade.v0.dto.DtoAvailableBalance;
import com.bbva.mzic.accounts.rm.Constants;

@Mapper(componentModel = "spring", implementationPackage = Constants.IMPL_PACKAGE_MAPPER, uses = BalanceMapperV0.class)
public interface AvailableBalanceMapperV0 {

	DtoIntAvailableBalance mapToInner(DtoAvailableBalance from);

	DtoAvailableBalance mapToOuter(DtoIntAvailableBalance from);

}
