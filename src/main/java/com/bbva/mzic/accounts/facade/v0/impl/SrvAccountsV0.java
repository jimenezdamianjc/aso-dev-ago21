package com.bbva.mzic.accounts.facade.v0.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseInterface;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseNoContent.ServiceResponseNoContentBuilder;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseOK;
import com.bbva.jee.arq.spring.core.servicing.annotations.CasContract;
import com.bbva.jee.arq.spring.core.servicing.annotations.CasIgnored;
import com.bbva.jee.arq.spring.core.servicing.annotations.SMC;
import com.bbva.jee.arq.spring.core.servicing.annotations.SN;
import com.bbva.jee.arq.spring.core.servicing.annotations.SecurityFunction;
import com.bbva.jee.arq.spring.core.servicing.annotations.VN;
import com.bbva.mzic.accounts.business.ISrvIntAccountsV0;
import com.bbva.mzic.accounts.business.dto.DtoIntConditions;
import com.bbva.mzic.accounts.business.dto.DtoIntFilterAccount;
import com.bbva.mzic.accounts.facade.v0.ISrvAccountsV0;
import com.bbva.mzic.accounts.facade.v0.dto.DtoAccount;
import com.bbva.mzic.accounts.facade.v0.dto.DtoAggregatedAvailableBalance;
import com.bbva.mzic.accounts.facade.v0.dto.DtoConditions;
import com.bbva.mzic.accounts.facade.v0.mapper.AccountMapperV0;
import com.bbva.mzic.accounts.facade.v0.mapper.AggregatedAvailableBalanceMapperV0;
import com.bbva.mzic.accounts.facade.v0.mapper.ConditionsMapperV0;
import com.bbva.mzic.accounts.facade.v0.validators.ValidatorAccountsV0;
import com.bbva.mzic.serviceutils.rm.utils.ids.IdAccountUtils;
import com.bbva.mzic.serviceutils.rm.utils.web.ContextUtils;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Path("/v0")
@SN(registryID = "SNMX1510047", logicalID = "accounts")
@VN(vnn = "v0")
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@Service("srvAccountsV0")
@Api(value = "/accounts/v0", description = "SN Accounts")
public class SrvAccountsV0 implements ISrvAccountsV0 {

	private static final String ACCOUNT_ID = "accountId";

	@Autowired
	private AggregatedAvailableBalanceMapperV0 aggregatedAvailableBalanceMapperV0;

	@Autowired
	private ContextUtils contextUtils;

	@Autowired
	private AccountMapperV0 accountMapperV0;

	@Autowired
	private ISrvIntAccountsV0 iSrvIntAccountsV0;

	@Autowired
	private IdAccountUtils idAccountUtils;

	@Autowired
	ConditionsMapperV0 conditionsMapperV0;

	private static final String GABI_CATALOG = "gabiCatalog";
	private static final String ASO_CATALOG = "asoCatalog";

	@Override
	@Path("/accounts/{accountId}")
	@GET
	@SMC(registryID = "SMCMX1610002", logicalID = "getAccount", forcedCatalog = GABI_CATALOG)
	@ApiOperation(value = "Recuperación de la información detallada de una cuenta específica.", notes = "Recuperación de la información detallada de una cuenta específica.", response = DtoAccount.class, nickname = "getAccount", httpMethod = HttpMethod.GET, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ServiceResponseInterface getAccounts(
			@CasContract(mask = IdAccountUtils.ACCOUNT_MASK) @SecurityFunction(outFunction = "cypher") @ApiParam(value = ACCOUNT_ID) @PathParam(ACCOUNT_ID) String accountId,
			@QueryParam("expands") String expands, @QueryParam("fields") String fields) {

		// Validar los obligatorios
		ValidatorAccountsV0.getAccount(accountId);

		final DtoIntFilterAccount filterAccount = new DtoIntFilterAccount();
		filterAccount.setAccountId(accountId);
		filterAccount.setExpands(expands);

		// Extracción de la cuenta
		filterAccount.setAccountNumber(idAccountUtils.getAccountNumber(accountId));
		filterAccount.setAccountType(idAccountUtils.getAccountType(accountId));

		// Extraer del contexto el cliente a 8 posiciones
		filterAccount.setClientId(contextUtils.getBackendProperties().getClientId());

		DtoAccount dtoResponse = new DtoAccount();

		// Llamar a business
		dtoResponse = accountMapperV0.mapToOuter(iSrvIntAccountsV0.getAccount(filterAccount));

		// Filtrar expands no necesarios
		// TODO: Implementar: filterExpands(expands, dtoResponse);

		return ServiceResponseOK.data(dtoResponse).build();
	}

	@Override
	@Path("/balances")
	@GET
	@CasIgnored
	@SMC(forcedCatalog = ASO_CATALOG, registryID = "SMCMX", logicalID = "getBalance")
	@ApiOperation(value = "Consultar el saldo de las cuentas asociadas a un usuario", notes = "Consultar el saldo de las cuentas asociadas a un usuario", response = DtoAggregatedAvailableBalance.class, nickname = "getBalance", httpMethod = "GET", consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public ServiceResponseInterface getBalance(String accountFamilyId) {
		ValidatorAccountsV0.validaAccountFamily(accountFamilyId);
		final DtoIntFilterAccount filterAccount = new DtoIntFilterAccount();
		filterAccount.setAccountFamilyId(accountFamilyId);
		DtoAggregatedAvailableBalance dtoResponse = new DtoAggregatedAvailableBalance();
		dtoResponse = aggregatedAvailableBalanceMapperV0.mapToOuter(iSrvIntAccountsV0.getBalance(filterAccount));
		return ServiceResponseOK.data(dtoResponse).build();
	}

	@Override
	@Path("/accounts/saving-goals")
	@GET
	@CasIgnored
	@SMC(forcedCatalog = ASO_CATALOG, registryID = "SMCMX", logicalID = "listSavingGoals")
	@ApiOperation(value = "Consultar ...", notes = "Consultar ...", response = List.class, nickname = "listSavingGoals", httpMethod = "GET", consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public ServiceResponseInterface listSavingsGoals(@QueryParam(value = "customer-id") String consumerId) {

		if (StringUtils.isBlank(consumerId)) {
			// return ServiceResponse.data(list).build();
			return ServiceResponseNoContentBuilder.build();

		}
		return ServiceResponseOK.data(consumerId).build();

	}

	@Override
	@Path("/accounts/{accountId}")
	@GET
	@SMC(registryID = "SMCMX1610003", logicalID = "listConditions", forcedCatalog = GABI_CATALOG)
	@ApiOperation(value = "Consultar las condiciones (tasas y otros pagos relacionados con el mantenimiento de la cuenta)", notes = "Consultar las condiciones (tasas y otros pagos relacionados con el mantenimiento de la cuenta)", response = List.class, nickname = "listConditions", httpMethod = HttpMethod.GET, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ServiceResponseInterface listConditions(
			@CasContract(mask = IdAccountUtils.ACCOUNT_MASK) @SecurityFunction(outFunction = "cypher") @ApiParam(value = ACCOUNT_ID) @PathParam(ACCOUNT_ID) String accountId) {

		ValidatorAccountsV0.getAccount(accountId);

		final DtoIntFilterAccount filterAccount = new DtoIntFilterAccount();
		filterAccount.setAccountId(accountId);

		final List<DtoConditions> response = new ArrayList<>();

		final List<DtoIntConditions> listDtoIntConditions = iSrvIntAccountsV0.listConditions(filterAccount);

		for (final DtoIntConditions dtoIntConditions : listDtoIntConditions) {
			DtoConditions dtoConditions = new DtoConditions();
			dtoConditions = conditionsMapperV0.mapToOuter(dtoIntConditions);
			response.add(dtoConditions);
		}

		return ServiceResponseOK.data(response).build();
	}

}
