package com.bbva.mzic.accounts.facade.v0.dto;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.bbva.mzic.accounts.rm.enums.PeriodEnum;
import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "period", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlType(name = "period", namespace = "urn:com:bbva:zic:accounts:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoPeriod implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Fact period identification.")
	private PeriodEnum id;

	@ApiModelProperty(value = "Fact period name or description.")
	private String name;

	@ApiModelProperty(value = "String based on ISO-8601 date format for specifying the next date when the Facts will be evaluated.")
	// @XmlJavaTypeAdapter(DateAdapter.class)
	private Date checkDate;

}
