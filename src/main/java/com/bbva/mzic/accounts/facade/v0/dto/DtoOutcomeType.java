package com.bbva.mzic.accounts.facade.v0.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.bbva.mzic.accounts.rm.enums.OutcomeTypeEnum;
import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "outcomeType", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlType(name = "outcomeType", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoOutcomeType implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Outcome type identification.")
	private OutcomeTypeEnum id;

	@ApiModelProperty(value = "Outcome type name or description.")
	private String name;

	public OutcomeTypeEnum getId() {
		return id;
	}

	public void setId(OutcomeTypeEnum id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
