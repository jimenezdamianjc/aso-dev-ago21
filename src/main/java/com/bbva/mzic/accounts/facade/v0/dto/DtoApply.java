package com.bbva.mzic.accounts.facade.v0.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.bbva.mzic.accounts.rm.enums.ApplyEnum;
import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "apply", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlType(name = "apply", namespace = "urn:com:bbva:mzic:accounts:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoApply implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Property identification.")
	private ApplyEnum id;

	@ApiModelProperty(value = "Property name or description.")
	private String name;

	public ApplyEnum getId() {
		return id;
	}

	public void setId(ApplyEnum id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
