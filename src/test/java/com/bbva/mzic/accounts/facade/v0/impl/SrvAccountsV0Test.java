package com.bbva.mzic.accounts.facade.v0.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseInterface;
import com.bbva.jee.arq.spring.core.managers.OutputHeaderManager;
import com.bbva.jee.arq.spring.core.servicing.utils.toolkit.gabi.GabiServiceToolkit;
import com.bbva.mzic.accounts.business.ISrvIntAccountsV0;
import com.bbva.mzic.accounts.business.dto.DtoIntAccount;
import com.bbva.mzic.accounts.business.dto.DtoIntAggregatedAvailableBalance;
import com.bbva.mzic.accounts.business.dto.DtoIntFilterAccount;
import com.bbva.mzic.accounts.facade.v0.FacadeTestFactory;
import com.bbva.mzic.accounts.facade.v0.dto.DtoAccount;
import com.bbva.mzic.accounts.facade.v0.dto.DtoAggregatedAvailableBalance;
import com.bbva.mzic.accounts.facade.v0.mapper.AccountMapperV0;
import com.bbva.mzic.accounts.facade.v0.mapper.AggregatedAvailableBalanceMapperV0;
import com.bbva.mzic.serviceutils.rm.utils.encrypter.SecurityFunctionUtils;
import com.bbva.mzic.serviceutils.rm.utils.ids.IdAccountUtils;
import com.bbva.mzic.serviceutils.rm.utils.web.ContextUtils;
import com.bbva.mzic.serviceutils.rm.utils.web.dto.BackendProperties;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class SrvAccountsV0Test {

	private static final String ACCOUNT_NUMBER = "1234567890123456";
	private static final String CLIENT_ID = "12345678";
	private static final String FIELD_ACCOUNT_ID = "accountId";
	private static final String ACCOUNT_TYPE = "ABCD";
	private static final String ACCOUNT_ID = "1234567890123456";
	private static final String ACCOUNT_FAMILY_ID = "987654321";
	@InjectMocks
	private SrvAccountsV0 srvAccountsV0;

	@Mock
	private IdAccountUtils idAccountUtils;

	@Mock
	private ContextUtils contextUtils;

	@Mock
	private ISrvIntAccountsV0 iSrvIntAccountsV0;

	@Mock
	private OutputHeaderManager outputHeaderManager;

	@Mock
	private SecurityFunctionUtils securityFunctionUtils;

	@Mock
	private AccountMapperV0 accountMapperV0;

	@Mock
	private GabiServiceToolkit gabiServiceTK;

	@Mock
	private BackendProperties bep;

	@Mock
	private AggregatedAvailableBalanceMapperV0 aggregatedAvailableBalanceMapperV0;

	private final FacadeTestFactory facadeFactory = new FacadeTestFactory();

	@Before
	public void prepareTest() {
		Mockito.when(idAccountUtils.getAccountNumber(Mockito.anyString())).thenReturn(ACCOUNT_NUMBER);
		Mockito.when(idAccountUtils.getAccountType(Mockito.anyString())).thenReturn(ACCOUNT_TYPE);

		Mockito.when(bep.getClientId()).thenReturn(CLIENT_ID);
		// Mockito.when(bep.getHostChannel()).thenReturn(CANAL_ID);
		// Mockito.when(bep.getClientNumber()).thenReturn(CUSTOMER_CONTEXT);
		Mockito.when(contextUtils.getBackendProperties()).thenReturn(bep);
	}

	@Test
	public void testGetAccountNoExpandsNoFields200() {
		// Arrange
		final DtoIntAccount dtoIntAccount = facadeFactory.getDtoIntAccountDummy();
		final DtoAccount dtoAccount = facadeFactory.getDtoAccountDummy();

		Mockito.when(iSrvIntAccountsV0.getAccount(Mockito.any(DtoIntFilterAccount.class))).thenReturn(dtoIntAccount);
		Mockito.when(accountMapperV0.mapToOuter(dtoIntAccount)).thenReturn(dtoAccount);
		// Act
		final ServiceResponseInterface response = srvAccountsV0.getAccounts(ACCOUNT_ID, null, null);

		// Assert
		Mockito.verify(iSrvIntAccountsV0).getAccount(Mockito.any(DtoIntFilterAccount.class));
		Mockito.verify(accountMapperV0).mapToOuter(dtoIntAccount);

		Assert.assertNotNull(response);
	}

	@Test
	public void testGetAccountNoExpandsNoFields204() {
		// Arrange
		final DtoIntAccount dtoIntAccount = facadeFactory.getDtoIntAccountDummy();
		Mockito.when(iSrvIntAccountsV0.getAccount(Mockito.any(DtoIntFilterAccount.class))).thenReturn(dtoIntAccount);
		Mockito.when(accountMapperV0.mapToOuter(dtoIntAccount)).thenReturn(null);
		// Act
		final ServiceResponseInterface response = srvAccountsV0.getAccounts(ACCOUNT_ID, null, null);

		// Assert
		Mockito.verify(iSrvIntAccountsV0).getAccount(Mockito.any(DtoIntFilterAccount.class));
		Mockito.verify(accountMapperV0).mapToOuter(dtoIntAccount);

		Assert.assertNotNull(response);
	}

	@Test
	public void testGetAccountBlocksExpandsAnyFields200() {
		// Arrange
		final DtoIntAccount dtoIntAccount = facadeFactory.getDtoIntAccountDummy();
		final DtoAccount dtoAccount = facadeFactory.getDtoAccountDummy();
		Mockito.when(iSrvIntAccountsV0.getAccount(Mockito.any(DtoIntFilterAccount.class))).thenReturn(dtoIntAccount);
		Mockito.when(accountMapperV0.mapToOuter(dtoIntAccount)).thenReturn(dtoAccount);
		// Act
		final ServiceResponseInterface response = srvAccountsV0.getAccounts(ACCOUNT_ID, "blocks", FIELD_ACCOUNT_ID);

		// Assert
		Mockito.verify(iSrvIntAccountsV0).getAccount(Mockito.any(DtoIntFilterAccount.class));
		Mockito.verify(accountMapperV0).mapToOuter(dtoIntAccount);

		Assert.assertNotNull(response);
	}

	@Test
	public void testGetAccountBlocksExpandsNoFields200() {
		// Arrange
		final DtoIntAccount dtoIntAccount = facadeFactory.getDtoIntAccountDummy();
		final DtoAccount dtoAccount = facadeFactory.getDtoAccountDummy();
		Mockito.when(iSrvIntAccountsV0.getAccount(Mockito.any(DtoIntFilterAccount.class))).thenReturn(dtoIntAccount);
		Mockito.when(accountMapperV0.mapToOuter(dtoIntAccount)).thenReturn(dtoAccount);
		// Act
		final ServiceResponseInterface response = srvAccountsV0.getAccounts(ACCOUNT_ID, "blocks", null);

		// Assert
		Mockito.verify(iSrvIntAccountsV0).getAccount(Mockito.any(DtoIntFilterAccount.class));
		Mockito.verify(accountMapperV0).mapToOuter(dtoIntAccount);

		Assert.assertNotNull(response);
	}

	@Test
	public void testGetAccountHoldsExpandsNoFields200() {
		// Arrange
		final DtoIntAccount dtoIntAccount = facadeFactory.getDtoIntAccountDummy();
		final DtoAccount dtoAccount = facadeFactory.getDtoAccountDummy();
		Mockito.when(iSrvIntAccountsV0.getAccount(Mockito.any(DtoIntFilterAccount.class))).thenReturn(dtoIntAccount);
		Mockito.when(accountMapperV0.mapToOuter(dtoIntAccount)).thenReturn(dtoAccount);
		// Act
		final ServiceResponseInterface response = srvAccountsV0.getAccounts(ACCOUNT_ID, "customized-format", null);

		// Assert
		Mockito.verify(iSrvIntAccountsV0).getAccount(Mockito.any(DtoIntFilterAccount.class));
		Mockito.verify(accountMapperV0).mapToOuter(dtoIntAccount);

		Assert.assertNotNull(response);
	}

	@Test
	public void testGetAccountCustomizedFormatsExpandsNoFields200() {
		// Arrange
		final DtoIntAccount dtoIntAccount = facadeFactory.getDtoIntAccountDummy();
		final DtoAccount dtoAccount = facadeFactory.getDtoAccountDummy();
		Mockito.when(iSrvIntAccountsV0.getAccount(Mockito.any(DtoIntFilterAccount.class))).thenReturn(dtoIntAccount);
		Mockito.when(accountMapperV0.mapToOuter(dtoIntAccount)).thenReturn(dtoAccount);
		// Act
		final ServiceResponseInterface response = srvAccountsV0.getAccounts(ACCOUNT_ID, "customized-format", null);

		// Assert
		Mockito.verify(iSrvIntAccountsV0).getAccount(Mockito.any(DtoIntFilterAccount.class));
		Mockito.verify(accountMapperV0).mapToOuter(dtoIntAccount);

		Assert.assertNotNull(response);
	}

	@Test
	public void testGetAccountParticipantsExpandsNoFields200() {
		// Arrange
		final DtoIntAccount dtoIntAccount = facadeFactory.getDtoIntAccountDummy();
		final DtoAccount dtoAccount = facadeFactory.getDtoAccountDummy();
		Mockito.when(iSrvIntAccountsV0.getAccount(Mockito.any(DtoIntFilterAccount.class))).thenReturn(dtoIntAccount);
		Mockito.when(accountMapperV0.mapToOuter(dtoIntAccount)).thenReturn(dtoAccount);
		// Act
		final ServiceResponseInterface response = srvAccountsV0.getAccounts(ACCOUNT_ID, "participants", null);

		// Assert
		Mockito.verify(iSrvIntAccountsV0).getAccount(Mockito.any(DtoIntFilterAccount.class));
		Mockito.verify(accountMapperV0).mapToOuter(dtoIntAccount);

		Assert.assertNotNull(response);
	}

	@Test
	public void testGetAccountConditionsExpandsNoFields200() {
		// Arrange
		final DtoIntAccount dtoIntAccount = facadeFactory.getDtoIntAccountDummy();
		final DtoAccount dtoAccount = facadeFactory.getDtoAccountDummy();
		Mockito.when(iSrvIntAccountsV0.getAccount(Mockito.any(DtoIntFilterAccount.class))).thenReturn(dtoIntAccount);
		Mockito.when(accountMapperV0.mapToOuter(dtoIntAccount)).thenReturn(dtoAccount);
		// Act
		final ServiceResponseInterface response = srvAccountsV0.getAccounts(ACCOUNT_ID, "conditions", null);

		// Assert
		Mockito.verify(iSrvIntAccountsV0).getAccount(Mockito.any(DtoIntFilterAccount.class));
		Mockito.verify(accountMapperV0).mapToOuter(dtoIntAccount);

		Assert.assertNotNull(response);
	}

	@Test
	public void testGetBalancesAccountFamilyIdexpected200() {
		// Arrange
		final DtoIntAggregatedAvailableBalance dtoIntAggregatedAvailableBalance = facadeFactory.getDtoIntAggregatedAvailableBalanceDummy();
		final DtoAggregatedAvailableBalance dtoAggregatedAvailableBalance = facadeFactory.getAggregatedAvailableBalanceDummy();
		Mockito.when(iSrvIntAccountsV0.getBalance(Mockito.any(DtoIntFilterAccount.class))).thenReturn(dtoIntAggregatedAvailableBalance);
		Mockito.when(aggregatedAvailableBalanceMapperV0.mapToOuter(dtoIntAggregatedAvailableBalance))
				.thenReturn(dtoAggregatedAvailableBalance);

		// Act
		final ServiceResponseInterface response = srvAccountsV0.getBalance(ACCOUNT_FAMILY_ID);

		// Asset
		Mockito.verify(iSrvIntAccountsV0).getBalance(Mockito.any(DtoIntFilterAccount.class));
		Mockito.verify(aggregatedAvailableBalanceMapperV0).mapToOuter(dtoIntAggregatedAvailableBalance);

		Assert.assertNotNull(response);
	}

}
