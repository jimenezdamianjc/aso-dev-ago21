package com.bbva.mzic.accounts.facade.v0.validators;

import static org.junit.Assert.assertTrue;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class ValidatorAccountsV0Test {
	private static final String ACCOUNT_ID = "12341234";
	private static final String CUSTOMER_ID = "87654321";
	@InjectMocks
	private ValidatorAccountsV0 validator;

	@Test
	public void validGetAccountTest() {
		ValidatorAccountsV0.getAccount("non-empty");
		assertTrue(true);

	}

	@Test(expected = BusinessServiceException.class)
	public void invalidEmptyGetAccountTest() {
		ValidatorAccountsV0.getAccount("");
	}

	@Test(expected = BusinessServiceException.class)
	public void invalidNullGetAccountTest() {
		ValidatorAccountsV0.getAccount(null);
	}

}
