package com.bbva.mzic.accounts.facade.v0;

import com.bbva.mzic.accounts.business.dto.DtoIntAccount;
import com.bbva.mzic.accounts.business.dto.DtoIntAccountType;
import com.bbva.mzic.accounts.business.dto.DtoIntAggregatedAvailableBalance;
import com.bbva.mzic.accounts.facade.v0.dto.DtoAccount;
import com.bbva.mzic.accounts.facade.v0.dto.DtoAccountType;
import com.bbva.mzic.accounts.facade.v0.dto.DtoAggregatedAvailableBalance;

public class FacadeTestFactory {

	public DtoAccount getDtoAccountDummy() {
		final DtoAccount dto = new DtoAccount();

		dto.setAccountId("1234567890123456");
		dto.setAccountType(new DtoAccountType());

		return dto;
	}

	public DtoIntAccount getDtoIntAccountDummy() {
		final DtoIntAccount dto = new DtoIntAccount();

		dto.setAccountId("1234567890123456");
		dto.setAccountType(new DtoIntAccountType());
		dto.setAlias("alias");

		return dto;
	}

	public DtoIntAggregatedAvailableBalance getDtoIntAggregatedAvailableBalanceDummy() {
		return new DtoIntAggregatedAvailableBalance();
	}

	public DtoAggregatedAvailableBalance getAggregatedAvailableBalanceDummy() {
		return new DtoAggregatedAvailableBalance();
	}

}
