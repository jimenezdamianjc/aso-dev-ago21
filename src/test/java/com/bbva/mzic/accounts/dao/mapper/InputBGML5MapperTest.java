package com.bbva.mzic.accounts.dao.mapper;

import com.bbva.mzic.accounts.business.dto.DtoIntFilterAccount;
import com.bbva.mzic.accounts.dao.model.bgl5.FormatoBGML5E;
import com.bbva.mzic.accounts.dao.model.bgl5.PeticionTransaccionBgl5;
import com.bbva.mzic.testutils.rm.utils.ServiceFactorySingleton;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class InputBGML5MapperTest {

	@InjectMocks
	private InputBGML5Mapper inputBgl5Mapper;

	@Test
	public void mapWithoutClientId() {
		// Arrange
		final DtoIntFilterAccount dtoIntFilterAccount = ServiceFactorySingleton.getInstance()
				.getStubObject(DtoIntFilterAccount.class);
		dtoIntFilterAccount.setClientId(null);

		// Act
		final PeticionTransaccionBgl5 response = inputBgl5Mapper.map(dtoIntFilterAccount);

		// Assert
		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getCuerpo());
		Assert.assertNotNull(response.getCuerpo().getPartes());
		Assert.assertNotNull(response.getCuerpo().getPartes().get(0));
	}

	@Test
	public void mapWithClientId() {
		// Arrange
		final DtoIntFilterAccount dtoIntFilterAccount = ServiceFactorySingleton.getInstance()
				.getStubObject(DtoIntFilterAccount.class);

		// Act
		final PeticionTransaccionBgl5 response = inputBgl5Mapper.map(dtoIntFilterAccount);

		// Assert
		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getCuerpo());
		Assert.assertNotNull(response.getCuerpo().getPartes());
		Assert.assertNotNull(response.getCuerpo().getPartes().get(0));
		final FormatoBGML5E formato = (FormatoBGML5E) response.getCuerpo().getPartes().get(0);
		// Assert.assertEquals(formato.getClienpu(), dtoIntFilterAccount.getClientId());
	}

	@Test
	public void mapInputNull() {
		final PeticionTransaccionBgl5 response = inputBgl5Mapper.map(null);
		Assert.assertNull(response);
	}
}
